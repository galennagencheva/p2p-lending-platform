import { AnonymousGuard } from './../auth/anonymous.guard';
import { UsersGuard } from './../auth/users.guard';
import { CustomSpinnerService } from './services/custom-spinner.service';
import { InvestmentsService } from './services/investments.service';
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToastrModule } from 'ngx-toastr';
import { NotificationService } from './services/notification.service';
import { StorageService } from './services/storage.service';
import { DebtsService } from './services/debts.service';
import { AuthGuard } from '../auth/auth.guard';
import { AdminGuard } from '../auth/admin.guard';

@NgModule({
  providers: [
    NotificationService,
    StorageService,
    DebtsService,
    InvestmentsService,
    CustomSpinnerService,
    AuthGuard,
    UsersGuard,
    AdminGuard,
    AnonymousGuard,
  ],
  imports: [
    CommonModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
    }),
  ],
})
export class CoreModule {
  public constructor(@Optional() @SkipSelf() parent: CoreModule) {
    if (parent) {
      throw new Error('Core module is already provided!');
    }
  }
}
