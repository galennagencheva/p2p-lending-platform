import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

interface ISpinnerState {
  show: boolean;
}

@Injectable({
  providedIn: 'root',
})
export class CustomSpinnerService {
  private loaderSubject = new Subject<ISpinnerState>();
  private loaderState = this.loaderSubject.asObservable();

  constructor() {}

  public getLoaderState(): Observable<ISpinnerState> {
    return this.loaderState;
  }

  public show() {
    this.loaderSubject.next({ show: true } as ISpinnerState);
  }

  public hide(delay?: number) {
    if (delay) {
      setTimeout(() => this.loaderSubject.next({ show: false } as ISpinnerState), delay);
    } else {
      this.loaderSubject.next({ show: false } as ISpinnerState);
    }
  }
}
