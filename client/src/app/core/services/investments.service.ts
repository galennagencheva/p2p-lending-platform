import { InvestmentDTO } from '../../models/investment-models/investment.dto';
import { CreateInvestmentDTO } from '../../models/investment-models/create-investment.dto';
import { CONFIG } from '../../config/config';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UpdateInvestmentDTO } from '../../models/investment-models/update-investment.dto';

@Injectable({
  providedIn: 'root',
})
export class InvestmentsService {
  constructor(private readonly http: HttpClient) {}

  getAllInvestmentsPerUser(userId: string): Observable<InvestmentDTO[]> {
    return this.http.get<InvestmentDTO[]>(`${CONFIG.DOMAIN_NAME}/users/${userId}/investments`);
  }

  createProposal(userId: string, debtId: string, body: CreateInvestmentDTO): Observable<InvestmentDTO> {
    return this.http.post<InvestmentDTO>(`${CONFIG.DOMAIN_NAME}/users/${userId}/debts/${debtId}/investments`, body);
  }

  updateInvestment(userId: string, investmentId: string, partialInvestment: Partial<UpdateInvestmentDTO>) {
    return this.http.put(`${CONFIG.DOMAIN_NAME}/users/${userId}/investments/${investmentId}`, partialInvestment);
  }
}
