import { BaseUserDTO } from './../../models/user-models/base-user.dto';
import { UserRegisterDTO } from '../../models/user-models/register-login.dto';
import { UserLoginDTO } from './../../models/user-models/login-user.dto';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { StorageService } from '../../core/services/storage.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private readonly helper = new JwtHelperService();

  private readonly isLoggedInSubject$ = new BehaviorSubject<boolean>(this.isUserLoggedIn());
  private readonly loggedUserSubject$ = new BehaviorSubject<BaseUserDTO>(this.loggedUser());

  constructor(
    private readonly http: HttpClient,
    private readonly router: Router,
    private readonly storage: StorageService,
  ) {}

  get isLoggedIn$(): Observable<boolean> {
    return this.isLoggedInSubject$.asObservable();
  }

  get loggedUser$(): Observable<BaseUserDTO> {
    return this.loggedUserSubject$.asObservable();
  }

  logout() {
    this.http.delete('http://localhost:3000/api/logout');
    this.storage.clear();
    this.isLoggedInSubject$.next(false);
    this.loggedUserSubject$.next(null);

    this.router.navigate(['/']);
  }

  login(user: UserLoginDTO) {
    return this.http.post<{ access_token: string }>(`http://localhost:3000/api/login`, user).pipe(
      tap(({ access_token }) => {
        try {
          const loggedUser = this.helper.decodeToken(access_token);

          this.storage.save('token', access_token);

          this.isLoggedInSubject$.next(true);
          this.loggedUserSubject$.next(loggedUser);
        } catch (error) {
          // error handling on the consumer side
        }
      }),
    );
  }

  register(user: UserRegisterDTO) {
    return this.http.post(`http://localhost:3000/users`, user);
  }

  createAdmin(user: UserRegisterDTO) {
    return this.http.post(`http://localhost:3000/users/admin`, user);
  }

  isUserLoggedIn(): boolean {
    return !!this.storage.read('token');
  }

  loggedUser(): BaseUserDTO {
    try {
      return this.helper.decodeToken(this.storage.read('token'));
    } catch (error) {
      // in case of storage tampering
      this.isLoggedInSubject$.next(false);

      return null;
    }
  }
}
