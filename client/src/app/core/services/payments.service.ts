import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PaymentDTO } from '../../models/payment-models/payment.dto';
import { Observable } from 'rxjs';
import { CONFIG } from '../../config/config';
import { CreatePaymentDTO } from '../../models/payment-models/create-payment.dto';
import { LoanDTO } from '../../models/loan-models/loan.dto';
import { tap } from 'rxjs/operators';
import { UsersService } from './users.service';

@Injectable({
  providedIn: 'root',
})
export class PaymentsService {
  constructor(private readonly http: HttpClient, private readonly usersService: UsersService) {}

  getAllPaymentsPerUser(userId: string, loanId: string): Observable<PaymentDTO[]> {
    return this.http.get<PaymentDTO[]>(`${CONFIG.DOMAIN_NAME}/users/${userId}/loans/${loanId}/payments`);
  }

  createPayment(userId: string, loanId: string, newPayment: CreatePaymentDTO): Observable<LoanDTO> {
    return this.http.post<LoanDTO>(`${CONFIG.DOMAIN_NAME}/users/${userId}/loans/${loanId}/payments`, newPayment).pipe(
      tap(updatedLoan => {
        const subjectValue = this.usersService.loggedUserInfoValue$();
        const index = subjectValue.loans.findIndex(loan => loan.id === updatedLoan.id);
        subjectValue.loans[index] = updatedLoan;
        subjectValue.balance -= newPayment.amount + newPayment.penaltyAmount;
        this.usersService.nextLoggedUserInfo$(subjectValue);
      }),
    );
  }
}
