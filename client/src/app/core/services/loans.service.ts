import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CONFIG } from '../../config/config';
import { LoanDTO } from '../../models/loan-models/loan.dto';
import { UsersService } from './users.service';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class LoansService {
  constructor(private readonly http: HttpClient, private usersService: UsersService) {}

  getAllLoansPerUser(userId: string): Observable<LoanDTO[]> {
    return this.http.get<LoanDTO[]>(`${CONFIG.DOMAIN_NAME}/users/${userId}/loans`);
  }

  createLoan(userId: string, debtId: string, investmentId: string): Observable<LoanDTO> {
    return this.http
      .post<LoanDTO>(`${CONFIG.DOMAIN_NAME}/users/${userId}/debts/${debtId}/investments/${investmentId}/loans`, {})
      .pipe(
        tap(createdLoan => {
          const subjectValue = this.usersService.loggedUserInfoValue$();
          subjectValue.loans.push(createdLoan);
          subjectValue.balance += createdLoan.investment.amount;
          this.usersService.nextLoggedUserInfo$(subjectValue);
        }),
      );
  }
}
