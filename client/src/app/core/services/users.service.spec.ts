import { LoggedUserInfoDTO } from './../../models/user-models/logged-user-info.dto';
import { UsersService } from './users.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { async, TestBed } from '@angular/core/testing';
import { CONFIG } from '../../config/config';
import { of } from 'rxjs';

describe('UsersService service', () => {
  let httpClient;
  let userId;
  let service: UsersService;

  beforeEach(async(() => {
    jest.clearAllMocks();

    httpClient = {
      get() {},
      post() {},
      put() {},
      patch() {},
      delete() {},
    };

    userId = '2';

    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [UsersService],
    }).overrideProvider(HttpClient, { useValue: httpClient });

    service = TestBed.get(UsersService);
  }));

  it('should be defined', () => {
    // Arrange & Act & Assert
    expect(service).toBeDefined();
  });

  describe('loggedUserInfoValue$', () => {
    it('should return the value of the subject', done => {
      // Arrange
      const newUser = new LoggedUserInfoDTO();
      (service as any).loggedUserInfoSubject$.next(newUser);

      // Act & Assert
      service.loggedUserInfoValue$();

      expect(service.loggedUserInfoValue$()).toEqual(newUser);

      done();
    });
  });

  describe('nextLoggedUserInfo$', () => {
    it('should import new value into the subject', done => {
      // Arrange
      const newUser = new LoggedUserInfoDTO();

      // Act & Assert
      service.nextLoggedUserInfo$(newUser);

      service.loggedUserInfo$.subscribe(loggedUserInfo => {
        expect(loggedUserInfo).toEqual(newUser);
        done();
      });
    });
  });

  describe('updateBalance should', () => {
    it('call the http.patch() method once', done => {
      // Arrange
      const url = `${CONFIG.DOMAIN_NAME}/users/${userId}`;
      const updatedBalance = { balance: 500 };
      service.nextLoggedUserInfo$({ balance: 0 } as any);
      const spy = jest.spyOn(httpClient, 'patch').mockReturnValue(of({ balance: 600 }));

      // Act & Assert
      (service as any).updateBalance(userId, updatedBalance).subscribe(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url, updatedBalance);

        done();
      });

      spy.mockClear();
    });

    it('update the behaviour subject balance property', done => {
      // Arrange
      const url = `${CONFIG.DOMAIN_NAME}/users/${userId}`;
      const updatedBalance: any = { balance: 500 };
      service.nextLoggedUserInfo$({ balance: 0 } as any);

      const spy = jest.spyOn(httpClient, 'patch').mockImplementation(() => of({ balance: 600 }));

      service.updateBalance(userId, updatedBalance).subscribe();

      // Act & Assert
      service.loggedUserInfo$.subscribe(loggedUserInfo => {
        expect(loggedUserInfo.balance).toBe(600);

        done();
      });
    });
  });

  describe('getAllUsers', () => {
    it('should call the http.get() method once', done => {
      // Arrange
      const url = `${CONFIG.DOMAIN_NAME}/users`;
      const spy = jest.spyOn(httpClient, 'get').mockReturnValue(of(['user1', 'user2']));

      // Act & Assert
      service.getAllUsers().subscribe(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url);

        done();
      });
    });

    it('should return users as an array', done => {
      // Arrange
      const mockedUsers: any = ['user1', 'user2'];
      const spy = jest.spyOn(httpClient, 'get').mockReturnValue(of(mockedUsers));

      // Act & Assert
      service.getAllUsers().subscribe(users => {
        expect(users).toEqual(mockedUsers);

        done();
      });
    });
  });

  describe('getUserById', () => {
    it('should call the http.get() method once', done => {
      // Arrange
      const url = `${CONFIG.DOMAIN_NAME}/users/${userId}`;
      const spy = jest.spyOn(httpClient, 'get').mockReturnValue(of('user'));

      // Act & Assert
      service.getUserById(userId).subscribe(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url);

        done();
      });
    });

    it('should return the http.get() method returned user', done => {
      // Arrange
      const spy = jest.spyOn(httpClient, 'get').mockReturnValue(of('user'));

      // Act & Assert
      service.getUserById(userId).subscribe(user => {
        expect(user).toEqual('user');

        done();
      });
    });
  });

  describe('getAllAdmins', () => {
    it('should call the http.get() method once', done => {
      // Arrange
      const url = `${CONFIG.DOMAIN_NAME}/users/admin`;
      const spy = jest.spyOn(httpClient, 'get').mockReturnValue(of(['admin1', 'admin2']));

      // Act & Assert
      service.getAllAdmins().subscribe(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url);

        done();
      });
    });

    it('should return the http.get() method returned admins array', done => {
      // Arrange
      const spy = jest.spyOn(httpClient, 'get').mockReturnValue(of(['admin1', 'admin2']));

      // Act & Assert
      service.getAllAdmins().subscribe(admins => {
        expect(admins).toEqual(['admin1', 'admin2']);

        done();
      });
    });
  });

  describe('banUser', () => {
    it('should call the http.put() method once', done => {
      // Arrange
      const url = `${CONFIG.DOMAIN_NAME}/users/${userId}`;
      const spy = jest.spyOn(httpClient, 'put').mockReturnValue(of('user'));

      // Act & Assert
      service.banUser(userId, '7').subscribe(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url, '7');

        done();
      });
    });

    it('should return the http.get() method returned banned user', done => {
      // Arrange
      const spy = jest.spyOn(httpClient, 'put').mockReturnValue(of('user'));

      // Act & Assert
      service.banUser(userId, '7').subscribe(user => {
        expect(user).toEqual('user');

        done();
      });
    });
  });

  describe('unBanUser', () => {
    it('should call the http.delete() method once', done => {
      // Arrange
      const url = `${CONFIG.DOMAIN_NAME}/users/unBan/${userId}`;
      const spy = jest.spyOn(httpClient, 'delete').mockReturnValue(of('user'));

      // Act & Assert
      service.unBanUser(userId).subscribe(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url);

        done();
      });
    });

    it('should return the http.deleted() method returned unbanned user', done => {
      // Arrange
      const spy = jest.spyOn(httpClient, 'delete').mockReturnValue(of('user'));

      // Act & Assert
      service.unBanUser(userId).subscribe(user => {
        expect(user).toEqual('user');

        done();
      });
    });
  });

  describe('delete', () => {
    it('should call the http.delete() method once', done => {
      // Arrange
      const url = `${CONFIG.DOMAIN_NAME}/users/${userId}`;
      const spy = jest.spyOn(httpClient, 'delete').mockReturnValue(of('user'));

      // Act & Assert
      service.deleteUser(userId).subscribe(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url);

        done();
      });
    });

    it('should return the http.deleted() method returned unbanned user', done => {
      // Arrange
      jest.spyOn(httpClient, 'delete').mockReturnValue(of('user'));

      // Act & Assert
      service.deleteUser(userId).subscribe(user => {
        expect(user).toEqual('user');

        done();
      });
    });
  });
});
