import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { LoansService } from './loans.service';
import { InvestmentsService } from './investments.service';
import { zip } from 'rxjs';
import { UsersService } from './users.service';
import { tap } from 'rxjs/operators';
import { LoggedUserInfoDTO } from '../../models/user-models/logged-user-info.dto';

@Injectable({
  providedIn: 'root',
})
export class SharedService {
  constructor(
    private readonly usersService: UsersService,
    private readonly authService: AuthService,
    private readonly loansService: LoansService,
    private readonly investmentsService: InvestmentsService,
  ) {
    this.authService.loggedUser$.subscribe(loggedUser => {
      !!loggedUser ? this.start(loggedUser.id) : this.stop();
    });
  }

  private start(userId: string) {
    return zip(
      this.usersService.getUserById(userId),
      this.investmentsService.getAllInvestmentsPerUser(userId),
      this.loansService.getAllLoansPerUser(userId),
    )
      .pipe(
        tap(([user, investments, loans]) => {
          const loggedUserInfo = new LoggedUserInfoDTO();
          loggedUserInfo.id = user.id;
          loggedUserInfo.username = user.username;
          loggedUserInfo.balance = user.balance;
          loggedUserInfo.loans = loans;
          loggedUserInfo.investments = investments;
          this.usersService.nextLoggedUserInfo$(loggedUserInfo);
        }),
      )
      .toPromise();
  }

  private stop() {
    this.usersService.nextLoggedUserInfo$(null);
  }
}
