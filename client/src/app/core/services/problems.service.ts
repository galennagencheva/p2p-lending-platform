import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ProblemDTO } from '../../models/problem-models/problem.dto';
import { CONFIG } from '../../config/config';
import { CreateProblemDTO } from '../../models/problem-models/create-problem.dto';

@Injectable({
  providedIn: 'root',
})
export class ProblemsService {
  constructor(private readonly http: HttpClient) {}

  getAllProblems(): Observable<ProblemDTO[]> {
    return this.http.get<ProblemDTO[]>(`${CONFIG.DOMAIN_NAME}/problems`);
  }

  createProblem(userId: string, loanId: string, problem: CreateProblemDTO): Observable<ProblemDTO> {
    return this.http.post<ProblemDTO>(`${CONFIG.DOMAIN_NAME}/problems/${userId}/loan/${loanId}`, problem);
  }
}
