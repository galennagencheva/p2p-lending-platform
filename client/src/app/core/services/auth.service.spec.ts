import { UserRegisterDTO } from '../../models/user-models/register-login.dto';
import { TestBed } from '@angular/core/testing';
import { AuthService } from './auth.service';
import { HttpClient } from '@angular/common/http';
import { StorageService } from './storage.service';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { combineLatest, of } from 'rxjs';
import { UserLoginDTO } from '../../models/user-models/login-user.dto';
import { LoginPageComponent } from '../../components/login-page/login-page.component';
import { SignUpSignInComponent } from '../../components/login-page/sign-up-sign-in/sign-up-sign-in.component';
import { SharedModule } from '../../shared/shared.module';
import { CommonModule } from '@angular/common';

describe('AuthService', () => {
  const http = {
    post() {},
    delete() {},
  };
  const storage = {
    read() {
      return '';
    },
    save() {},
    clear() {},
  };
  const jwtService = {
    decodeToken() {},
  };

  let getService: () => AuthService;
  let router: Router;

  beforeEach(() => {
    // clear all spies and mocks
    jest.clearAllMocks();

    TestBed.configureTestingModule({
      imports: [CommonModule, SharedModule, RouterTestingModule],
      declarations: [LoginPageComponent, SignUpSignInComponent],
      providers: [AuthService, HttpClient, StorageService],
    })
      .overrideProvider(HttpClient, { useValue: http })
      .overrideProvider(StorageService, { useValue: storage })
      .overrideProvider(jwtService, { useValue: jwtService });

    getService = () => TestBed.get(AuthService);
    router = TestBed.get(Router);
  });

  it('should handle correctly invalid (or empty/none) tokens', done => {
    const token = 'invalid.token';

    const spy = jest.spyOn(storage, 'save').mockImplementation(() => token);

    const service = getService();

    combineLatest(service.isLoggedIn$, service.loggedUser$).subscribe(([loggedIn, loggedUser]) => {
      expect(loggedIn).toBe(false);
      expect(loggedUser).toBe(null);

      done();
    });
  });

  it('should correctly initialize state with a valid token', done => {
    // tslint:disable-next-line: max-line-length
    const token =
      // tslint:disable-next-line:max-line-length
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluQGFkbWluLmNvbSIsIm5hbWUiOiJhZG1pbjEiLCJpYXQiOjE1NzE2Njc2OTUsImV4cCI6MTU3MTc1NDA5NX0.a7a0Ey6RCw_1djRi4z4r3vKZbuVFJ1YwFJPF8HE6vfo';

    const spy = jest.spyOn(storage, 'read').mockImplementation(() => token);

    const service = getService();

    combineLatest(service.isLoggedIn$, service.loggedUser$).subscribe(([loggedIn, loggedUser]) => {
      expect(loggedIn).toBe(true);
      expect(loggedUser).toBeDefined();
      done();
    });
  });

  it('login() should call http.post with the correct parameters', () => {
    const token =
      // tslint:disable-next-line:max-line-length
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluQGFkbWluLmNvbSIsIm5hbWUiOiJhZG1pbjEiLCJpYXQiOjE1NzE2Njc2OTUsImV4cCI6MTU3MTc1NDA5NX0.a7a0Ey6RCw_1djRi4z4r3vKZbuVFJ1YwFJPF8HE6vfo';

    const spy = jest.spyOn(http, 'post').mockImplementation(() => of({ token }));

    const service = getService();
    const user = new UserLoginDTO();

    service.login(user);

    expect(http.post).toHaveBeenCalledTimes(1);
    expect(http.post).toHaveBeenCalledWith(`http://localhost:3000/api/login`, user);
  });

  it('login() should call storage.save with the return valid token', done => {
    // tslint:disable-next-line:variable-name
    const access_token =
      // tslint:disable-next-line:max-line-length
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluQGFkbWluLmNvbSIsIm5hbWUiOiJhZG1pbjEiLCJpYXQiOjE1NzE2Njc2OTUsImV4cCI6MTU3MTc1NDA5NX0.a7a0Ey6RCw_1djRi4z4r3vKZbuVFJ1YwFJPF8HE6vfo';

    const postSpy = jest.spyOn(http, 'post').mockImplementation(() => of({ access_token }));
    const saveSpy = jest.spyOn(storage, 'save');

    const service = getService();
    const user = new UserLoginDTO();

    service.login(user).subscribe(() => {
      expect(storage.save).toHaveBeenCalledTimes(1);
      expect(storage.save).toHaveBeenCalledWith('token', access_token);

      done();
    });
  });

  it('login() should correctly set the state with a valid token returned', done => {
    // tslint:disable-next-line:variable-name
    const access_token =
      // tslint:disable-next-line:max-line-length
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluQGFkbWluLmNvbSIsIm5hbWUiOiJhZG1pbjEiLCJpYXQiOjE1NzE2Njc2OTUsImV4cCI6MTU3MTc1NDA5NX0.a7a0Ey6RCw_1djRi4z4r3vKZbuVFJ1YwFJPF8HE6vfo';

    const postSpy = jest.spyOn(http, 'post').mockImplementation(() => of({ access_token }));

    const service = getService();
    const user = new UserLoginDTO();

    service.login(user).subscribe(() => {
      combineLatest(service.isLoggedIn$, service.loggedUser$).subscribe(([isLoggedIn, loggedUser]) => {
        expect(isLoggedIn).toBe(true);
        expect(loggedUser).toBeDefined();

        done();
      });
    });
  });

  it('logout() should call storage.clear', () => {
    const spy = jest.spyOn(storage, 'clear').mockImplementation(() => {});

    const service = getService();

    service.logout();

    expect(storage.clear).toHaveBeenCalled();
  });

  it('logout() should set the correct state', done => {
    // tslint:disable-next-line: max-line-length
    const token =
      // tslint:disable-next-line:max-line-length
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluQGFkbWluLmNvbSIsIm5hbWUiOiJhZG1pbjEiLCJpYXQiOjE1NzE2Njc2OTUsImV4cCI6MTU3MTc1NDA5NX0.a7a0Ey6RCw_1djRi4z4r3vKZbuVFJ1YwFJPF8HE6vfo';

    const saveSpy = jest.spyOn(storage, 'save').mockImplementation(() => {});
    const readSpy = jest.spyOn(storage, 'read').mockImplementation(() => token);

    const service = getService();

    service.logout();

    combineLatest(service.isLoggedIn$, service.loggedUser$).subscribe(([loggedIn, loggedUser]) => {
      expect(loggedIn).toBe(false);
      expect(loggedUser).toBe(null);

      done();
    });
  });

  it('logout() should navigate to /', () => {
    // tslint:disable-next-line: max-line-length
    const token =
      // tslint:disable-next-line:max-line-length
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluQGFkbWluLmNvbSIsIm5hbWUiOiJhZG1pbjEiLCJpYXQiOjE1NzE2Njc2OTUsImV4cCI6MTU3MTc1NDA5NX0.a7a0Ey6RCw_1djRi4z4r3vKZbuVFJ1YwFJPF8HE6vfo';

    const spy = jest.spyOn(router, 'navigate').mockImplementation(async (...params: any[]) => true);

    const service = getService();

    service.logout();

    expect(router.navigate).toHaveBeenCalledWith(['/']);
  });

  it('register() should call http.post', () => {
    const user = new UserRegisterDTO();
    const spy = jest.spyOn(http, 'post');

    const service = getService();

    service.register(user);

    expect(http.post).toHaveBeenCalledWith(`http://localhost:3000/users`, user);
  });
});
