import { UpdateDebtDTO } from './../../models/debt-models/update-debt.dto';
import { DebtDTO } from '../../models/debt-models/debt.dto';
import { CONFIG } from '../../config/config';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CreateDebtDTO } from '../../models/debt-models/create-debt.dto';

@Injectable({
  providedIn: 'root',
})
export class DebtsService {
  constructor(private readonly http: HttpClient) {}

  createDebt(userId: string, createdDebt: CreateDebtDTO): Observable<DebtDTO> {
    return this.http.post<DebtDTO>(`${CONFIG.DOMAIN_NAME}/users/${userId}/debts`, createdDebt);
  }

  getAllDebts(): Observable<DebtDTO[]> {
    return this.http.get<DebtDTO[]>(`${CONFIG.DOMAIN_NAME}/debts`);
  }

  getAllDebtsPerUser(userId: string): Observable<DebtDTO[]> {
    return this.http.get<DebtDTO[]>(`${CONFIG.DOMAIN_NAME}/users/${userId}/debts`);
  }

  updateDebt(userId: string, debtId: string, updatedDebt: UpdateDebtDTO): Observable<DebtDTO> {
    return this.http.put<DebtDTO>(`${CONFIG.DOMAIN_NAME}/users/${userId}/debts/${debtId}`, updatedDebt);
  }

  deleteDebt(userId: string, debtId: string): Observable<DebtDTO> {
    return this.http.delete<DebtDTO>(`${CONFIG.DOMAIN_NAME}/users/${userId}/debts/${debtId}`);
  }
}
