import { LoggedUserInfoDTO } from './../../models/user-models/logged-user-info.dto';
import { UserBalanceInfoDTO } from './../../models/user-models/user-balance-info.dto';
import { UpdateUserBalanceDTO } from './../../models/user-models/update-user-balance.dto';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { CONFIG } from '../../config/config';
import { tap } from 'rxjs/operators';
import { BaseUserDTO } from '../../models/user-models/base-user.dto';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  private readonly loggedUserInfoSubject$ = new BehaviorSubject(null);

  constructor(private readonly http: HttpClient) {}

  get loggedUserInfo$(): Observable<any> {
    return this.loggedUserInfoSubject$.asObservable();
  }

  loggedUserInfoValue$() {
    return this.loggedUserInfoSubject$.getValue();
  }

  nextLoggedUserInfo$(value: LoggedUserInfoDTO): void {
    this.loggedUserInfoSubject$.next(value);
  }

  updateBalance(userId: string, updatedBalance: UpdateUserBalanceDTO): Observable<UserBalanceInfoDTO> {
    return this.http.patch<UserBalanceInfoDTO>(`${CONFIG.DOMAIN_NAME}/users/${userId}`, updatedBalance).pipe(
      tap((user: UserBalanceInfoDTO) => {
        this.nextLoggedUserInfo$({ ...this.loggedUserInfoValue$(), balance: user.balance });
      }),
    );
  }

  getAllUsers(): Observable<BaseUserDTO[]> {
    return this.http.get<BaseUserDTO[]>(`${CONFIG.DOMAIN_NAME}/users`);
  }

  getUserById(userId: string): Observable<UserBalanceInfoDTO> {
    return this.http.get<UserBalanceInfoDTO>(`${CONFIG.DOMAIN_NAME}/users/${userId}`);
  }

  getAllAdmins(): Observable<BaseUserDTO[]> {
    return this.http.get<BaseUserDTO[]>(`${CONFIG.DOMAIN_NAME}/users/admin`);
  }

  banUser(userId: string, DaysOfBan): Observable<BaseUserDTO> {
    return this.http.put<BaseUserDTO>(`${CONFIG.DOMAIN_NAME}/users/${userId}`, DaysOfBan);
  }

  unBanUser(userId: string): Observable<BaseUserDTO> {
    return this.http.delete<BaseUserDTO>(`${CONFIG.DOMAIN_NAME}/users/unBan/${userId}`);
  }

  deleteUser(userId: string): Observable<BaseUserDTO> {
    return this.http.delete<BaseUserDTO>(`${CONFIG.DOMAIN_NAME}/users/${userId}`);
  }
}
