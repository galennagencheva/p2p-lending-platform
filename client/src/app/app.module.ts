import { SharedModule } from './shared/shared.module';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { LoginPageModule } from './components/login-page/login-page.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { httpInterceptors } from './config';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from './core/core.module';
import { UserSummaryComponent } from './shared/user-summary/user-summary.component';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';

@NgModule({
  declarations: [AppComponent, NavbarComponent, UserSummaryComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CoreModule,
    LoginPageModule,
    BrowserAnimationsModule,
    SharedModule,
    ReactiveFormsModule,
    SweetAlert2Module.forRoot(),
  ],
  providers: [...httpInterceptors],
  bootstrap: [AppComponent],
})
export class AppModule {}
