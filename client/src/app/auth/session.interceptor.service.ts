import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { of, Observable } from 'rxjs';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { AuthService } from '../core/services/auth.service';

@Injectable()
export class SessionInterceptor implements HttpInterceptor {
  constructor(private readonly authService: AuthService, private readonly router: Router) {}

  private errorHandle(err: HttpErrorResponse, caught: Observable<HttpEvent<any>>): Observable<any> {
    if (err.status === 401) {
      this.authService.logout();
      return of();
    }

    if (err.status === 404) {
      this.router.navigate(['not-found']);
    }

    if (err.status === 500) {
      this.router.navigate(['server-error']);
    }

    throw err;
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const authReq = req.clone({});
    return next.handle(authReq).pipe(catchError((x, caught) => this.errorHandle(x, caught)));
  }
}
