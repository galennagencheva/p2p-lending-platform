import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';
import { StorageService } from '../core/services/storage.service';

@Injectable()
export class TokenInterceptorService implements HttpInterceptor {
  constructor(private readonly storageService: StorageService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler) {
    const token = this.storageService.read('token') || '';
    let updatedRequest;

    if (request.url.indexOf('imgur') > -1) {
      updatedRequest = request.clone({
        headers: request.headers.set('Authorization', `Client-ID 7084d3c72f8fab9`)
      });
    } else {
      updatedRequest = request.clone({
        headers: request.headers.set('Authorization', `Bearer ${token}`)
      });
    }

    return next.handle(updatedRequest);
  }
}
