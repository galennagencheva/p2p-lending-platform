import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { NotificationService } from '../core/services/notification.service';
import { AuthService } from '../core/services/auth.service';

@Injectable()
export class UsersGuard implements CanActivate {
  constructor(
    private readonly authService: AuthService,
    private readonly notificator: NotificationService,
    private readonly router: Router
  ) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.authService.loggedUser$.pipe(
      map(user => {
        if (user.roles.includes('admin')) {
          this.router.navigate(['admin']);
          this.notificator.error(`You are not authorized to access this page!`);
          return false;
        } else {
          return true;
        }
      })
    );
  }
}
