import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { AuthService } from '../core/services/auth.service';

@Injectable()
export class AnonymousGuard implements CanActivate {
  constructor(private readonly authService: AuthService, private readonly router: Router) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.authService.isLoggedIn$.pipe(
      tap(loggedIn => {
        if (loggedIn) {
          this.router.navigateByUrl('/dashboard');
        }
      }),
      map(loggedIn => {
        if (loggedIn === false) {
          return !loggedIn;
        }
      })
    );
  }
}
