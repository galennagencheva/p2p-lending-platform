import { Directive, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[appOverDueBorder]'
})
export class OverDueBorderDirective {
  @Input('appOverDueBorder')
  set show(show: boolean) {
    if (show) {
      this.el.nativeElement.style.border = '2px solid rgba(167, 0, 9, 0.859)';
    }
  }

  constructor(private el: ElementRef) {}
}
