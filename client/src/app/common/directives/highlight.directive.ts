import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {
  @Input('appHighlight')
  highlightColor: string;

  constructor(private el: ElementRef) {}

  @HostListener('mouseenter') onMouseEnter() {
    this.highlight(`0px 0px 4px 1px ${this.highlightColor}`, 'pointer');
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.highlight(null, null);
  }

  private highlight(boxShadowProperties: string, cursorProperty: string) {
    this.el.nativeElement.style.boxShadow = boxShadowProperties;
    this.el.nativeElement.style.cursor = cursorProperty;
  }
}
