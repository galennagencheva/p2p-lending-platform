import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'isArrayLength' })
export class IsArrayLengthPipe implements PipeTransform {
  transform(array: [], keyword?: string) {
    if (!array.length) {
      return `You do not have any ${keyword} at the moment.`;
    }
    return;
  }
}
