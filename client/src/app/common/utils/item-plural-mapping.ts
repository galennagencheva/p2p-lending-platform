export const itemPluralMapping = {
  investors: {
    '=0': 'currently no investors',
    '=1': '1 investor',
    other: '# investors',
  },
  borrowers: {
    '=0': 'currently no borrowers',
    '=1': '1 borrower',
    other: '# borrowers',
  },
  monthsForLoan: {
    '=0': 'you do not have any loans',
    '=1': '1 month',
    other: '# months',
  },
  monthsForInvestment: {
    '=0': 'you do not have any investments',
    '=1': '1 month',
    other: '# months',
  },
  nextDueDate: {
    '=0': 'currently none',
    other: '#',
  },
  nextInstallment: {
    '=£0.00': 'currently none',
    other: '#',
  },
};
