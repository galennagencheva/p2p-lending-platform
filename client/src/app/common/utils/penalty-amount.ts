export const calculatePenaltyAmount = (days: number, penaltyInterest: number, totalAmount: number) => {
  const penaltyAmount = Math.pow(1 + penaltyInterest / 100 / 360, days);
  const penaltyAmountPerDay = (penaltyAmount - 1) * totalAmount;

  return days * penaltyAmountPerDay;
};
