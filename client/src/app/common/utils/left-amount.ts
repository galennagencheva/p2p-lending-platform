export const calculateLeftAmount = (totalAmount: number, installmentAmount: number, paymentsCount: number) => {
  return totalAmount - installmentAmount * paymentsCount;
};
