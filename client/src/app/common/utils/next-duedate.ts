import * as moment from 'moment';

export const calculateDueDate = (createOnDate: string, paymentsCount: number) => {
  return moment(createOnDate)
    .add(paymentsCount + 1, 'M')
    .format('YYYY-MM-DD');
};
