export const calculateOverallAmount = (amount: number, interest: number, period: number) => {
  return amount * (1 + (interest / 100 / 12) * period);
};
