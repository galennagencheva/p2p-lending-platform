import * as moment from 'moment';

export const calculateOverdueDays = (dueDate: string, currentDate: string) => {
  return moment(currentDate).diff(moment(dueDate), 'days');
};
