import { calculateDueDate } from './next-duedate';
import { calculateLeftAmount } from './left-amount';
import { calculateOverdueDays } from './overdue-days';
import { calculatePenaltyAmount } from './penalty-amount';
import * as moment from 'moment';

export const calculateLoanDetails = loan => {
  const currentDate = moment(new Date()).format('YYYY-MM-DD');
  const overallAmount: number = loan.overallAmount;
  const monthlyInstallment: number = loan.monthlyInstallment;
  const interest: number = loan.investment.interest;
  const penaltyInterest: number = loan.investment.overdueInterest;
  const createdDate = loan.createdOn;

  let paymentsCount: number;
  if (loan.payments.length) {
    paymentsCount = loan.payments.length;
  } else {
    paymentsCount = 0;
  }

  const nextDueDate = calculateDueDate(createdDate, paymentsCount);
  const leftAmount = calculateLeftAmount(overallAmount, monthlyInstallment, paymentsCount);

  let overdueDays: number;
  let penaltyAmount: number;
  if (currentDate > nextDueDate) {
    overdueDays = calculateOverdueDays(nextDueDate, currentDate);
    penaltyAmount = calculatePenaltyAmount(overdueDays, penaltyInterest, overallAmount);
  } else {
    overdueDays = 0;
    penaltyAmount = 0;
  }

  const investorUsername = loan.investment.user.username;

  return {
    overallAmount,
    monthlyInstallment,
    interest,
    penaltyInterest,
    createdDate,
    nextDueDate,
    leftAmount,
    overdueDays,
    penaltyAmount,
    investorUsername,
  };
};
