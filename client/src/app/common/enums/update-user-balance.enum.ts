export enum UpdateUserBalance {
  Deposit = 'Deposit',
  Withdraw = 'Withdraw',
}
