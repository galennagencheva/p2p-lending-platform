export enum StatusDebt {
  Pending = 'Pending',
  PartiallyFunded = 'Partially Funded',
  Funded = 'Funded',
}
