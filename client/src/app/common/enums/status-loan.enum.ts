export enum StatusLoan {
  Ongoing = 'Ongoing',
  Completed = 'Completed',
}
