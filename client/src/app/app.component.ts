import { CustomSpinnerService } from './core/services/custom-spinner.service';
import { BaseUserDTO } from './models/user-models/base-user.dto';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from './core/services/auth.service';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit, OnDestroy {
  show;

  private loggedInSubscription: Subscription;
  private userSubscription: Subscription;

  public loggedIn: boolean;
  public user: BaseUserDTO;

  constructor(
    private readonly authService: AuthService,
    public router: Router,
    public route: ActivatedRoute,
    private readonly customSpinnerService: CustomSpinnerService,
  ) {
    this.router.events
      .pipe(filter(e => e instanceof NavigationEnd))
      .subscribe(e => this.customSpinnerService.hide(500));
    this.router.events.pipe(filter(e => e instanceof NavigationStart)).subscribe(e => this.customSpinnerService.show());
    this.router.events.pipe(filter(e => e instanceof NavigationEnd)).subscribe(e => this.modify(e));
  }

  ngOnInit() {
    this.loggedInSubscription = this.authService.isLoggedIn$.subscribe(loggedIn => (this.loggedIn = loggedIn));
    this.userSubscription = this.authService.loggedUser$.subscribe(user => (this.user = user));
  }

  ngOnDestroy() {
    this.loggedInSubscription.unsubscribe();
    this.userSubscription.unsubscribe();
  }

  triggerLogout() {
    this.authService.logout();
  }

  modify(location) {
    if (location.url === '/home' || location.urlAfterRedirects === '/home') {
      this.show = false;
    } else if (location.url === '/not-found' || location.urlAfterRedirects === '/not-found') {
      this.show = false;
    } else if (location.url === '/server-error' || location.urlAfterRedirects === '/server-error') {
      this.show = false;
    } else {
      this.show = true;
    }
  }
}
