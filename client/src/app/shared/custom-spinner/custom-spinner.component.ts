import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CustomSpinnerService } from '../../core/services/custom-spinner.service';

@Component({
  selector: 'app-custom-spinner',
  templateUrl: './custom-spinner.component.html',
  styleUrls: ['./custom-spinner.component.css'],
})
export class CustomSpinnerComponent implements OnInit, OnDestroy {
  public destroy$: Subject<boolean> = new Subject<boolean>();

  public show = false;

  constructor(private spinnerService: CustomSpinnerService) {}

  ngOnInit() {
    this.spinnerService
      .getLoaderState()
      .pipe(takeUntil(this.destroy$))
      .subscribe(state => {
        this.show = state.show;
        this.setScrollBarVisibility(!this.show);
      });
  }

  private setScrollBarVisibility(visible: boolean) {
    document.body.style.overflow = visible ? 'auto' : 'hidden';
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
}
