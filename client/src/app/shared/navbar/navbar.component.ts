import { OnInit, Component, OnDestroy } from '@angular/core';
import { AuthService } from '../../../app/core/services/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit, OnDestroy {
  admin;
  user;
  isCollapsed = false;
  loggedUser: any;
  loggedUserSubscription: Subscription;

  constructor(private readonly authService: AuthService) {}

  ngOnInit() {
    this.loggedUserSubscription = this.authService.loggedUser$.subscribe(loggedUser => (this.loggedUser = loggedUser));
    this.verifyUserRolesAdmin();
    this.verifyUserRolesUser();
  }

  ngOnDestroy() {
    if (this.loggedUserSubscription) {
      this.loggedUserSubscription.unsubscribe();
    }
  }

  logOut() {
    this.authService.logout();
  }
  private verifyUserRolesAdmin() {
    if (this.loggedUser.roles.includes('admin')) {
      this.admin = true;
    } else {
      this.admin = false;
    }
  }

  private verifyUserRolesUser() {
    if (this.loggedUser.roles.includes('user')) {
      this.user = true;
    } else {
      this.user = false;
    }
  }
}
