import { LoggedUserInfoDTO } from './../../models/user-models/logged-user-info.dto';
import { SharedService } from './../../core/services/shared.service';
import { StatusInvestment } from './../../common/enums/status-investment.enum';
import { calculateDueDate } from '../../common/utils/next-duedate';
import { Subscription } from 'rxjs';
import { UserSummaryInfoDTO } from './../../models/user-models/user-summary-info.dto';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { UsersService } from '../../core/services/users.service';
import { AuthService } from '../../core/services/auth.service';
import { calculateLeftAmount } from '../../common/utils/left-amount';
import { StatusLoan } from '../../common/enums/status-loan.enum';

@Component({
  selector: 'app-user-summary',
  templateUrl: './user-summary.component.html',
  styleUrls: ['./user-summary.component.css'],
})
export class UserSummaryComponent implements OnInit, OnDestroy {
  loggedUser: UserSummaryInfoDTO;
  loggedUserRoles: string[];

  loggedUserInfoSubscription$: Subscription;
  loggedUserRolesSubscription$: Subscription;

  constructor(
    private readonly usersService: UsersService,
    private readonly sharedService: SharedService,
    private readonly authService: AuthService,
  ) {}

  ngOnInit() {
    this.loggedUserInfoSubscription$ = this.usersService.loggedUserInfo$.subscribe(
      (loggedUserInfo: LoggedUserInfoDTO) => {
        if (loggedUserInfo !== null) {
          this.getUserSummaryInfo(loggedUserInfo);
        }
      },
    );

    this.loggedUserRolesSubscription$ = this.authService.loggedUser$.subscribe(loggedUser => {
      if (loggedUser) {
        this.loggedUserRoles = loggedUser.roles;
      }
    });
  }

  ngOnDestroy() {
    if (this.loggedUserInfoSubscription$) {
      this.loggedUserInfoSubscription$.unsubscribe();
    }

    if (this.loggedUserRolesSubscription$) {
      this.loggedUserRolesSubscription$.unsubscribe();
    }
  }

  private getUserSummaryInfo(loggedUserInfo: LoggedUserInfoDTO) {
    this.loggedUser = new UserSummaryInfoDTO();
    this.loggedUser.username = loggedUserInfo.username;
    this.loggedUser.currentBalance = loggedUserInfo.balance;
    this.loggedUser.totalDebtAmount = loggedUserInfo.loans.reduce((acc, loan) => {
      return acc + calculateLeftAmount(loan.overallAmount, loan.monthlyInstallment, loan.payments.length);
    }, 0);

    const allInvestmentsLoans = [];
    loggedUserInfo.investments
      .filter(investment => investment.status === StatusInvestment.Ongoing)
      .map(investment => {
        const eachInvestmentLoan = investment.loans[0];
        if (eachInvestmentLoan) {
          allInvestmentsLoans.push(eachInvestmentLoan);
        }
      });

    const totalInvestedAmount: number = allInvestmentsLoans
      .filter(loan => loan.status === StatusLoan.Ongoing)
      .reduce(
        (acc, loan) => acc + calculateLeftAmount(loan.overallAmount, loan.monthlyInstallment, loan.payments.length),
        0,
      );
    this.loggedUser.totalInvestedAmount = totalInvestedAmount;

    const allLoansDueDates = loggedUserInfo.loans
      .filter(loan => loan.status === StatusLoan.Ongoing)
      .map(loan => calculateDueDate(loan.createdOn, loan.payments.length));
    if (allLoansDueDates.length) {
      this.loggedUser.nextDueDate = allLoansDueDates.sort()[0];
    }
  }

  private verifyUserRole() {
    return this.loggedUserRoles.includes('admin') ? true : false;
  }
}
