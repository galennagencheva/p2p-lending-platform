import { OnInit, OnDestroy, Component } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { AuthService } from '../../core/services/auth.service';

@Component({
  selector: 'app-server-error-page',
  templateUrl: './server-error-page.component.html',
  styleUrls: ['./server-error-page.component.css'],
})
export class ServerErrorPageComponent implements OnInit, OnDestroy {
  loggedUserState;
  private loggedUserSubscription: Subscription;

  constructor(private readonly router: Router, private authService: AuthService) {}

  ngOnInit() {
    this.loggedUserSubscription = this.authService.isLoggedIn$.subscribe(
      isLoggedIn => (this.loggedUserState = isLoggedIn),
    );
  }

  ngOnDestroy() {
    if (this.loggedUserSubscription) {
      this.loggedUserSubscription.unsubscribe();
    }
  }

  redirectToBooks() {
    if (this.loggedUserState) {
      this.router.navigateByUrl('dashboard');
    } else {
      this.router.navigateByUrl('home');
    }
  }
}
