import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../../core/services/auth.service';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.css'],
})
export class NotFoundComponent implements OnInit, OnDestroy {
  loggedUserState;
  private loggedUserSubscription: Subscription;

  constructor(private readonly router: Router, private authService: AuthService) {}

  ngOnInit() {
    this.loggedUserSubscription = this.authService.isLoggedIn$.subscribe(
      isLoggedIn => (this.loggedUserState = isLoggedIn),
    );
  }

  ngOnDestroy() {
    if (this.loggedUserSubscription) {
      this.loggedUserSubscription.unsubscribe();
    }
  }

  redirectToDashboard() {
    if (this.loggedUserState) {
      this.router.navigateByUrl('dashboard');
    } else {
      this.router.navigateByUrl('home');
    }
  }
}
