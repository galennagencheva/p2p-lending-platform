import { OverDueBorderDirective } from './../common/directives/overdue-border.directive';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { NgxBootstrapSliderModule } from 'ngx-bootstrap-slider';
import { ModalModule, TooltipModule, PopoverModule, ButtonsModule } from 'angular-bootstrap-md';
import { BsDropdownModule } from 'ngx-bootstrap';
import { ServerErrorPageComponent } from './server-error-page/server-error-page.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { IsArrayLengthPipe } from '../common/pipes/array-length.pipe';
import { FadeInAnimationDirective } from '../common/directives/fade-in-animation.directive';
import { HighlightDirective } from '../common/directives/highlight.directive';
import { CustomSpinnerComponent } from './custom-spinner/custom-spinner.component';

@NgModule({
  declarations: [
    ServerErrorPageComponent,
    NotFoundComponent,
    IsArrayLengthPipe,
    HighlightDirective,
    FadeInAnimationDirective,
    OverDueBorderDirective,
    CustomSpinnerComponent,
  ],
  imports: [
    NgxBootstrapSliderModule,
    HttpClientModule,
    CommonModule,
    FormsModule,
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
  ],
  exports: [
    NgbModule,
    FontAwesomeModule,
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    BsDropdownModule,
    ModalModule,
    TooltipModule,
    PopoverModule,
    ButtonsModule,
    NgxBootstrapSliderModule,
    IsArrayLengthPipe,
    HighlightDirective,
    FadeInAnimationDirective,
    OverDueBorderDirective,
    CustomSpinnerComponent,
  ],
})
export class SharedModule {}
