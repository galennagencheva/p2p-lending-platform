import { StatusDebt } from '../../common/enums/status-debt.enum';
import { BaseInvestmentDTO } from '../investment-models/base-investment.dto';
import { LoanDTO } from '../loan-models/loan.dto';
import { BaseUserDTO } from '../user-models/base-user.dto';
export class DebtDTO {
  id: string;
  amount: number;
  period: number;
  status: StatusDebt;
  isDeleted: boolean;
  isPartial: boolean;
  investments: BaseInvestmentDTO[];
  loans: LoanDTO[];
  user: BaseUserDTO;
}
