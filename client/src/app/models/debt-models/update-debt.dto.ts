export class UpdateDebtDTO {
  amount?: number;
  period?: number;
}
