import { StatusDebt } from '../../common/enums/status-debt.enum';
import { BaseUserDTO } from '../user-models/base-user.dto';

export class BaseDebtDTO {
  id: string;
  amount: number;
  period: number;
  status: StatusDebt;
  isDeleted: boolean;
  isPartial: boolean;
  user: BaseUserDTO;
}
