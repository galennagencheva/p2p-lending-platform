import { BaseUserDTO } from '../user-models/base-user.dto';
import { StatusInvestment } from '../../common/enums/status-investment.enum';
import { BaseDebtDTO } from '../debt-models/base-debt.dto';
import { LoanWithPaymentsDTO } from '../loan-models/loan-with-payments.dto';

export class InvestmentDTO {
  id: string;
  amount: number;
  period: number;
  interest: number;
  overdueInterest: number;
  status: any;
  user: BaseUserDTO;
  debt: BaseDebtDTO;
  loans: LoanWithPaymentsDTO[];
}
