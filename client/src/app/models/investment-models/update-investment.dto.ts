import { StatusInvestment } from './../../common/enums/status-investment.enum';

export class UpdateInvestmentDTO {
  amount: number;
  period: number;
  interest: number;
  overdueInterest: number;
  status: StatusInvestment;
}
