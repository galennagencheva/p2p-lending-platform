import { StatusInvestment } from '../../common/enums/status-investment.enum';
import { BaseDebtDTO } from '../debt-models/base-debt.dto';

export class BaseInvestmentWithoutUserDTO {
  id: string;
  amount: number;
  period: number;
  interest: number;
  overdueInterest: number;
  status: StatusInvestment;
  debt: BaseDebtDTO;
}
