export class CreateInvestmentDTO {
  amount: number;
  period: number;
  interest: number;
  overdueInterest: number;
}
