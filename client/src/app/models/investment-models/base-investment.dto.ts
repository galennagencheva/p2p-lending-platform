import { StatusInvestment } from '../../common/enums/status-investment.enum';
import { BaseUserDTO } from '../user-models/base-user.dto';

export class BaseInvestmentDTO {
  id: string;
  amount: number;
  period: number;
  interest: number;
  overdueInterest: number;
  status: StatusInvestment;
  user: BaseUserDTO;
}
