export class UserBalanceInfoDTO {
  id: string;
  username: string;
  balance: number;
}
