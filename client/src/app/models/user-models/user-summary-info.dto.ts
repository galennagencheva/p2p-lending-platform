export class UserSummaryInfoDTO {
  username: string;
  currentBalance: number;
  totalInvestedAmount: number;
  totalDebtAmount: number;
  nextDueDate: string;
}
