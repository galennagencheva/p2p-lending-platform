import { BaseDebtDTO } from '../debt-models/base-debt.dto';
import { LoanDTO } from '../loan-models/loan.dto';
import { InvestmentDTO } from '../investment-models/investment.dto';

export class LoggedUserInfoDTO {
  id: string;
  username: string;
  balance: number;
  investments: InvestmentDTO[];
  loans: LoanDTO[];
}
