export class DashboardUserInfoDTO {
  totalPeriodOfInvestment: number;
  borrowersCount: number;
  nextDueDate: string;
  nextInstallment: number;
}
