export class BaseUserDTO {
  id: string;
  username: string;
  roles: string[];
  BanExpirationDate: Date;
  isBanned: boolean;
  balance: number;
}
