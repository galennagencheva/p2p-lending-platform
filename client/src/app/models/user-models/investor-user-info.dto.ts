export class InvestorUserInfoDTO {
  totalPeriodOfInvestments: number;
  borrowersCount: number;
  nextDueDate: string;
  nextInstallmentToBePayedToTheInvestor: number;
}
