import { UpdateUserBalance } from './../../common/enums/update-user-balance.enum';
export class UpdateUserBalanceDTO {
  action: UpdateUserBalance;
  amount: number;
}
