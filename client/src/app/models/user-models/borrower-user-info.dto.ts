export class BorrowerUserInfoDTO {
  totalPeriodOfLoans: number;
  investorsCount: number;
  nextDueDate: string;
  nextInstallment: number;
}
