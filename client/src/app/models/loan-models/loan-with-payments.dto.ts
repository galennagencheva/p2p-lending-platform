import { PaymentDTO } from '../payment-models/payment.dto';
import { StatusLoan } from '../../common/enums/status-loan.enum';
import { BaseUserDTO } from '../user-models/base-user.dto';

export class LoanWithPaymentsDTO {
  id: string;
  createdOn: string;
  monthlyInstallment: number;
  overallAmount: number;
  status: StatusLoan;
  payments: PaymentDTO[];
  user: BaseUserDTO;
}
