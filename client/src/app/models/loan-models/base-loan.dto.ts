import { PaymentDTO } from '../payment-models/payment.dto';
import { StatusLoan } from '../../common/enums/status-loan.enum';
import { BaseInvestmentDTO } from '../investment-models/base-investment.dto';

export class BaseLoanDTO {
  id: string;
  createdOn: string;
  monthlyInstallment: number;
  overallAmount: number;
  status: StatusLoan;
  payments: PaymentDTO[];
  investment: BaseInvestmentDTO;
}
