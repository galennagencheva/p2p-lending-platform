export class CalculatedLoanDetailsDTO {
  overallAmount: number;
  monthlyInstallment: number;
  interest: number;
  penaltyInterest: number;
  createdDate: string;
  nextDueDate: string;
  leftAmount: number;
  overdueDays: number;
  penaltyAmount: number;
  investorUsername: string;
}
