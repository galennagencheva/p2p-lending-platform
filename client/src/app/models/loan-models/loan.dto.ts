import { StatusLoan } from './../../common/enums/status-loan.enum';
import { BaseUserDTO } from '../user-models/base-user.dto';
import { BaseInvestmentDTO } from '../investment-models/base-investment.dto';
import { PaymentDTO } from '../payment-models/payment.dto';
import { BaseDebtDTO } from '../debt-models/base-debt.dto';

export class LoanDTO {
  id: string;
  createdOn: string;
  overallAmount: number;
  monthlyInstallment: number;
  status: StatusLoan;
  user: BaseUserDTO;
  investment: BaseInvestmentDTO;
  debt: BaseDebtDTO;
  payments: PaymentDTO[];
}
