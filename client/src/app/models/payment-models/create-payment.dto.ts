export class CreatePaymentDTO {
  amount: number;
  overdue?: number;
  penaltyAmount?: number;
}
