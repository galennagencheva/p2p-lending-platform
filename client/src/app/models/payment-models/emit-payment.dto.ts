import { CreatePaymentDTO } from './create-payment.dto';

export class EmitPaymentDTO {
  id: string;
  newPayment: CreatePaymentDTO;
}
