import { CreateProblemDTO } from './create-problem.dto';

export class EmitProblemDTO {
  loanId: string;
  description: CreateProblemDTO;
}
