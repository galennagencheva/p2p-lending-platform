import { BaseUserDTO } from '../user-models/base-user.dto';
import { BaseLoanDTO } from '../loan-models/base-loan.dto';

export class ProblemDTO {
  id: string;
  description: string;
  user: BaseUserDTO;
  loan: BaseLoanDTO;
}
