import { AnonymousGuard } from './auth/anonymous.guard';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ServerErrorPageComponent } from './shared/server-error-page/server-error-page.component';
import { NotFoundComponent } from './shared/not-found/not-found.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: LoginPageComponent, canActivate: [AnonymousGuard] },
  {
    path: 'dashboard',
    loadChildren: () => import('./components/dashboard/dashboard.module').then(m => m.DashboardModule),
  },
  { path: 'investor', loadChildren: () => import('./components/investor/investor.module').then(m => m.InvestorModule) },
  { path: 'borrower', loadChildren: () => import('./components/borrower/borrower.module').then(m => m.BorrowerModule) },
  { path: 'admin', loadChildren: () => import('./components/admin/admin.module').then(m => m.AdminModule) },
  { path: 'not-found', component: NotFoundComponent },
  { path: 'server-error', component: ServerErrorPageComponent },
  { path: '**', redirectTo: 'not-found' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
