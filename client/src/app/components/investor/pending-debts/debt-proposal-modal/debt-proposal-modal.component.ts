import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DebtDTO } from '../../../../models/debt-models/debt.dto';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-debt-proposal-modal',
  templateUrl: './debt-proposal-modal.component.html',
  styleUrls: ['./debt-proposal-modal.component.css'],
})
export class DebtProposalModalComponent implements OnInit {
  @Input()
  debt: DebtDTO;
  debtRemainingAmount: number;
  createProposalForm: FormGroup;

  @Output() create = new EventEmitter();

  constructor(private readonly fb: FormBuilder, protected debtProposalModal: BsModalRef) {}

  ngOnInit() {
    if (this.debt.isPartial === false) {
      this.createProposalForm = this.fb.group({
        amount: [
          { value: this.debt.amount, disabled: true },
          Validators.compose([Validators.required, Validators.min(100), Validators.max(this.debt.amount)]),
        ],
        period: [{ value: this.debt.period, disabled: true }, Validators.compose([Validators.required])],
        interest: ['', Validators.compose([Validators.required, Validators.min(1)])],
        overdueInterest: ['', Validators.compose([Validators.required, Validators.min(1)])],
      });
    } else {
      const acceptedInvestments = this.debt.investments.filter(eachInvestment => eachInvestment.status === 1);
      const totalSumOfAcceptedInvestments = acceptedInvestments.reduce((acc, investment) => {
        return acc + investment.amount;
      }, 0);

      this.debtRemainingAmount = this.debt.amount - totalSumOfAcceptedInvestments;

      this.createProposalForm = this.fb.group({
        amount: [
          '',
          Validators.compose([Validators.required, Validators.min(100), Validators.max(this.debtRemainingAmount)]),
        ],
        period: ['', Validators.compose([Validators.required, Validators.min(1)])],
        interest: ['', Validators.compose([Validators.required, Validators.min(1)])],
        overdueInterest: ['', Validators.compose([Validators.required, Validators.min(1)])],
      });
    }
  }

  createProposalInvestment() {
    const proposal = this.createProposalForm.getRawValue();
    this.create.emit(proposal);
  }
}
