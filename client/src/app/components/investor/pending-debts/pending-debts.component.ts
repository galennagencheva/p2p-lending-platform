import { itemPluralMapping } from '../../../common/utils/item-plural-mapping';
import { DebtProposalModalComponent } from './debt-proposal-modal/debt-proposal-modal.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { NotificationService } from './../../../core/services/notification.service';
import { Subscription } from 'rxjs';
import { UsersService } from './../../../core/services/users.service';
import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DebtDTO } from '../../../models/debt-models/debt.dto';

@Component({
  selector: 'app-pending-debts',
  templateUrl: './pending-debts.component.html',
  styleUrls: ['./pending-debts.component.css'],
})
export class PendingDebtsComponent implements OnInit, OnDestroy {
  @Input() debt: DebtDTO;
  @Output() create = new EventEmitter();

  loggedUserBalance: number;
  debtRemainingAmount: number;
  createProposalForm: FormGroup;

  debtProposalModal: BsModalRef;

  loggedUserInfoSubscription$: Subscription;
  debtProposalSubscription$: Subscription;

  protected readonly highlightColor = '#26695599';
  protected readonly itemPluralMapping = itemPluralMapping;

  constructor(
    private readonly usersService: UsersService,
    private readonly notificationService: NotificationService,
    private readonly modalService: BsModalService,
  ) {}

  ngOnInit() {
    this.loggedUserInfoSubscription$ = this.usersService.loggedUserInfo$.subscribe(loggedUserInfo => {
      if (loggedUserInfo !== null) {
        this.loggedUserBalance = loggedUserInfo.balance;
      }
    });

    const acceptedInvestments = this.debt.investments.filter(eachInvestment => eachInvestment.status === 1);
    const totalSumOfAcceptedInvestments = acceptedInvestments.reduce((acc, investment) => {
      return acc + investment.amount;
    }, 0);

    this.debtRemainingAmount = this.debt.amount - totalSumOfAcceptedInvestments;
  }

  ngOnDestroy() {
    if (this.loggedUserInfoSubscription$) {
      this.loggedUserInfoSubscription$.unsubscribe();
    }

    if (this.debtProposalSubscription$) {
      this.debtProposalSubscription$.unsubscribe();
    }
  }

  openDebtProposalModal() {
    const initialState = {
      debt: this.debt,
    };
    this.debtProposalModal = this.modalService.show(DebtProposalModalComponent, { initialState });
    this.debtProposalSubscription$ = this.debtProposalModal.content.create.subscribe(proposal => {
      if (proposal.amount > this.loggedUserBalance) {
        this.debtProposalModal.hide();
        this.notificationService.error('Insufficient balance! Please top up!');
      } else {
        this.create.emit(proposal);
        this.debtProposalModal.hide();
        this.notificationService.success('Investment proposal sent!');
      }
    });
  }
}
