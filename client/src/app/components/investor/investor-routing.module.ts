import { UsersGuard } from './../../auth/users.guard';
import { InvestorMainViewComponent } from './investor-main-view/investor-main-view.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DebtResolverService } from './resolvers/debt-resolver.service';
import { InvestorResolverService } from './resolvers/investment-resolver.service';
import { AuthGuard } from '../../auth/auth.guard';

const investorRoutes: Routes = [
  {
    path: '',
    component: InvestorMainViewComponent,
    resolve: {
      debts: DebtResolverService,
      investments: InvestorResolverService,
    },
    canActivate: [UsersGuard, AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(investorRoutes)],
  exports: [RouterModule],
})
export class InvestorRoutingModule {}
