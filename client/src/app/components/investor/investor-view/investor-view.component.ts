import { itemPluralMapping } from '../../../common/utils/item-plural-mapping';
import { CalculatedLoanDetailsDTO } from './../../../models/loan-models/calculated-loan-details.dto';
import { InvestmentDTO } from './../../../models/investment-models/investment.dto';
import { Component, OnInit, Input, SimpleChanges, OnChanges, Output, EventEmitter } from '@angular/core';
import { LoanWithPaymentsDTO } from '../../../models/loan-models/loan-with-payments.dto';
import { calculateLoanDetails } from '../../../common/utils/calculate-loan-details';
import { StatusLoan } from '../../../common/enums/status-loan.enum';
import { BaseUserDTO } from '../../../models/user-models/base-user.dto';

@Component({
  selector: 'app-investor-view',
  templateUrl: './investor-view.component.html',
  styleUrls: ['./investor-view.component.css'],
})
export class InvestorViewComponent implements OnInit, OnChanges {
  loggedUser: BaseUserDTO;
  @Input() investment: InvestmentDTO;
  @Input() isCollapsed;

  @Output() showLoan = new EventEmitter();

  protected readonly itemPluralMapping = itemPluralMapping;
  protected readonly highlightColor = '#fc9a4aa3';

  loan: LoanWithPaymentsDTO;
  calculatedLoan: CalculatedLoanDetailsDTO;

  ngOnInit() {
    if (this.investment.status !== 0) {
      this.investment.loans.filter(x => x.status !== StatusLoan.Completed).map(loan => (this.loan = loan));
      this.calculatedLoan = calculateLoanDetails(this.loan);
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    this.isCollapsed = changes.isCollapsed;
  }

  toggleDetails() {
    this.isCollapsed = !this.isCollapsed;
  }

  private checkIfLoanIsOverDue() {
    if (this.calculatedLoan) {
      return !!this.calculatedLoan.overdueDays;
    }
    return false;
  }
}
