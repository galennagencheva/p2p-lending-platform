import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { switchMap, take } from 'rxjs/operators';
import { AuthService } from '../../../core/services/auth.service';
import { InvestmentsService } from '../../../core/services/investments.service';
import { InvestmentDTO } from '../../../models/investment-models/investment.dto';

@Injectable({
  providedIn: 'root',
})
export class InvestorResolverService implements Resolve<any> {
  constructor(private readonly investmentsService: InvestmentsService, private readonly authService: AuthService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<InvestmentDTO[]> {
    return this.authService.loggedUser$.pipe(
      switchMap(loggedUser => this.investmentsService.getAllInvestmentsPerUser(loggedUser.id)),
      take(1),
    );
  }
}
