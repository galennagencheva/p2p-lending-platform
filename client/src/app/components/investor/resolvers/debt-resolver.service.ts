import { Resolve, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { DebtsService } from '../../../core/services/debts.service';
import { BaseDebtDTO } from '../../../models/debt-models/base-debt.dto';

@Injectable({
  providedIn: 'root',
})
export class DebtResolverService implements Resolve<BaseDebtDTO[]> {
  constructor(private readonly router: Router, private readonly debtsService: DebtsService) {}

  resolve() {
    return this.debtsService.getAllDebts().pipe(
      map(debts => {
        if (debts) {
          return debts;
        } else {
          this.router.navigate(['/dashboard']);
          return;
        }
      }),
    );
  }
}
