import { ActivatedRoute } from '@angular/router';
import { InvestorMainViewComponent } from './investor-main-view.component';
import { of } from 'rxjs';
import { ComponentFixture, async, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { InvestorViewComponent } from '../investor-view/investor-view.component';
import { PendingDebtsComponent } from '../pending-debts/pending-debts.component';
import { CoreModule } from '../../../core/core.module';
import { BaseUserDTO } from '../../../models/user-models/base-user.dto';
import { SharedModule } from '../../../shared/shared.module';
import { AuthService } from '../../../core/services/auth.service';
import { InvestmentsService } from '../../../core/services/investments.service';

describe('InvestorMainViewComponent', () => {
  const user51: BaseUserDTO = {
    id: '5',
    username: 'user51',
    BanExpirationDate: null,
    balance: 1500,
    isBanned: false,
    roles: ['user'],
  };
  const testUser2: BaseUserDTO = {
    id: '2',
    username: 'testUser2',
    BanExpirationDate: null,
    balance: 1500,
    isBanned: false,
    roles: ['user'],
  };
  const MockActivatedRoute = {
    data: of({
      investments: [],
      debts: [],
    }),
  };

  let mockAuthService;
  let mockInvestmentsService;
  let debts;
  let investments;
  let fixture: ComponentFixture<InvestorMainViewComponent>;
  let component: InvestorMainViewComponent;

  beforeEach(async(() => {
    jest.clearAllMocks();
    // clear all spies and mocks

    debts = [];
    investments = [{ id: 1 }];

    (mockAuthService = {
      get loggedUser$() {
        return of();
      },
    }),
      (mockInvestmentsService = {
        createProposal() {},
      });

    TestBed.configureTestingModule({
      declarations: [
        InvestorViewComponent,
        InvestorMainViewComponent,
        PendingDebtsComponent,
      ],
      imports: [RouterTestingModule, SharedModule, CoreModule],
      providers: [AuthService, InvestmentsService],
    })
      .overrideProvider(ActivatedRoute, { useValue: MockActivatedRoute })
      .overrideProvider(InvestmentsService, {
        useValue: mockInvestmentsService,
      })
      .overrideProvider(AuthService, { useValue: mockAuthService })
      .compileComponents();

    fixture = TestBed.createComponent(InvestorMainViewComponent);
    component = fixture.componentInstance;
  }));

  it('should be defined', () => {
    // Arrange & Act & Assert
    expect(component).toBeDefined();
  });

  it('should be truthy', () => {
    // Arrange & Act & Assert
    expect(component).toBeTruthy();
  });

  it('should initialize correctly with the data passed from the resolver', () => {
    jest.spyOn(mockAuthService, 'loggedUser$', 'get').mockReturnValue(of(user51));
    investments = [];
    MockActivatedRoute.data = of({ debts, investments });

    component.ngOnInit();

    expect(component.investments).toEqual(investments);
  });

  it('should initialize correctly with the data passed from the resolver', () => {
    jest.spyOn(mockAuthService, 'loggedUser$', 'get').mockReturnValue(of(user51));

    MockActivatedRoute.data = of({ debts, investments });

    component.ngOnInit();

    expect(component.debts).toEqual(debts);
  });

  it('should initialize correctly with the data passed from the authService subscription', () => {
    jest.spyOn(mockAuthService, 'loggedUser$', 'get').mockReturnValue(of(user51));

    MockActivatedRoute.data = of({ debts, investments });

    component.ngOnInit();

    expect(component.loggedUser).toEqual(user51);
  });

  it('should correctly unsubscribe from the logged user subscription', () => {
    jest.spyOn(mockAuthService, 'loggedUser$', 'get').mockReturnValue(of(user51));

    MockActivatedRoute.data = of({ debts, investments });

    component.ngOnInit();
    component.ngOnDestroy();

    expect(component['userSubscription$'].closed).toBe(true);
  });

  it('should correctly add proposel from response at the start in array', done => {
    jest.spyOn(mockInvestmentsService, 'createProposal').mockReturnValue(of('proposel'));

    component.debts = debts;
    component.investments = investments;
    component.loggedUser = user51;

    (component as any).createProposal('proposel');

    expect(component.investments[1]).toEqual('proposel');

    done();
  });

  it('should correctly filter the incoming debts', done => {
    jest.spyOn(mockAuthService, 'loggedUser$', 'get').mockReturnValue(of(user51));
    debts = [
      { status: 'Pending', user: user51 },
      { status: 'Pending', user: { username: 'asasasasas' } },
    ];
    MockActivatedRoute.data = of({ debts, investments });

    component.ngOnInit();

    expect(component.debts).toEqual([{ status: 'Pending', user: { username: 'asasasasas' } }]);

    done();
  });

  describe('createProposel()', () => {
    it('should create proposel with the correct data', () => {
      investments = [{ id: '1' }];
      debts = [{ id: '1', user: testUser2, status: 'Pending' }];
      MockActivatedRoute.data = of({ debts, investments });

      component.loggedUser = user51;
      component.ngOnInit();

      const investmentToPropose = {
        amount: 1233,
        interest: 12,
        overdueInterest: 12,
        period: 12,
      };
      const debtId = '2';

      const spy = jest.spyOn(mockInvestmentsService, 'createProposal').mockReturnValue(of('proposal'));
      component.createProposal(investmentToPropose, debtId);
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith('5', debtId, investmentToPropose);
    });

    it('should make isCollapsed to be false', () => {
      investments = [{ id: '1' }];
      debts = [{ id: '1', user: testUser2, status: 'Pending' }];
      MockActivatedRoute.data = of({ debts, investments });
      component.loggedUser = user51;
      component.ngOnInit();
      component.isCollapsed = true;

      const investmentToPropose = {
        amount: 1233,
        interest: 12,
        overdueInterest: 12,
        period: 12,
      };

      const debtId = '2';

      jest.spyOn(mockInvestmentsService, 'createProposal').mockReturnValue(of('proposal'));
      component.createProposal(investmentToPropose, debtId);
      expect(component.isCollapsed).toBe(false);
    });
  });
});
