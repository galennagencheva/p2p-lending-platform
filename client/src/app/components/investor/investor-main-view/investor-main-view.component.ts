import { CreateInvestmentDTO } from './../../../models/investment-models/create-investment.dto';
import { AuthService } from '../../../core/services/auth.service';
import { InvestmentsService } from '../../../core/services/investments.service';
import { BaseDebtDTO } from '../../../models/debt-models/base-debt.dto';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { InvestmentDTO } from '../../../models/investment-models/investment.dto';
import { BaseUserDTO } from '../../../models/user-models/base-user.dto';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { StatusDebt } from '../../../common/enums/status-debt.enum';

@Component({
  selector: 'app-investor-main-view',
  templateUrl: './investor-main-view.component.html',
  styleUrls: ['./investor-main-view.component.css'],
})
export class InvestorMainViewComponent implements OnInit, OnDestroy {
  debts: BaseDebtDTO[];
  investments: InvestmentDTO[];
  loggedUser: BaseUserDTO;
  debtUser: string;
  debtId: string;
  isCollapsed = true;
  private userSubscription$: Subscription;
  constructor(
    private readonly route: ActivatedRoute,
    private readonly investmentsService: InvestmentsService,
    private readonly authService: AuthService,
  ) {}

  ngOnInit() {
    this.userSubscription$ = this.authService.loggedUser$.subscribe(user => (this.loggedUser = user));

    this.route.data.subscribe(({ debts, investments }) => {
      this.debts = debts
        .filter(debt => debt.status !== StatusDebt.Funded)
        .filter(debt => debt.user.username !== this.loggedUser.username);

      this.investments = investments.filter(investment => investment.status === 0 || investment.status === 1);
    });
  }

  ngOnDestroy() {
    if (this.userSubscription$) {
      this.userSubscription$.unsubscribe();
    }
  }

  createProposal(createProposal: CreateInvestmentDTO, debtId: string) {
    return this.investmentsService
      .createProposal(this.loggedUser.id, debtId, createProposal)
      .subscribe(proposedInvestment => {
        this.isCollapsed = !this.isCollapsed;
        this.investments.push(proposedInvestment);
      });
  }
}
