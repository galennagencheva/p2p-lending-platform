import { ModalModule } from 'ngx-bootstrap/modal';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { InvestorMainViewComponent } from './investor-main-view/investor-main-view.component';
import { InvestorRoutingModule } from './investor-routing.module';
import { SharedModule } from './../../shared/shared.module';
import { InvestorViewComponent } from './investor-view/investor-view.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PendingDebtsComponent } from './pending-debts/pending-debts.component';
import { DebtProposalModalComponent } from './pending-debts/debt-proposal-modal/debt-proposal-modal.component';

@NgModule({
  declarations: [
    InvestorViewComponent,
    InvestorMainViewComponent,
    PendingDebtsComponent,
    DebtProposalModalComponent,
  ],
  imports: [CommonModule, SharedModule, InvestorRoutingModule, ModalModule.forRoot()],
  providers: [BsModalRef, BsModalService],
  entryComponents: [DebtProposalModalComponent],
})
export class InvestorModule {}
