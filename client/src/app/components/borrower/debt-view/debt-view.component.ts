import { InvestmentsModalComponent } from './investments-modal/investments-modal.component';
import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { DebtDTO } from '../../../models/debt-models/debt.dto';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { BaseInvestmentDTO } from '../../../models/investment-models/base-investment.dto';
import { Subscription } from 'rxjs';
import { faTrash } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-debt-view',
  templateUrl: './debt-view.component.html',
  styleUrls: ['./debt-view.component.css'],
})
export class DebtViewComponent implements OnInit, OnDestroy {
  @Input() debt: DebtDTO;
  pendingInvestments: BaseInvestmentDTO[];
  remainingAmount: number;
  isDebtPartial = false;
  bsModalRef: BsModalRef;

  faTrash = faTrash;

  protected readonly highlightColor = '#26695599';

  acceptedInvestmentSubscription$: Subscription;
  declinedInvestmentSubscription$: Subscription;

  @Output() acceptInvestment = new EventEmitter();
  @Output() declineInvestment = new EventEmitter();
  @Output() deleteDebt = new EventEmitter();

  constructor(private modalService: BsModalService) {}

  ngOnInit() {
    const acceptedInvestments = this.debt.investments.filter(eachInvestment => eachInvestment.status === 1);
    const totalSumOfAcceptedInvestments = acceptedInvestments.reduce((acc, investment) => {
      return acc + investment.amount;
    }, 0);
    this.remainingAmount = this.debt.amount - totalSumOfAcceptedInvestments;

    if (this.debt.isPartial) {
      this.isDebtPartial = true;
    }

    this.pendingInvestments = this.debt.investments
      .filter(eachInvestment => eachInvestment.status === 0 && this.remainingAmount - eachInvestment.amount >= 0)
      .filter(investment => investment.amount < investment.user.balance);
  }

  ngOnDestroy() {
    if (this.acceptedInvestmentSubscription$) {
      this.acceptedInvestmentSubscription$.unsubscribe();
    }

    if (this.declinedInvestmentSubscription$) {
      this.declinedInvestmentSubscription$.unsubscribe();
    }
  }

  openModalWithComponent() {
    const initialState = {
      pendingInvestments: this.pendingInvestments,
    };

    this.bsModalRef = this.modalService.show(InvestmentsModalComponent, {
      initialState,
    });

    this.acceptedInvestmentSubscription$ = this.bsModalRef.content.accept.subscribe((investmentId: string) => {
      const acceptedInvestment = this.pendingInvestments.find(investment => investment.id === investmentId);
      const indexOfAcceptedInvestment = this.pendingInvestments.findIndex(investment => investment.id === investmentId);
      this.pendingInvestments.splice(indexOfAcceptedInvestment, 1);
      this.remainingAmount = this.remainingAmount - acceptedInvestment.amount;

      this.acceptInvestment.emit({
        investmentId,
        debtId: this.debt.id,
        remainingAmountOfTheDebt: this.remainingAmount,
      });

      if (this.debt.isPartial) {
        this.pendingInvestments.map(eachInvestment => {
          if (this.remainingAmount - eachInvestment.amount <= 0) {
            const indexOfTheInvestmentWithBiggerAmount = this.pendingInvestments.findIndex(
              investment => investment.id === eachInvestment.id,
            );
            this.pendingInvestments.splice(indexOfTheInvestmentWithBiggerAmount, 1);
          }
        });
      }

      if (!this.debt.isPartial) {
        this.pendingInvestments.length = 0;
      }
    });

    this.declinedInvestmentSubscription$ = this.bsModalRef.content.decline.subscribe((investmentId: string) => {
      this.declineInvestment.emit(investmentId);
      const index = this.pendingInvestments.findIndex(investment => investmentId === investment.id);
      this.pendingInvestments.splice(index, 1);
    });
  }

  deleteSelectedDebt() {
    this.deleteDebt.emit(this.debt.id);
  }
}
