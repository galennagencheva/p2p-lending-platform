import { BaseInvestmentDTO } from './../../../../models/investment-models/base-investment.dto';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-investments-modal',
  templateUrl: './investments-modal.component.html',
  styleUrls: ['./investments-modal.component.css'],
})
export class InvestmentsModalComponent implements OnInit {
  @Input() pendingInvestments: BaseInvestmentDTO[];
  @Output() accept = new EventEmitter<string>();
  @Output() decline = new EventEmitter<string>();

  constructor(public bsModalRef: BsModalRef) {}

  ngOnInit() {
    this.pendingInvestments.filter(investment => investment.amount < investment.user.balance);
  }

  acceptInvestment(investmentId: string) {
    this.accept.emit(investmentId);
  }

  declineInvestment(investmentId: string) {
    this.decline.emit(investmentId);
  }

  private investmentsExist() {
    return this.pendingInvestments && this.pendingInvestments.length;
  }
}
