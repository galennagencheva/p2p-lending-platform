import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BorrowerViewComponent } from './borrower-view/borrower-view.component';
import { BorrowerRoutingModule } from './borrower-routing.module';
import { CreateDebtRequestComponent } from './create-debt-request/create-debt-request.component';
import { DebtViewComponent } from './debt-view/debt-view.component';
import { LoanViewComponent } from './loan-view/loan-view.component';
import { InvestmentsModalComponent } from './debt-view/investments-modal/investments-modal.component';
import { ModalModule, BsModalRef } from 'ngx-bootstrap/modal';
import { LoanDetailsModalComponent } from './loan-view/loan-details-modal/loan-details-modal.component';
import { LoanProblemModalComponent } from './loan-view/loan-problem-modal/loan-problem-modal.component';

@NgModule({
  declarations: [
    BorrowerViewComponent,
    DebtViewComponent,
    LoanViewComponent,
    CreateDebtRequestComponent,
    InvestmentsModalComponent,
    LoanDetailsModalComponent,
    LoanProblemModalComponent,
  ],
  imports: [CommonModule, SharedModule, BorrowerRoutingModule, ModalModule.forRoot()],
  providers: [BsModalRef],
  entryComponents: [InvestmentsModalComponent, LoanDetailsModalComponent, LoanProblemModalComponent],
})
export class BorrowerModule {}
