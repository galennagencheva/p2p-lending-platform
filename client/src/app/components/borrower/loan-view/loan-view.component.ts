import { NotificationService } from './../../../core/services/notification.service';
import { EmitProblemDTO } from './../../../models/problem-models/emit-problem.dto';
import { CalculatedLoanDetailsDTO } from './../../../models/loan-models/calculated-loan-details.dto';
import { LoanDetailsModalComponent } from './loan-details-modal/loan-details-modal.component';
import { EmitPaymentDTO } from './../../../models/payment-models/emit-payment.dto';
import { LoanDTO } from '../../../models/loan-models/loan.dto';
import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { CreatePaymentDTO } from '../../../models/payment-models/create-payment.dto';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Subscription } from 'rxjs';
import { calculateLoanDetails } from '../../../common/utils/calculate-loan-details';
import { LoanProblemModalComponent } from './loan-problem-modal/loan-problem-modal.component';
import { CreateProblemDTO } from '../../../models/problem-models/create-problem.dto';
import { UsersService } from '../../../core/services/users.service';

@Component({
  selector: 'app-loan-view',
  templateUrl: './loan-view.component.html',
  styleUrls: ['./loan-view.component.css'],
})
export class LoanViewComponent implements OnInit, OnDestroy {
  @Input() loan: LoanDTO;
  @Input() triggerAnimation: boolean;

  userBalance: number;
  calculatedLoan: CalculatedLoanDetailsDTO;

  loanDetailsModal: BsModalRef;
  reportProblemModal: BsModalRef;

  @Output() payMonthlyInstallment = new EventEmitter<EmitPaymentDTO>();
  @Output() reportProblem = new EventEmitter();

  protected readonly highlightColor = '#fc9a4aa3';
  private problemSubscription$: Subscription;

  constructor(
    private readonly bsModalService: BsModalService,
    private readonly usersService: UsersService,
    private readonly notificationService: NotificationService,
  ) {}

  ngOnInit() {
    this.calculatedLoan = calculateLoanDetails(this.loan);
    this.usersService.loggedUserInfo$.subscribe(loggedUserInfo => {
      if (loggedUserInfo !== null) {
        this.userBalance = loggedUserInfo.balance;
      }
    });
  }

  ngOnDestroy() {
    if (this.problemSubscription$) {
      this.problemSubscription$.unsubscribe();
    }
  }

  makePayment() {
    const newPayment: CreatePaymentDTO = {
      amount: this.calculatedLoan.monthlyInstallment,
      overdue: this.calculatedLoan.overdueDays,
      penaltyAmount: this.calculatedLoan.penaltyAmount,
    };

    const emitPayment: EmitPaymentDTO = {
      id: this.loan.id,
      newPayment,
    };

    if (this.userBalance < newPayment.amount + newPayment.penaltyAmount) {
      this.notificationService.error('Insufficient balance! Please top up!');
    } else {
      this.payMonthlyInstallment.emit(emitPayment);
    }
  }

  openLoanDetailsModal() {
    const initialState = {
      calculatedLoan: this.calculatedLoan,
    };
    this.loanDetailsModal = this.bsModalService.show(LoanDetailsModalComponent, { initialState });
  }

  openReportProblemModal() {
    this.reportProblemModal = this.bsModalService.show(LoanProblemModalComponent);
    this.problemSubscription$ = this.reportProblemModal.content.report.subscribe((createdProblem: CreateProblemDTO) => {
      const problem: EmitProblemDTO = {
        loanId: this.loan.id,
        description: createdProblem,
      };

      this.reportProblem.emit(problem);
      this.reportProblemModal.hide();
    });
  }
}
