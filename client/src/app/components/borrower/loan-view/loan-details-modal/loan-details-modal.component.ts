import { CalculatedLoanDetailsDTO } from './../../../../models/loan-models/calculated-loan-details.dto';
import { BsModalRef } from 'ngx-bootstrap';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-loan-details-modal',
  templateUrl: './loan-details-modal.component.html',
  styleUrls: ['./loan-details-modal.component.css'],
})
export class LoanDetailsModalComponent implements OnInit {
  @Input() calculatedLoan: CalculatedLoanDetailsDTO;

  constructor(protected loanDetailsModal: BsModalRef) {}

  ngOnInit() {}
}
