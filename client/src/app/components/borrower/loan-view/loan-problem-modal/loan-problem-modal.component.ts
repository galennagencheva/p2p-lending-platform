import { BsModalRef } from 'ngx-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-loan-problem-modal',
  templateUrl: './loan-problem-modal.component.html',
  styleUrls: ['./loan-problem-modal.component.css'],
})
export class LoanProblemModalComponent implements OnInit {
  reportProblemForm: FormGroup;

  @Output() report = new EventEmitter();

  constructor(private readonly fb: FormBuilder, protected readonly loanProblemModal: BsModalRef) {
    this.reportProblemForm = this.fb.group({
      description: ['', Validators.compose([Validators.required, Validators.minLength(10)])],
    });
  }

  ngOnInit() {}

  reportProblem() {
    this.report.emit(this.reportProblemForm.value);
  }
}
