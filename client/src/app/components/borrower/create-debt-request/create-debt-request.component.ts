import { itemPluralMapping } from '../../../common/utils/item-plural-mapping';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-create-debt-request',
  templateUrl: './create-debt-request.component.html',
  styleUrls: ['./create-debt-request.component.css'],
})
export class CreateDebtRequestComponent implements OnInit {
  faPlusCircle = faPlusCircle;
  createDebtForm: FormGroup;

  protected itemPluralMapping = itemPluralMapping;

  @Output() create = new EventEmitter();

  constructor(private readonly fb: FormBuilder) {
    this.createDebtForm = this.fb.group({
      amount: [100, Validators.compose([Validators.required, Validators.min(100)])],
      period: [1, Validators.compose([Validators.required, Validators.min(1)])],
      isPartial: [false],
    });
  }

  ngOnInit() {}

  createDebt() {
    this.create.emit(this.createDebtForm.value);
  }
}
