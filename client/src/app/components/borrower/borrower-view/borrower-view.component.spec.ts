import { StatusLoan } from './../../../common/enums/status-loan.enum';
import { ProblemsService } from './../../../core/services/problems.service';
import { LoansService } from './../../../core/services/loans.service';
import { PaymentsService } from './../../../core/services/payments.service';
import { DebtsService } from './../../../core/services/debts.service';
import { ModalModule, BsModalRef } from 'ngx-bootstrap/modal';
import { BorrowerViewComponent } from './borrower-view.component';
import { of } from 'rxjs';
import { async, TestBed, ComponentFixture } from '@angular/core/testing';
import { SharedModule } from '../../../shared/shared.module';
import { AuthService } from '../../../core/services/auth.service';
import { NotificationService } from '../../../core/services/notification.service';
import { ActivatedRoute } from '@angular/router';
import { LoanDetailsModalComponent } from '../loan-view/loan-details-modal/loan-details-modal.component';
import { DebtViewComponent } from '../debt-view/debt-view.component';
import { LoanViewComponent } from '../loan-view/loan-view.component';
import { CreateDebtRequestComponent } from '../create-debt-request/create-debt-request.component';
import { InvestmentsModalComponent } from '../debt-view/investments-modal/investments-modal.component';
import { LoanProblemModalComponent } from '../loan-view/loan-problem-modal/loan-problem-modal.component';
import { RouterTestingModule } from '@angular/router/testing';
import { InvestmentsService } from '../../../core/services/investments.service';
import { StatusInvestment } from '../../../common/enums/status-investment.enum';

describe('Borrower View Component', () => {
  const route = {
    data: of({
      debts: [],
      loans: [],
    }),
  };

  let fixture: ComponentFixture<BorrowerViewComponent>;
  let component: BorrowerViewComponent;
  let debts;
  let loans;
  let id;
  let debtsService;
  let paymentsService;
  let investmentsService;
  let loansService;
  let notificationService;
  let problemsService;
  let authService;
  let loggedUser;
  beforeEach(async(() => {
    jest.clearAllMocks();

    id = '1';
    loggedUser = {
      id: '1',
    };

    debts = [];
    loans = [];

    debtsService = {
      createDebt() {},
    };

    paymentsService = {
      createPayment() {},
    };

    investmentsService = {
      updateInvestment() {},
    };

    loansService = {
      createLoan() {},
      updateInvestment() {},
    };

    problemsService = {
      createProblem() {},
    };

    notificationService = {
      success() {},
      error() {},
    };

    authService = {
      get loggedUser$() {
        return of();
      },
    };

    TestBed.configureTestingModule({
      imports: [SharedModule, RouterTestingModule, ModalModule],
      declarations: [
        BorrowerViewComponent,
        DebtViewComponent,
        LoanViewComponent,
        CreateDebtRequestComponent,
        InvestmentsModalComponent,
        LoanDetailsModalComponent,
        LoanProblemModalComponent,
      ],
      providers: [
        BsModalRef,
        AuthService,
        DebtsService,
        PaymentsService,
        LoansService,
        ProblemsService,
        NotificationService,
        InvestmentsService,
      ],
    })
      .overrideProvider(AuthService, { useValue: authService })
      .overrideProvider(ActivatedRoute, { useValue: route })
      .overrideProvider(DebtsService, { useValue: debtsService })
      .overrideProvider(PaymentsService, { useValue: paymentsService })
      .overrideProvider(LoansService, { useValue: loansService })
      .overrideProvider(ProblemsService, { useValue: problemsService })
      .overrideProvider(NotificationService, { useValue: notificationService })
      .overrideProvider(InvestmentsService, { useValue: investmentsService })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(BorrowerViewComponent);
        component = fixture.componentInstance;
        // fixture.detectChanges();
      });
  }));

  it('should be defined', () => {
    // Arrange & Act & Assert
    expect(component).toBeDefined();
  });

  it('should be defined', () => {
    // Arrange & Act & Assert
    expect(component).toBeTruthy();
  });

  it('should initialize correctly with the debts data passed from the resolver', () => {
    jest.spyOn(authService, 'loggedUser$', 'get').mockReturnValue(of(loggedUser));

    route.data = of({ debts, loans });

    // Act
    component.ngOnInit();

    // Assert
    expect(component.debts).toBe(debts);
  });

  it('should initialize correctly with the loans data passed from the resolver', () => {
    jest.spyOn(authService, 'loggedUser$', 'get').mockReturnValue(of(loggedUser));

    route.data = of({ debts, loans });

    // Act
    component.ngOnInit();

    // Assert
    expect(component.loans).toBe(loans);
  });

  it('should initialize correctly with the data passed from the authService subscription', () => {
    // Arrange
    jest.spyOn(authService, 'loggedUser$', 'get').mockReturnValue(of(loggedUser));

    route.data = of({ loans, debts });
    // Act
    component.ngOnInit();

    // Assert
    expect(component.loggedUserId).toBe(loggedUser.id);
  });

  it('should correctly unsubscribe from the logged user subscription', () => {
    // Arrange
    jest.spyOn(authService, 'loggedUser$', 'get').mockReturnValue(of(loggedUser));

    route.data = of({ loans, debts });

    // Act
    component.ngOnInit();
    component.ngOnDestroy();

    // Assert
    expect(component.loggedUserSubscription$.closed).toBe(true);
  });

  describe('createDebt()', () => {
    it('should call the debtsService.createDebt() with with the right arguments', () => {
      // Arrange
      const spy = jest.spyOn(debtsService, 'createDebt').mockReturnValue(of('debt'));

      component.debts = debts;
      component.loggedUserId = id;

      // Act
      (component as any).createDebt('debt');

      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(id, 'debt');
    });

    it('should correctly add newDebt from response at the start in array', done => {
      // Arrange
      jest.spyOn(debtsService, 'createDebt').mockReturnValue(of('debt'));

      component.debts = debts;
      component.loggedUserId = id;

      // Act
      (component as any).createDebt('debt');

      // Assert
      expect(component.debts[0]).toEqual('debt');

      done();
    });

    it('should call notificationService.success() once with correct parameters', done => {
      // Arrange
      jest.spyOn(debtsService, 'createDebt').mockReturnValue(of('debt'));

      const spy = jest.spyOn(notificationService, 'success');

      component.debts = debts;
      component.loggedUserId = id;

      // Act
      component.createDebt('debt' as any);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith('Debt successfully created!');

      done();
    });
  });

  describe('makePayment()', () => {
    it('should call paymentsService.createPayment() once with correct parameters', done => {
      // Arrange
      const emittedPayment: any = { id: '2', newPayment: 'newPayment' };

      const spy = jest.spyOn(paymentsService, 'createPayment').mockImplementation(() => of('updatedLoan'));

      component.loggedUserId = '1';
      component.loans = loans;

      // Act
      component.makePayment(emittedPayment);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith('1', '2', 'newPayment');

      done();
    });

    it('should correctly replace the updated loan from response if the loan is still ongoing', done => {
      // Arrange
      const emittedPayment: any = { id: '2', newPayment: 'newPayment' };
      const updatedLoan = { id: '1', status: StatusLoan.Ongoing };
      const oldLoan: any = { id: '1', status: StatusLoan.Ongoing };

      jest.spyOn(paymentsService, 'createPayment').mockImplementation(() => of(updatedLoan));

      component.loggedUserId = '1';
      component.loans = [oldLoan];

      // Act
      component.makePayment(emittedPayment);

      // Assert
      expect(component.loans[0]).toEqual(updatedLoan);

      done();
    });

    it('should splice the updated loan from response if the loan is completed', done => {
      // Arrange
      const emittedPayment: any = { id: '2', newPayment: 'newPayment' };
      const updatedLoan = { id: '1', status: StatusLoan.Completed };
      const oldLoan: any = { id: '1', status: StatusLoan.Ongoing };

      jest.spyOn(paymentsService, 'createPayment').mockImplementation(() => of(updatedLoan));

      component.loggedUserId = '1';
      component.loans = [oldLoan];

      // Act
      component.makePayment(emittedPayment);

      // Assert
      expect(component.loans).toEqual([]);

      done();
    });

    it('should call notificationService.success() once with the correct parameters when the loan is still ongoing', done => {
      // Arrange
      const emittedPayment = { id: '2', newPayment: 'newPayment' };
      const updatedLoan = { id: '1', status: StatusLoan.Ongoing };
      const oldLoan: any = { id: '1', status: StatusLoan.Ongoing };
      const spy = jest.spyOn(notificationService, 'success');

      jest.spyOn(paymentsService, 'createPayment').mockImplementation(() => of(updatedLoan));

      component.loggedUserId = '1';
      component.loans = [oldLoan];
      component.debts = [debts];


      // Act
      (component as any).makePayment(emittedPayment);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith('Your payment has been proccessed!');
      done();
    });

    it('should call notificationService.success() once with the correct parameters when the loan is completed', done => {
      // Arrange
      const emittedPayment: any = { id: '2', newPayment: 'newPayment' };
      const updatedLoan = { id: '1', status: StatusLoan.Completed };
      const oldLoan: any = { id: '1', status: StatusLoan.Ongoing };
      const spy = jest.spyOn(notificationService, 'success');

      jest.spyOn(paymentsService, 'createPayment').mockImplementation(() => of(updatedLoan));

      component.loggedUserId = '1';
      component.loans = [oldLoan];

      // Act
      component.makePayment(emittedPayment);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith('That was the last payment for this loan. Debt was successfully payed!');
      done();
    });
  });

  describe('createLoan()', () => {
    it('should call loansService.createLoan() once with correct parameters', done => {
      // Arrange
      const debtsArray: any = [{id: '2'}];

      const spy = jest.spyOn(loansService, 'createLoan').mockImplementation(() => of('createdLoan'));

      component.loggedUserId = '1';
      component.loans = loans;
      component.debts = debtsArray;

      // Act
      component.createLoan({ investmentId: '1', debtId: '2' });

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith('1', '2', '1');

      done();
    });

    it('should correctly push the created loan from response to the loans array', done => {
      // Arrange
      const debtsArray: any = [{id: '2'}];
      jest.spyOn(loansService, 'createLoan').mockImplementation(() => of('createdLoan'));

      component.loggedUserId = '1';
      component.loans = loans;
      component.debts = debtsArray;

      // Act
      (component as any).createLoan({ investmentId: '1', debtId: '2' });

      // Assert
      expect(component.loans as any).toContain('createdLoan');
      expect(component.loans[0]).toEqual('createdLoan');

      done();
    });

    it('should call notificationService.success() once with the correct parameters when the response is successful', done => {
      // Arrange
      const debtsArray: any = [{id: '2'}];
      const spy = jest.spyOn(notificationService, 'success');
      jest.spyOn(loansService, 'createLoan').mockImplementation(() => of('createdLoan'));

      component.loggedUserId = '1';
      component.loans = loans;
      component.debts = debtsArray;

      // Act
      (component as any).createLoan({ investmentId: '1', debtId: '2' });

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith('Investment accepted!');
      done();
    });
  });

  describe('declineInvestment()', () => {
    it('should call investmentsService.updateInvestment() once with correct parameters', done => {
      // Arrange
      const status = { status: StatusInvestment.Declined };
      const spy = jest.spyOn(investmentsService, 'updateInvestment').mockImplementation(() => of('_'));

      component.loggedUserId = '1';

      // Act
      component.declineInvestment(id);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith('1', id, status);

      done();
    });

    it('should call notificationService.success() once with the correct parameters when the response is successful', done => {
      // Arrange
      const status = { status: StatusInvestment.Declined };
      const spy = jest.spyOn(notificationService, 'success');
      jest.spyOn(investmentsService, 'updateInvestment').mockImplementation(() => of('_'));

      component.loggedUserId = '1';

      // Act
      component.declineInvestment(id);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith('Investment declined!');
      done();
    });
  });

  describe('reportProblem()', () => {
    it('should call problemsService.createProblem() once with correct parameters', done => {
      // Arrange
      const reportedProblem = { loanId: '1', description: 'description' };
      const spy = jest.spyOn(problemsService, 'createProblem').mockImplementation(() => of('_'));

      component.loggedUserId = '1';

      // Act
      (component as any).reportProblem(reportedProblem);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith('1', reportedProblem.loanId, reportedProblem.description);

      done();
    });

    it('should call notificationService.success() once with the correct parameters when the response is successful', done => {
      // Arrange
      const spy = jest.spyOn(notificationService, 'success');
      const reportedProblem = { loanId: '1', description: 'description' };
      jest.spyOn(problemsService, 'createProblem').mockImplementation(() => of('_'));

      component.loggedUserId = '1';

      // Act
      (component as any).reportProblem(reportedProblem);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith('Problem reported!');
      done();
    });
  });
});
