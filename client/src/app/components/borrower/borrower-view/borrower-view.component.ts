import { StatusDebt } from './../../../common/enums/status-debt.enum';
import { EmitProblemDTO } from './../../../models/problem-models/emit-problem.dto';
import { StatusLoan } from './../../../common/enums/status-loan.enum';
import { EmitPaymentDTO } from './../../../models/payment-models/emit-payment.dto';
import { Subscription } from 'rxjs';
import { CreateDebtDTO } from './../../../models/debt-models/create-debt.dto';
import { DebtsService } from './../../../core/services/debts.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { DebtDTO } from '../../../models/debt-models/debt.dto';
import { LoanDTO } from '../../../models/loan-models/loan.dto';
import { PaymentsService } from '../../../core/services/payments.service';
import { AuthService } from '../../../core/services/auth.service';
import { LoansService } from '../../../core/services/loans.service';
import { StatusInvestment } from '../../../common/enums/status-investment.enum';
import { NotificationService } from '../../../core/services/notification.service';
import { InvestmentsService } from '../../../core/services/investments.service';
import { ProblemsService } from '../../../core/services/problems.service';

@Component({
  selector: 'app-borrower-view',
  templateUrl: './borrower-view.component.html',
  styleUrls: ['./borrower-view.component.css'],
})
export class BorrowerViewComponent implements OnInit, OnDestroy {
  debts: DebtDTO[];
  loans: LoanDTO[];
  loggedUserId: string;
  triggerAnimation = false;

  loggedUserSubscription$: Subscription;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly authService: AuthService,
    private readonly debtsService: DebtsService,
    private readonly paymentsService: PaymentsService,
    private readonly loansService: LoansService,
    private readonly notificationService: NotificationService,
    private readonly problemsService: ProblemsService,
    private readonly investmentsService: InvestmentsService,
  ) {}

  ngOnInit() {
    this.route.data.subscribe(({ debts, loans }) => {
      this.debts = debts;
      this.loans = loans;
    });
    this.loggedUserSubscription$ = this.authService.loggedUser$.subscribe(loggedUser => {
      if (loggedUser !== null) {
        this.loggedUserId = loggedUser.id;
      }
    });
  }

  ngOnDestroy() {
    if (this.loggedUserSubscription$) {
      this.loggedUserSubscription$.unsubscribe();
    }
  }

  createDebt(createdDebt: CreateDebtDTO) {
    this.debtsService.createDebt(this.loggedUserId, createdDebt).subscribe(newDebt => {
      this.debts.unshift(newDebt);
      this.notificationService.success('Debt successfully created!');
    });
  }

  deleteDebt(debtId: string) {
    this.debtsService.deleteDebt(this.loggedUserId, debtId).subscribe(deletedDebt => {
      const index: number = this.debts.findIndex(debt => debt.id === deletedDebt.id);
      this.debts.splice(index, 1);
      this.notificationService.success('Debt Deleted!');
    });
  }

  makePayment(emittedPayment: EmitPaymentDTO) {
    const { id, newPayment } = emittedPayment;
    this.paymentsService.createPayment(this.loggedUserId, id, newPayment).subscribe(updatedLoan => {
      const index: number = this.loans.findIndex(eachLoan => eachLoan.id === updatedLoan.id);

      if (updatedLoan.status === StatusLoan.Completed) {
        this.loans.splice(index, 1);
        this.notificationService.success('That was the last payment for this loan. Debt was successfully payed!');
      } else {
        this.loans[index] = updatedLoan;
        this.notificationService.success('Your payment has been proccessed!');
      }

      this.triggerAnimation = true;
    });
  }

  createLoan(createdLoanNeededInfo) {
    const { investmentId, debtId, remainingAmountOfTheDebt } = createdLoanNeededInfo;
    this.loansService.createLoan(this.loggedUserId, debtId, investmentId).subscribe(createdLoan => {
      this.loans.push(createdLoan);
      this.notificationService.success('Investment accepted!');

      const index: number = this.debts.findIndex(debt => debt.id === debtId);
      const foundDebt = this.debts.find(debt => debt.id === debtId);

      if (foundDebt.isPartial === true) {
        if (remainingAmountOfTheDebt > 0) {
          foundDebt.status = StatusDebt.PartiallyFunded;
          this.debts[index] = foundDebt;
        } else {
          this.debts.splice(index, 1);
        }
      }

      if (foundDebt.isPartial === false) {
        this.debts.splice(index, 1);
      }
    });
  }

  declineInvestment(investmentId: string) {
    this.investmentsService
      .updateInvestment(this.loggedUserId, investmentId, { status: StatusInvestment.Declined })
      .subscribe(_ => {
        this.notificationService.success('Investment declined!');
      });
  }

  reportProblem(problem: EmitProblemDTO) {
    this.problemsService.createProblem(this.loggedUserId, problem.loanId, problem.description).subscribe(_ => {
      this.notificationService.success('Problem reported!');
    });
  }
}
