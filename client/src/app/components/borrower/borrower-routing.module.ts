import { UsersGuard } from './../../auth/users.guard';
import { LoansResolverService } from './resolvers/loans-resolver.service';
import { BorrowerViewComponent } from './borrower-view/borrower-view.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DebtsResolverService } from './resolvers/debts-resolver.service';
import { AdminGuard } from '../../auth/admin.guard';
import { AuthGuard } from '../../auth/auth.guard';

const borrowerRoutes: Routes = [
  {
    path: '',
    component: BorrowerViewComponent,
    resolve: {
      debts: DebtsResolverService,
      loans: LoansResolverService,
    },
    canActivate: [UsersGuard, AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(borrowerRoutes)],
  exports: [RouterModule],
})
export class BorrowerRoutingModule {}
