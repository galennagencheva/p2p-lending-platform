import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { map, switchMap, first, take } from 'rxjs/operators';
import { AuthService } from '../../../core/services/auth.service';
import { LoansService } from '../../../core/services/loans.service';
import { LoanDTO } from '../../../models/loan-models/loan.dto';

@Injectable({
  providedIn: 'root',
})
export class LoansResolverService implements Resolve<any> {
  constructor(private readonly loansService: LoansService, private readonly authService: AuthService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<LoanDTO[]> {
    return this.authService.loggedUser$.pipe(
      switchMap(loggedUser => this.loansService.getAllLoansPerUser(loggedUser.id)),
      take(1),
    );
  }
}
