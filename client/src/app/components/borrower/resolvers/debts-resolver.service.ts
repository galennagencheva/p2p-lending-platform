import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { switchMap, take } from 'rxjs/operators';
import { DebtDTO } from '../../../models/debt-models/debt.dto';
import { AuthService } from '../../../core/services/auth.service';
import { DebtsService } from '../../../core/services/debts.service';

@Injectable({
  providedIn: 'root',
})
export class DebtsResolverService implements Resolve<any> {
  constructor(private readonly debtsService: DebtsService, private readonly authService: AuthService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<DebtDTO[]> {
    return this.authService.loggedUser$.pipe(
      switchMap(loggedUser => this.debtsService.getAllDebtsPerUser(loggedUser.id)),
      take(1),
    );
  }
}
