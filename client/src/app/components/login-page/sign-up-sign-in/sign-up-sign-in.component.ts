import { NotificationService } from './../../../core/services/notification.service';
import { AuthService } from './../../../core/services/auth.service';
import { OnInit, Component, OnDestroy } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-sign-up-sign-in',
  templateUrl: './sign-up-sign-in.component.html',
  styleUrls: ['./sign-up-sign-in.component.css'],
})
export class SignUpSignInComponent implements OnInit, OnDestroy {
  registerForm: FormGroup;
  loginForm: FormGroup;

  loggedUserSubscription$: Subscription;
  constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly notificator: NotificationService,
    private readonly fb: FormBuilder,
  ) {
    this.registerForm = this.fb.group({
      username: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      email: ['', Validators.compose([Validators.required, Validators.email])],

      password: ['', Validators.compose([Validators.required, Validators.minLength(8)])],
    });
    this.loginForm = this.fb.group({
      username: ['', Validators.compose([Validators.required, Validators.minLength(6)])],

      password: ['', Validators.compose([Validators.required, Validators.minLength(8)])],
    });
  }

  ngOnInit() {}

  ngOnDestroy() {
    if (this.loggedUserSubscription$) {
      this.loggedUserSubscription$.unsubscribe();
    }
  }

  login() {
    this.loggedUserSubscription$ = this.authService
      .login(this.loginForm.value)
      .pipe(switchMap(_ => this.authService.loggedUser$))
      .subscribe(decodedLoggedUser => {
        if (decodedLoggedUser !== null) {
          if (decodedLoggedUser.roles.includes('admin')) {
            this.router.navigate(['admin']);
          } else {
            this.router.navigate(['dashboard']);
          }
        }
      },
      (err) => this.notificator.error(err.error.error));
  }

  register() {
    this.authService.register(this.registerForm.value).subscribe(
      () => {
        this.notificator.success('You registered successfully!');
        const { username, password } = this.registerForm.value;
        this.authService.login({ username, password }).subscribe(_ => {
          this.router.navigate(['dashboard']);
        });
      },
      error => {
        this.notificator.error(error.error.error);
      },
    );
  }
}
