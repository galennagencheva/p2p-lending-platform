import { SharedModule } from './../../shared/shared.module';
import { SignUpSignInComponent } from './sign-up-sign-in/sign-up-sign-in.component';
import { LoginPageComponent } from './login-page.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [LoginPageComponent, SignUpSignInComponent],
  imports: [CommonModule, SharedModule],
  exports: [LoginPageComponent],
})
export class LoginPageModule {}
