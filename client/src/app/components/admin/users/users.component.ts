import { AuthService } from './../../../core/services/auth.service';
import { UsersService } from './../../../core/services/users.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { BaseUserDTO } from '../../../models/user-models/base-user.dto';
import { NotificationService } from '../../../core/services/notification.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
})
export class UsersComponent implements OnInit, OnDestroy {
  users: BaseUserDTO[];
  loggedUser: BaseUserDTO;
  days: number;
  loggedUserId: string;
  loggedUserSubscription$: Subscription;

  constructor(
    private readonly usersService: UsersService,
    private readonly authService: AuthService,
    private readonly notificationService: NotificationService,
  ) {}

  ngOnInit() {
    this.loggedUserSubscription$ = this.authService.loggedUser$.subscribe(user => (this.loggedUser = user));
    this.usersService.getAllUsers().subscribe(incomingUsers => {
      const index: number = incomingUsers.findIndex(user => user.id === this.loggedUser.id);
      incomingUsers.splice(index, 1);
      this.users = incomingUsers;
    });
  }

  deleteUser(userId: string) {
    this.usersService.deleteUser(userId).subscribe(deletedUser => {
      const index: number = this.users.findIndex(user1 => user1.id === userId);
      this.users.splice(index, 1);
      this.notificationService.success('User deleted!');
    });
  }

  banUser(userId: string) {
    const newDays = +this.days;
    this.usersService.banUser(userId, { numberOfDays: newDays }).subscribe(user => {
      const index: number = this.users.findIndex(eachUser => eachUser.id === user.id);
      this.users[index] = user;
      this.notificationService.success(`User banned for ${newDays} days.`);
    });
  }

  unBanUser(userId: string) {
    this.usersService.unBanUser(userId).subscribe(user => {
      const index: number = this.users.findIndex(eachUser => user.id === eachUser.id);
      this.users[index] = user;
      this.notificationService.success(`User unBanned`);
    });
  }

  ngOnDestroy(): void {
    if (this.loggedUserSubscription$) {
      this.loggedUserSubscription$.unsubscribe();
    }
  }
}
