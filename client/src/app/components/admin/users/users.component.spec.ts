import { TestBed, ComponentFixture, async } from '@angular/core/testing';
import { UsersComponent } from './users.component';
import { UsersService } from '../../../core/services/users.service';
import { NotificationService } from '../../../core/services/notification.service';
import { AuthService } from '../../../core/services/auth.service';
import { of } from 'rxjs';
import { BaseUserDTO } from '../../../models/user-models/base-user.dto';
import { AdminInfoComponent } from '../admin-info/admin-info.component';
import { CreateAdminComponent } from '../create-admin/create-admin.component';
import { PendingProblemsComponent } from '../pending-problems/pending-problems.component';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '../../../shared/shared.module';
import { CoreModule } from '../../../core/core.module';
import { ActivatedRoute } from '@angular/router';
import { InvestmentsService } from '../../../core/services/investments.service';
import { AdminRoutingModule } from '../admin-routing.module';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from '../../../app-routing.module';
import { LoginPageModule } from '../../login-page/login-page.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('UsersController', () => {
  const user51: BaseUserDTO = {
    id: '5',
    username: 'user51',
    BanExpirationDate: null,
    balance: 1500,
    isBanned: false,
    roles: ['user'],
  };
  const MockActivatedRoute = {
    data: of({
      investments: [],
      debts: [],
    }),
  };

  let mockAuthService;
  let mockInvestmentsService;
  let mockUsersService;
  let debts;
  let users;
  let investments;
  let fixture: ComponentFixture<UsersComponent>;
  let component: UsersComponent;
  let notificationService;

  beforeEach(async(() => {
    jest.clearAllMocks();
    // clear all spies and mocks

    debts = [];
    investments = [];
    users = [
      user51,
      { username: 'testUser2', id: '2', role: ['user'] },
      { username: 'testUser3', id: '3', role: ['user'] },
    ];

    (mockAuthService = {
      get loggedUser$() {
        return of();
      },
    }),
      (notificationService = {
        success() {},
        error() {},
      });

    (mockUsersService = {
      getAllUsers() {
        return of();
      },
      deleteUser() {
        return of();
      },
      banUser() {
        return of();
      },
      unBanUser() {
        return of();
      },
    }),
      (mockInvestmentsService = {
        createProposal() {},
      });

    TestBed.configureTestingModule({
      declarations: [AdminInfoComponent, UsersComponent, CreateAdminComponent, PendingProblemsComponent],
      imports: [
        CommonModule,
        AdminRoutingModule,
        SharedModule,
        FormsModule,
        CoreModule,
        BrowserModule,
        CoreModule,
        LoginPageModule,
        BrowserAnimationsModule,
        SharedModule,
        ReactiveFormsModule,
        RouterTestingModule,
      ],
      providers: [AuthService, NotificationService, InvestmentsService, UsersService],
    })
      .overrideProvider(ActivatedRoute, { useValue: MockActivatedRoute })
      .overrideProvider(UsersService, { useValue: mockUsersService })
      .overrideProvider(AuthService, { useValue: mockAuthService })
      .overrideProvider(NotificationService, { useValue: notificationService })
      .compileComponents();

    fixture = TestBed.createComponent(UsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should be defined', () => {
    // Arrange & Act & Assert
    expect(component).toBeDefined();
  });

  it('should be truthy', () => {
    // Arrange & Act & Assert
    expect(component).toBeTruthy();
  });

  it('should initialize correctly with the data passed from the authService subscription', () => {
    // Arrange
    jest.spyOn(mockAuthService, 'loggedUser$', 'get').mockReturnValue(of(user51));

    // Act
    component.ngOnInit();

    // Assert
    expect(component.loggedUser).toBe(user51);
  });

  it('should correctly unsubscribe from the logged user subscription', () => {
    // Arrange
    jest.spyOn(mockAuthService, 'loggedUser$', 'get').mockReturnValue(of(user51));

    // Act
    component.ngOnInit();
    component.ngOnDestroy();

    // Assert
    expect(component.loggedUserSubscription$.closed).toBe(true);
  });

  it('should initialize correctly with the data passed from the users subscription', () => {
    // Arrange
    jest.spyOn(mockUsersService, 'getAllUsers').mockReturnValue(of([]));
    // Act
    component.ngOnInit();

    // Assert
    expect(component.users).toEqual([]);
  });

  it('should correctly splice the logged user', () => {
    // Arrange
    jest.spyOn(mockUsersService, 'getAllUsers').mockReturnValue(of(users));
    jest.spyOn(mockAuthService, 'loggedUser$', 'get').mockReturnValue(of(user51));

    // Act
    component.ngOnInit();

    // Assert
    expect(component.users).toEqual([
      {
        username: 'testUser2',
        id: '2',
        role: ['user'],
      },
      {
        username: 'testUser3',
        id: '3',
        role: ['user'],
      },
    ]);
  });

  describe('deleteUser()', () => {
    it('should call the usersService.deleteUser() with with the right arguments', () => {
      // Arrange
      users = [
        { username: 'testUser2', id: '2', role: ['user'] },
        {
          username: 'testUser3',
          id: '3',
          role: ['user'],
        },
      ];
      component.users = users;
      const spy = jest
        .spyOn(mockUsersService, 'deleteUser')
        .mockReturnValue(of({ username: 'testUser2', id: '2', role: ['user'] }));

      // Act
      component.deleteUser('2');

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith('2');
    });

    it('should correctly splice the user witch is deleted from the users array', () => {
      // Arrange
      users = [
        { username: 'testUser2', id: '2', role: ['user'] },
        {
          username: 'testUser3',
          id: '3',
          role: ['user'],
        },
      ];
      component.users = users;
      jest
        .spyOn(mockUsersService, 'deleteUser')
        .mockReturnValue(of({ username: 'testUser2', id: '2', role: ['user'] }));
      jest.spyOn(mockAuthService, 'loggedUser$', 'get').mockReturnValue(of(user51));

      // Act
      component.deleteUser('2');

      // Assert
      expect(component.users).toEqual([
        {
          username: 'testUser3',
          id: '3',
          role: ['user'],
        },
      ]);
    });

    it('should call notificationService.success() once with correct parameters', done => {
      // Arrange
      users = [
        { username: 'testUser2', id: '2', role: ['user'] },
        {
          username: 'testUser3',
          id: '3',
          role: ['user'],
        },
      ];
      jest.spyOn(mockUsersService, 'deleteUser').mockReturnValue(of('debt'));

      const spy = jest.spyOn(notificationService, 'success');

      component.users = users;

      // Act
      component.deleteUser('2');

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith('User deleted!');

      done();
    });
    describe('banUser()', () => {
      it('should call usersService.banUser() once with correct parameters', done => {
        // Arrange
        users = [
          { username: 'testUser2', id: '2', role: ['user'] },
          {
            username: 'testUser3',
            id: '3',
            role: ['user'],
          },
        ];

        const spy = jest
          .spyOn(mockUsersService, 'banUser')
          .mockImplementation(() => of({ username: 'testUser2', id: '2', role: ['user'] }));

        component.loggedUserId = user51.id;
        component.users = users;

        // Act
        component.days = 2;
        component.banUser('2');

        // Assert
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith('2', { numberOfDays: 2 });

        done();
      });

      it('should correctly update the baned user', done => {
        // Arrange
        users = [
          { username: 'testUser2', id: '2', role: ['user'], isBanned: false },
          {
            username: 'testUser3',
            id: '3',
            role: ['user'],
            isBanned: false,
          },
        ];

        jest.spyOn(mockUsersService, 'banUser').mockImplementation(() =>
          of({
            username: 'testUser2',
            id: '2',
            role: ['user'],
            isBanned: true,
          }),
        );

        component.loggedUserId = user51.id;
        component.users = users;

        // Act
        component.users = users;
        component.days = 2;
        component.banUser('2');

        // Assert
        expect(component.users[0]).toEqual({
          username: 'testUser2',
          id: '2',
          role: ['user'],
          isBanned: true,
        });

        done();
      });

      it('should call notificationService.success() once with correct parameters', done => {
        // Arrange
        users = [
          { username: 'testUser2', id: '2', role: ['user'] },
          {
            username: 'testUser3',
            id: '3',
            role: ['user'],
          },
        ];

        jest
          .spyOn(mockUsersService, 'banUser')
          .mockImplementation(() => of({ username: 'testUser2', id: '2', role: ['user'] }));

        const spy = jest.spyOn(notificationService, 'success');

        component.loggedUserId = user51.id;
        component.users = users;

        // Act
        component.days = 2;
        component.banUser('2');

        // Assert
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith('User banned for 2 days.');

        done();
      });
      describe('banUser()', () => {
        it('should call usersService.unBanUser() once with correct parameters', done => {
          // Arrange
          users = [
            { username: 'testUser2', id: '2', role: ['user'], isBanned: true },
            {
              username: 'testUser3',
              id: '3',
              role: ['user'],
              isBanned: false,
            },
          ];

          const spy = jest.spyOn(mockUsersService, 'unBanUser').mockImplementation(() =>
            of({
              username: 'testUser2',
              id: '2',
              role: ['user'],
              isBanned: false,
            }),
          );

          component.loggedUserId = user51.id;
          component.users = users;

          // Act
          component.unBanUser('2');

          // Assert
          expect(spy).toHaveBeenCalledTimes(1);
          expect(spy).toHaveBeenCalledWith('2');

          done();
        });

        it('should correctly update the baned user', done => {
          // Arrange
          users = [
            { username: 'testUser2', id: '2', role: ['user'], isBanned: true },
            {
              username: 'testUser3',
              id: '3',
              role: ['user'],
              isBanned: false,
            },
          ];

          jest.spyOn(mockUsersService, 'unBanUser').mockImplementation(() =>
            of({
              username: 'testUser2',
              id: '2',
              role: ['user'],
              isBanned: false,
            }),
          );

          component.loggedUserId = user51.id;
          component.users = users;

          // Act
          component.users = users;
          component.unBanUser('2');

          // Assert
          expect(component.users[0]).toEqual({
            username: 'testUser2',
            id: '2',
            role: ['user'],
            isBanned: false,
          });

          done();
        });

        it('should call notificationService.success() once with correct parameters', done => {
          // Arrange
          users = [
            { username: 'testUser2', id: '2', role: ['user'], isBanned: true },
            {
              username: 'testUser3',
              id: '3',
              role: ['user'],
              isBanned: false,
            },
          ];

          jest.spyOn(mockUsersService, 'unBanUser').mockImplementation(() =>
            of({
              username: 'testUser2',
              id: '2',
              role: ['user'],
              isBanned: false,
            }),
          );

          const spy = jest.spyOn(notificationService, 'success');

          component.loggedUserId = user51.id;
          component.users = users;

          // Act
          component.days = 2;
          component.unBanUser('2');

          // Assert
          expect(spy).toHaveBeenCalledTimes(1);
          expect(spy).toHaveBeenCalledWith('User unBanned');

          done();
        });
      });
    });
  });
});
