import { ModalModule } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminInfoComponent } from './admin-info/admin-info.component';
import { UsersComponent } from './users/users.component';
import { AdminRoutingModule } from './admin-routing.module';
import { CreateAdminComponent } from './create-admin/create-admin.component';
import { PendingProblemsComponent } from './pending-problems/pending-problems.component';
import { SharedModule } from '../../shared/shared.module';
import { ProblemDetailsModalComponent } from './pending-problems/problem-details-modal/problem-details-modal.component';

@NgModule({
  declarations: [
    AdminInfoComponent,
    CreateAdminComponent,
    PendingProblemsComponent,
    ProblemDetailsModalComponent,
    UsersComponent,
  ],
  imports: [CommonModule, AdminRoutingModule, SharedModule, FormsModule, ModalModule.forRoot()],
  providers: [BsModalRef],
  entryComponents: [ProblemDetailsModalComponent],
})
export class AdminModule {}
