import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-info',
  templateUrl: './admin-info.component.html',
  styleUrls: ['./admin-info.component.css'],
})
export class AdminInfoComponent {
  show = false;

  showUsers() {
    this.show = true;
  }
  closeUsers() {
    this.show = false;
  }
}
