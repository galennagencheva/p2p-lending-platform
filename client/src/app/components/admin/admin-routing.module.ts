import { AuthGuard } from './../../auth/auth.guard';
import { AdminInfoComponent } from '../admin/admin-info/admin-info.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CreateAdminComponent } from './create-admin/create-admin.component';
import { PendingProblemsComponent } from './pending-problems/pending-problems.component';
import { AdminGuard } from '../../auth/admin.guard';

const adminRoutes: Routes = [
  { path: '', component: AdminInfoComponent, canActivate: [AdminGuard, AuthGuard] },
  { path: 'create-admin', component: CreateAdminComponent, canActivate: [AdminGuard, AuthGuard] },
  { path: 'pending-problems', component: PendingProblemsComponent, canActivate: [AdminGuard, AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(adminRoutes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {}
