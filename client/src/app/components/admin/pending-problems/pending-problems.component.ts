import { ProblemDetailsModalComponent } from './problem-details-modal/problem-details-modal.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { BaseUserDTO } from '../../../models/user-models/base-user.dto';
import { ProblemDTO } from '../../../models/problem-models/problem.dto';
import { AuthService } from '../../../core/services/auth.service';
import { ProblemsService } from '../../../core/services/problems.service';

@Component({
  selector: 'app-pending-problems',
  templateUrl: './pending-problems.component.html',
  styleUrls: ['./pending-problems.component.css'],
})
export class PendingProblemsComponent implements OnInit, OnDestroy {
  private userSubscription$: Subscription;
  loggedUser: BaseUserDTO;
  problems: ProblemDTO[];

  problemDetailsModal: BsModalRef;

  protected readonly highlightColor = '#fc9a4aa3';

  constructor(
    private readonly authService: AuthService,
    private readonly problemsService: ProblemsService,
    private readonly modalsService: BsModalService,
  ) {}

  ngOnInit() {
    this.userSubscription$ = this.authService.loggedUser$.subscribe(user => (this.loggedUser = user));

    this.problemsService.getAllProblems().subscribe(problems => {
      this.problems = problems;
    });
  }

  openProblemDetailsModal(problem: ProblemDTO) {
    const initialState = {
      problem,
    };
    this.problemDetailsModal = this.modalsService.show(ProblemDetailsModalComponent, { initialState });
  }

  ngOnDestroy() {
    if (this.userSubscription$) {
      this.userSubscription$.unsubscribe();
    }
  }
}
