import { itemPluralMapping } from '../../../../common/utils/item-plural-mapping';
import { ProblemDTO } from './../../../../models/problem-models/problem.dto';
import { BsModalRef } from 'ngx-bootstrap';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-problem-details-modal',
  templateUrl: './problem-details-modal.component.html',
  styleUrls: ['./problem-details-modal.component.css'],
})
export class ProblemDetailsModalComponent implements OnInit {
  @Input()
  problem: ProblemDTO;

  protected readonly itemPluralMapping = itemPluralMapping;

  constructor(protected problemDetailsModal: BsModalRef) {}

  ngOnInit() {}
}
