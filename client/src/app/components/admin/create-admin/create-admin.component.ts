import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AuthService } from '../../../core/services/auth.service';
import { NotificationService } from '../../../core/services/notification.service';
import { UsersService } from '../../../core/services/users.service';

@Component({
  selector: 'app-create-admin',
  templateUrl: './create-admin.component.html',
  styleUrls: ['./create-admin.component.css'],
})
export class CreateAdminComponent implements OnInit {
  admins = [];
  showModal = true;
  registerForm: FormGroup;

  constructor(
    private readonly fb: FormBuilder,
    private readonly authService: AuthService,
    private readonly notificator: NotificationService,
    private readonly usersService: UsersService,
  ) {
    this.registerForm = this.fb.group({
      username: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      email: ['', Validators.compose([Validators.required, Validators.email])],

      password: ['', Validators.compose([Validators.required, Validators.minLength(8)])],
    });
  }

  ngOnInit() {
    this.usersService
      .getAllAdmins()
      .subscribe(admins => (this.admins = admins.filter(admin => admin.roles.includes('admin'))));
  }

  createAdmin() {
    this.authService.createAdmin(this.registerForm.value).subscribe(
      admin => {
        this.notificator.success(`You registered Admin Successfuly !`);
        const { username, password } = this.registerForm.value;
        this.admins.push(admin);
      },
      error => {
        this.notificator.error(error.error.error);
      },
    );
  }
}
