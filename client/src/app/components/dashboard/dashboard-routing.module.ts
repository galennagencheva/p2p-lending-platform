import { DashboardViewComponent } from './dashboard-view/dashboard-view.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../auth/auth.guard';
import { UsersGuard } from '../../auth/users.guard';

const dashboardRoutes: Routes = [{ path: '', component: DashboardViewComponent, canActivate: [AuthGuard, UsersGuard] }];

@NgModule({
  imports: [RouterModule.forChild(dashboardRoutes)],
  exports: [RouterModule],
})
export class DashboardRoutingModule {}
