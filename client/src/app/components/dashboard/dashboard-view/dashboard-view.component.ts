import { itemPluralMapping } from '../../../common/utils/item-plural-mapping';
import { InvestorUserInfoDTO } from './../../../models/user-models/investor-user-info.dto';
import { BorrowerUserInfoDTO } from './../../../models/user-models/borrower-user-info.dto';
import { LoggedUserInfoDTO } from './../../../models/user-models/logged-user-info.dto';
import { DashboardUserInfoDTO } from './../../../models/user-models/dashboard-user-info.dto';
import { calculateDueDate } from '../../../common/utils/next-duedate';
import { UpdateUserBalanceDTO } from './../../../models/user-models/update-user-balance.dto';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { UsersService } from '../../../core/services/users.service';
import { StatusLoan } from '../../../common/enums/status-loan.enum';
import { StatusInvestment } from '../../../common/enums/status-investment.enum';
import * as moment from 'moment';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-dashboard-view',
  templateUrl: './dashboard-view.component.html',
  styleUrls: ['./dashboard-view.component.css'],
})
export class DashboardViewComponent implements OnInit, OnDestroy {
  loggedUserId: string;
  dashboardUserInfo: DashboardUserInfoDTO;
  borrowerUserInfo: BorrowerUserInfoDTO;
  investorUserInfo: InvestorUserInfoDTO;

  overdueBorrowerShow: boolean;
  overdueInvestorShow: boolean;

  currentDate = moment(new Date()).format('YYYY-MM-DD');

  protected readonly itemPluralMapping = itemPluralMapping;

  private loggedUserInfoSubscription$: Subscription;

  constructor(private readonly route: ActivatedRoute, private readonly usersService: UsersService) {}

  ngOnInit() {
    this.loggedUserInfoSubscription$ = this.usersService.loggedUserInfo$.subscribe(loggedUserInfo => {
      if (loggedUserInfo !== null) {
        this.getborrowerInfo(loggedUserInfo);
        this.getInvestorUserInfo(loggedUserInfo);

        this.loggedUserId = loggedUserInfo.id;
      }
    });
  }

  ngOnDestroy() {
    if (this.loggedUserInfoSubscription$) {
      this.loggedUserInfoSubscription$.unsubscribe();
    }
  }

  depositToBalance(depositToBalance: UpdateUserBalanceDTO) {
    this.usersService.updateBalance(this.loggedUserId, depositToBalance).toPromise();
  }

  withdrawFromBalance(withdrawFromBalance: UpdateUserBalanceDTO) {
    this.usersService.updateBalance(this.loggedUserId, withdrawFromBalance).toPromise();
  }

  private getborrowerInfo(loggedUserInfo: LoggedUserInfoDTO) {
    this.borrowerUserInfo = new BorrowerUserInfoDTO();

    const investorsCount = new Set();
    loggedUserInfo.loans.map(loan => investorsCount.add(loan.investment));
    this.borrowerUserInfo.investorsCount = investorsCount.size;

    const closestLoanToPay = loggedUserInfo.loans
      .filter(loan => loan.status === StatusLoan.Ongoing)
      .map(loan => {
        const nextDueDate: string = calculateDueDate(loan.createdOn, loan.payments.length);
        return {
          nextDueDate,
          monthlyInstallment: loan.monthlyInstallment,
        };
      })
      .sort((a, b) => a.nextDueDate.localeCompare(b.nextDueDate))[0];

    if (closestLoanToPay) {
      this.borrowerUserInfo.nextDueDate = closestLoanToPay.nextDueDate;
      this.borrowerUserInfo.nextInstallment = closestLoanToPay.monthlyInstallment;

      if (closestLoanToPay.nextDueDate < this.currentDate) {
        this.overdueBorrowerShow = true;
        Swal.fire({
          icon: `warning`,
          title: `You have missed your monthly installment!`,
          text: `Please top up and proceed payment
          as soon as possible!`,
        });
      }
    } else {
      this.borrowerUserInfo.nextDueDate = '0';
      this.borrowerUserInfo.nextInstallment = 0;
    }

    const totalPeriodOfLoans = loggedUserInfo.loans
      .filter(loan => loan.status === StatusLoan.Ongoing)
      .map(loan => loan.investment.period)
      .sort((a, b) => b - a)[0];
    if (totalPeriodOfLoans) {
      this.borrowerUserInfo.totalPeriodOfLoans = totalPeriodOfLoans;
    } else {
      this.borrowerUserInfo.totalPeriodOfLoans = 0;
    }
  }

  private getInvestorUserInfo(loggedUserInfo: LoggedUserInfoDTO) {
    this.investorUserInfo = new InvestorUserInfoDTO();

    const borrowersCount = new Set();
    loggedUserInfo.investments
      .filter(investment => investment.status === StatusInvestment.Ongoing)
      .map(investment => borrowersCount.add(investment.debt.user.id));
    this.investorUserInfo.borrowersCount = borrowersCount.size;

    const allInvestmentsLoans = [];
    loggedUserInfo.investments
      .filter(investment => investment.status === StatusInvestment.Ongoing)
      .map(investment => {
        const eachInvestmentLoan = investment.loans[0];
        if (eachInvestmentLoan) {
          allInvestmentsLoans.push(eachInvestmentLoan);
        }
      });

    const totalPeriodOfInvestmentLoan = allInvestmentsLoans
      .filter(loan => loan.status === StatusLoan.Ongoing)
      .map(loan => loan.investment.period)
      .sort((a, b) => b - a)
      .shift();

    if (totalPeriodOfInvestmentLoan) {
      this.investorUserInfo.totalPeriodOfInvestments = totalPeriodOfInvestmentLoan;
    } else {
      this.investorUserInfo.totalPeriodOfInvestments = 0;
    }

    const closestLoanToBePayedTheInvestor = allInvestmentsLoans
      .filter(loan => loan.status === StatusLoan.Ongoing)
      .map(loan => {
        const nextDueDate: string = calculateDueDate(loan.createdOn, loan.payments.length);
        return {
          nextDueDate,
          monthlyInstallment: loan.monthlyInstallment,
        };
      })
      .sort((a, b) => a.nextDueDate.localeCompare(b.nextDueDate))[0];

    if (closestLoanToBePayedTheInvestor) {
      this.investorUserInfo.nextDueDate = closestLoanToBePayedTheInvestor.nextDueDate;
      this.investorUserInfo.nextInstallmentToBePayedToTheInvestor = closestLoanToBePayedTheInvestor.monthlyInstallment;

      if (closestLoanToBePayedTheInvestor.nextDueDate < this.currentDate) {
        this.overdueInvestorShow = true;
        Swal.fire({
          icon: `warning`,
          title: `You have an investment with overdue payment!`,
          text: `The borrower has been notified! If no further payment appears, please contact the administrator!`,
        });
      }
    } else {
      this.investorUserInfo.nextDueDate = '0';
      this.investorUserInfo.nextInstallmentToBePayedToTheInvestor = 0;
    }
  }
}
