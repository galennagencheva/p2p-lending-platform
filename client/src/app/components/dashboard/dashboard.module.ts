import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardViewComponent } from './dashboard-view/dashboard-view.component';
import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BalanceViewComponent } from './balance-view/balance-view.component';
import { WithdrawalModalComponent } from './balance-view/withdrawal-modal/withdrawal-modal.component';
import { DepositModalComponent } from './balance-view/deposit-modal/deposit-modal.component';

@NgModule({
  declarations: [DashboardViewComponent, BalanceViewComponent, WithdrawalModalComponent, DepositModalComponent],
  imports: [CommonModule, SharedModule, DashboardRoutingModule, ModalModule.forRoot(), SweetAlert2Module],
  providers: [BsModalRef],
  entryComponents: [DepositModalComponent, WithdrawalModalComponent],
  exports: [],
})
export class DashboardModule {}
