import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-deposit-modal',
  templateUrl: './deposit-modal.component.html',
  styleUrls: ['./deposit-modal.component.css'],
})
export class DepositModalComponent implements OnInit {
  depositMoneyForm: FormGroup;

  @Output() deposit = new EventEmitter<number>();

  constructor(public bsModalRef: BsModalRef, private readonly fb: FormBuilder) {
    this.depositMoneyForm = this.fb.group({
      amount: [1, [Validators.compose([Validators.required, Validators.min(1)])]],
    });
  }

  ngOnInit() {}

  depositMoney() {
    this.deposit.emit(+this.depositMoneyForm.value.amount);
  }
}
