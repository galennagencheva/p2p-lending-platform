import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-withdrawal-modal',
  templateUrl: './withdrawal-modal.component.html',
  styleUrls: ['./withdrawal-modal.component.css'],
})
export class WithdrawalModalComponent implements OnInit {
  withdrawMoneyForm: FormGroup;

  @Output() withdraw: EventEmitter<number> = new EventEmitter();

  constructor(public bsModalRef: BsModalRef, private readonly fb: FormBuilder) {
    this.withdrawMoneyForm = this.fb.group({
      amount: [1, [Validators.compose([Validators.required, Validators.min(1)])]],
    });
  }

  ngOnInit() {}

  withdrawMoney() {
    this.withdraw.emit(+this.withdrawMoneyForm.value.amount);
  }
}
