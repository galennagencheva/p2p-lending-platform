import { NotificationService } from './../../../core/services/notification.service';
import { WithdrawalModalComponent } from './withdrawal-modal/withdrawal-modal.component';
import { UpdateUserBalance } from './../../../common/enums/update-user-balance.enum';
import { UpdateUserBalanceDTO } from './../../../models/user-models/update-user-balance.dto';
import { DepositModalComponent } from './deposit-modal/deposit-modal.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Subscription } from 'rxjs';
import { UsersService } from './../../../core/services/users.service';
import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-balance-view',
  templateUrl: './balance-view.component.html',
  styleUrls: ['./balance-view.component.css'],
})
export class BalanceViewComponent implements OnInit, OnDestroy {
  loggedUserBalance: number;
  bsModalRef: BsModalRef;

  @Output() deposit: EventEmitter<UpdateUserBalanceDTO> = new EventEmitter();
  @Output() withdraw: EventEmitter<UpdateUserBalanceDTO> = new EventEmitter();

  loggedUserInfoSubscription$: Subscription;
  depositToBalanceSubscription$: Subscription;
  withdrawFromBalanceSubscription$: Subscription;

  constructor(
    private readonly usersService: UsersService,
    private readonly modalService: BsModalService,
    private readonly notificationService: NotificationService,
  ) {}

  ngOnInit() {
    this.loggedUserInfoSubscription$ = this.usersService.loggedUserInfo$.subscribe(loggedUserInfo => {
      if (loggedUserInfo !== null) {
        this.loggedUserBalance = loggedUserInfo.balance;
      }
    });
  }

  ngOnDestroy() {
    if (this.loggedUserInfoSubscription$) {
      this.loggedUserInfoSubscription$.unsubscribe();
    }

    if (this.depositToBalanceSubscription$) {
      this.depositToBalanceSubscription$.unsubscribe();
    }

    if (this.withdrawFromBalanceSubscription$) {
      this.withdrawFromBalanceSubscription$.unsubscribe();
    }
  }

  openModalForDeposit() {
    this.bsModalRef = this.modalService.show(DepositModalComponent);

    this.depositToBalanceSubscription$ = this.bsModalRef.content.deposit.subscribe(depositedAmount => {
      const depositValue: UpdateUserBalanceDTO = {
        amount: depositedAmount,
        action: UpdateUserBalance.Deposit,
      };
      this.deposit.emit(depositValue);
      this.bsModalRef.hide();
    });
  }

  openModalForWithdrawal() {
    this.bsModalRef = this.modalService.show(WithdrawalModalComponent);

    this.withdrawFromBalanceSubscription$ = this.bsModalRef.content.withdraw.subscribe(withdrawedAmount => {
      if (withdrawedAmount > this.loggedUserBalance) {
        this.notificationService.error('Insufficient balance! Please top up!');
      } else {
        const withdrawValue: UpdateUserBalanceDTO = {
          amount: withdrawedAmount,
          action: UpdateUserBalance.Withdraw,
        };
        this.withdraw.emit(withdrawValue);
      }
      this.bsModalRef.hide();
    });
  }
}
