import { CoreModule } from './core.module';
import { Module } from '@nestjs/common';
import { DatabaseModule } from './database/database.module';
import { DebtsModule } from './features/debts/debts.module';
import { UsersModule } from './features/users/users.module';
import { InvestmentsModule } from './features/investments/investments.module';
import { LoansModule } from './features/loans/loans.module';
import { ProblemsModule } from './features/problems/problems.module';
import { PaymentsModule } from './features/payments/payments.module';

@Module({
  imports: [
    DatabaseModule,
    UsersModule,
    DebtsModule,
    CoreModule,
    InvestmentsModule,
    LoansModule,
    PaymentsModule,
    ProblemsModule,
  ],
})
export class AppModule {}
