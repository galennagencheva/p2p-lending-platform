import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProblemsController } from './problems.controller';
import { Problem } from '../../database/entities/problems.entity';
import { ProblemsService } from './problems.service';
import { Loan } from '../../database/entities/loan.entity';
import { User } from '../../database/entities/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Problem, Loan])],
  providers: [ProblemsService],
  controllers: [ProblemsController],
  exports: [ProblemsService],
})
export class ProblemsModule {}
