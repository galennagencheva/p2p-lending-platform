import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import {
  UseGuards,
  Controller,
  Get,
  UseInterceptors,
  Post,
  Param,
  Body,
} from '@nestjs/common';
import { AuthGuardWithBlacklisting } from '../../middlewares/guards/blacklist.guard';
import { ProblemDTO } from '../../models/issues-models/problem.dto';
import { TransformInterceptor } from '../../middlewares/transformer/interceptors/transform.interceptor';
import { ProblemsService } from './problems.service';
import { CreateIssueDTO } from '../../models/issues-models/create-issue.dto';
import { Problem } from '../../database/entities/problems.entity';
import { validateLoggedUser } from '../../common/utils/validate-logged-user';
import { User } from '../../database/entities/user.entity';
import { GetUser } from '../../middlewares/decorators/user.decorator';
import { RolesGuard } from '../../middlewares/guards/roles.guard';
import { RoleType } from '../../common/enums/role-type.enum';
import { Roles } from '../../common/decorators/roles.decorator';

@Controller('problems')
@UseGuards(AuthGuardWithBlacklisting)
@ApiUseTags('Problems controller')
export class ProblemsController {
  constructor(private readonly problemsService: ProblemsService) {}

  @Get()
  @ApiBearerAuth()
  @UseGuards(AuthGuardWithBlacklisting, RolesGuard)
  @Roles(RoleType.Admin)
  @UseInterceptors(new TransformInterceptor(ProblemDTO))
  async getAllProblems() {
    return await this.problemsService.getAllProblems();
  }

  @Post(':userId/loan/:loanId')
  @ApiBearerAuth()
  @UseGuards(AuthGuardWithBlacklisting, RolesGuard)
  @Roles(RoleType.User)
  @UseInterceptors(new TransformInterceptor(CreateIssueDTO))
  async createProblem(
    @Param('userId') userId: string,
    @Param('loanId') loanId: string,
    @Body() body: CreateIssueDTO,
    @GetUser() user: User,
  ): Promise<Problem> {
    validateLoggedUser(userId, user);
    return await this.problemsService.createProblem(user, loanId, body);
  }
}
