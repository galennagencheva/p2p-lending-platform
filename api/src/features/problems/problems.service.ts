import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Problem } from '../../database/entities/problems.entity';
import { Repository } from 'typeorm';
import { CreateIssueDTO } from '../../models/issues-models/create-issue.dto';
import { Loan } from '../../database/entities/loan.entity';
import { User } from '../../database/entities/user.entity';

@Injectable()
export class ProblemsService {
  constructor(
    @InjectRepository(Problem)
    private readonly ProblemsRepository: Repository<Problem>,
    @InjectRepository(Loan)
    private readonly loansRepository: Repository<Loan>,
  ) {}

  async getAllProblems(): Promise<Problem[]> {
    return await this.ProblemsRepository.find({
      relations: ['user', 'loan', 'loan.investment', 'loan.investment.user'],
      order: { id: 'DESC' },
    });
  }

  async createProblem(
    user: User,
    loanId: string,
    problem: CreateIssueDTO,
  ): Promise<Problem> {
    const foundLoan: Loan = await this.loansRepository.findOne({
      where: { id: loanId },
    });
    const createdProblem: Problem = this.ProblemsRepository.create(problem);
    createdProblem.user = Promise.resolve(user);
    createdProblem.loan = Promise.resolve(foundLoan);
    return this.ProblemsRepository.save(createdProblem);
  }
}
