import { User } from './../../database/entities/user.entity';
import { Payment } from './../../database/entities/payment.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { PaymentsController } from './payments.controller';
import { PaymentsService } from './payments.service';
import { Loan } from '../../database/entities/loan.entity';
import { Investment } from '../../database/entities/investment.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Payment, Loan, User, Investment])],
  controllers: [PaymentsController],
  providers: [PaymentsService],
})
export class PaymentsModule {}
