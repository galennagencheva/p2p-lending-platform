import { validateEntity } from '../../common/utils/validate-entity';
import { Loan } from './../../database/entities/loan.entity';
import { Payment } from './../../database/entities/payment.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../../database/entities/user.entity';
import { CreatePaymentDTO } from '../../models/payments-models/create-payment.dto';
import { StatusLoan } from '../../common/enums/status-loan.enum';
import { Investment } from '../../database/entities/investment.entity';
import { StatusInvestment } from '../../common/enums/status-investment.enum';

@Injectable()
export class PaymentsService {
  constructor(
    @InjectRepository(Payment)
    private readonly paymentsRepository: Repository<Payment>,
    @InjectRepository(Loan)
    private readonly loansRepository: Repository<Loan>,
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
    @InjectRepository(Investment)
    private readonly investmentsRepository: Repository<Investment>,
  ) {}

  async createPayment(
    loanId: string,
    userId: string,
    createdPayment: CreatePaymentDTO,
  ): Promise<Loan> {
    const loanOfThePayment: Loan = await this.loansRepository.findOne({
      where: { id: loanId },
      relations: ['investment', 'investment.user'],
    });
    validateEntity(loanOfThePayment, 'loan');

    const totalPaymentAmount: number =
      createdPayment.amount + createdPayment.penaltyAmount;

    const investmentToTheLoan: Investment = await loanOfThePayment.investment;
    const investorUser: User = await investmentToTheLoan.user;
    await this.usersRepository.update(investorUser.id, {
      balance: investorUser.balance + totalPaymentAmount,
    });

    const userThatPays: User = await this.usersRepository.findOne(userId);
    validateEntity(userThatPays, 'user');
    await this.usersRepository.update(userThatPays.id, {
      balance: userThatPays.balance - totalPaymentAmount,
    });

    const newPayment: Payment = this.paymentsRepository.create(createdPayment);
    newPayment.loan = Promise.resolve(loanOfThePayment);

    await this.paymentsRepository.save(newPayment);

    if (
      (await loanOfThePayment.payments).length ===
      (await loanOfThePayment.investment).period
    ) {
      await this.investmentsRepository.update(investmentToTheLoan.id, {
        status: StatusInvestment.Completed,
      });
      await this.loansRepository.update(loanOfThePayment.id, {
        status: StatusLoan.Completed,
      });
    }

    return await this.loansRepository.findOne({
      where: { id: loanId },
      relations: ['investment', 'investment.user', 'payments'],
    });
  }

  async getAllPaymentsPerUser(loanId: string, user: User): Promise<Payment[]> {
    return await this.paymentsRepository.find({ where: { loan: loanId } });
  }
}
