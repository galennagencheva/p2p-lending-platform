import { LoanDTO } from './../../models/loans-models/loan.dto';
import { AuthGuardWithBlacklisting } from './../../middlewares/guards/blacklist.guard';
import { validateLoggedUser } from '../../common/utils/validate-logged-user';
import { CreatePaymentDTO } from './../../models/payments-models/create-payment.dto';
import { PaymentsService } from './payments.service';
import {
  Controller,
  Param,
  Body,
  UseGuards,
  Post,
  Get,
  UseInterceptors,
} from '@nestjs/common';
import { GetUser } from '../../middlewares/decorators/user.decorator';
import { User } from '../../database/entities/user.entity';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import { PaymentDTO } from '../../models/payments-models/payment.dto';
import { TransformInterceptor } from '../../middlewares/transformer/interceptors/transform.interceptor';

@Controller('users/:userId/loans/:loanId/payments')
@UseGuards(AuthGuardWithBlacklisting)
@ApiUseTags('Payments controller')
export class PaymentsController {
  constructor(private readonly paymentsService: PaymentsService) {}

  @Get()
  @ApiBearerAuth()
  @UseInterceptors(new TransformInterceptor(PaymentDTO))
  async getAllPaymentsPerUser(
    @Param('userId') userId: string,
    @Param('loanId') loanId: string,
    @GetUser() user: User,
  ) {
    validateLoggedUser(userId, user);
    return await this.paymentsService.getAllPaymentsPerUser(loanId, user);
  }

  @Post()
  @ApiBearerAuth()
  @UseInterceptors(new TransformInterceptor(LoanDTO))
  async createPayment(
    @Param('userId') userId: string,
    @Param('loanId') loanId: string,
    @GetUser() user: User,
    @Body() createPayment: CreatePaymentDTO,
  ) {
    validateLoggedUser(userId, user);
    return await this.paymentsService.createPayment(
      loanId,
      userId,
      createPayment,
    );
  }
}
