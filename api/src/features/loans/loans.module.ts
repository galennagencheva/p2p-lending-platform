import { User } from './../../database/entities/user.entity';
import { Investment } from './../../database/entities/investment.entity';
import { Loan } from './../../database/entities/loan.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { LoansService } from './loans.service';
import { LoansController } from './loans.controller';
import { Debt } from '../../database/entities/debt.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Loan, Investment, Debt, User])],
  providers: [LoansService],
  controllers: [LoansController],
  exports: [LoansService],
})
export class LoansModule {}
