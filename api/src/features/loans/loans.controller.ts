import { LoanDTO } from './../../models/loans-models/loan.dto';
import { User } from './../../database/entities/user.entity';
import { AuthGuardWithBlacklisting } from './../../middlewares/guards/blacklist.guard';
import { validateLoggedUser } from '../../common/utils/validate-logged-user';
import { LoansService } from './loans.service';
import {
  Controller,
  Post,
  Param,
  UseGuards,
  Get,
  UseInterceptors,
  Body,
} from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { GetUser } from '../../middlewares/decorators/user.decorator';
import { TransformInterceptor } from '../../middlewares/transformer/interceptors/transform.interceptor';

@Controller()
@UseGuards(AuthGuardWithBlacklisting)
@ApiUseTags('Loans controller')
export class LoansController {
  constructor(private readonly loansService: LoansService) {}

  @Post('users/:userId/debts/:debtId/investments/:investmentId/loans')
  @ApiBearerAuth()
  @UseInterceptors(new TransformInterceptor(LoanDTO))
  async createLoan(
    @Param('userId') userId: string,
    @Param('debtId') debtId: string,
    @Param('investmentId') investmentId: string,
    @Body() createLoan: { userId: string; investmentId: string },
    @GetUser() user: User,
  ) {
    validateLoggedUser(userId, user);
    return await this.loansService.createLoan(
      debtId,
      investmentId,
      user,
      createLoan,
    );
  }

  @Get('users/:userId/loans')
  @ApiBearerAuth()
  @UseInterceptors(new TransformInterceptor(LoanDTO))
  async getAllLoansPerUser(
    @Param('userId') userId: string,
    @GetUser() user: User,
  ) {
    validateLoggedUser(userId, user);
    return await this.loansService.getAllLoansPerUser(userId);
  }
}
