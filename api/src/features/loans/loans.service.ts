import { User } from './../../database/entities/user.entity';
import { Investment } from './../../database/entities/investment.entity';
import { Debt } from './../../database/entities/debt.entity';
import { validateEntity } from '../../common/utils/validate-entity';
import { Injectable } from '@nestjs/common';
import { Loan } from '../../database/entities/loan.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { StatusInvestment } from '../../common/enums/status-investment.enum';
import { StatusLoan } from '../../common/enums/status-loan.enum';
import { StatusDebt } from '../../common/enums/status-debt.enum';

@Injectable()
export class LoansService {
  constructor(
    @InjectRepository(Loan)
    private readonly loansRepository: Repository<Loan>,
    @InjectRepository(Investment)
    private readonly investmentsRepository: Repository<Investment>,
    @InjectRepository(Debt)
    private readonly debtsRepository: Repository<Debt>,
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
  ) {}

  async createLoan(
    debtId: string,
    investmentId: string,
    user: User,
    createLoan: { userId: string; investmentId: string },
  ) {
    const acceptedInvestment: Investment = await this.investmentsRepository.findOne(
      {
        where: { id: investmentId },
        relations: ['debt', 'debt.user'],
      },
    );
    validateEntity(acceptedInvestment, 'investment');

    const debtOfTheNewLoan: Debt = await this.debtsRepository.findOne(debtId);
    validateEntity(debtOfTheNewLoan, 'debt');

    await this.investmentsRepository.update(acceptedInvestment.id, {
      status: StatusInvestment.Ongoing,
    });

    const borrowerUser: User = await this.usersRepository.findOne(user.id);
    validateEntity(borrowerUser, 'user');

    await this.usersRepository.update(borrowerUser.id, {
      balance: borrowerUser.balance + acceptedInvestment.amount,
    });

    const investorUser: User = await acceptedInvestment.user;
    await this.usersRepository.update(investorUser.id, {
      balance: investorUser.balance - acceptedInvestment.amount,
    });

    const allInvestmentsPerDebtOfTheLoan: Investment[] = (await debtOfTheNewLoan.investments).filter(
      eachInvestment => eachInvestment.status === 0,
    );

    if (debtOfTheNewLoan.amount - acceptedInvestment.amount !== 0) {
      await this.debtsRepository.update(debtId, {
        status: StatusDebt.PartiallyFunded,
      });

      const remainingAmountOfTheDebt: number =
        debtOfTheNewLoan.amount -
        (allInvestmentsPerDebtOfTheLoan
          .filter(eachInvestment => eachInvestment.status === 1)
          .reduce((acc, eachInvestment) => acc + eachInvestment.amount, 0) +
          acceptedInvestment.amount);

      allInvestmentsPerDebtOfTheLoan.map(async eachInvestment => {
        if (remainingAmountOfTheDebt - eachInvestment.amount <= 0) {
          await this.investmentsRepository.update(eachInvestment.id, {
            status: StatusInvestment.Declined,
          });
        }
      });
    } else if (debtOfTheNewLoan.amount - acceptedInvestment.amount === 0) {
      await this.debtsRepository.update(debtId, { status: StatusDebt.Funded });
      allInvestmentsPerDebtOfTheLoan.map(
        async eachInvestment =>
          await this.investmentsRepository.update(eachInvestment.id, {
            status: StatusInvestment.Declined,
          }),
      );
    }

    const amount: number = acceptedInvestment.amount;
    const period: number = acceptedInvestment.period;
    const interest: number = acceptedInvestment.interest;
    const overallAmount: number = amount * (1 + (interest / 100 / 12) * period);

    const newLoan: Loan = this.loansRepository.create();

    newLoan.overallAmount = overallAmount;
    newLoan.monthlyInstallment = overallAmount / period;
    newLoan.investment = Promise.resolve(acceptedInvestment);
    newLoan.user = Promise.resolve(user);
    newLoan.debt = Promise.resolve(debtOfTheNewLoan);
    newLoan.payments = Promise.resolve([]);

    return await this.loansRepository.save(newLoan);
  }

  async getAllLoansPerUser(userId: string): Promise<Loan[]> {
    return await this.loansRepository.find({
      where: { user: userId, status: StatusLoan.Ongoing },
      relations: ['investment', 'investment.user', 'debt', 'payments'],
      order: { id: 'DESC' },
    });
  }
}
