import { User } from './../../database/entities/user.entity';
import { addDays } from '../../common/utils/add-days-fn';
import { CreateUserDTO } from '../../models/user-models/create-user.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { DaysUserIsBannedDTO } from '../../models/user-models/days-user-is-banned.dto';
import { SystemError } from '../../middlewares/exceptions/system.error';
import { validateEntity } from '../../common/utils/validate-entity';
import { Role } from '../../database/entities/roles.entity';
import { RoleType } from '../../common/enums/role-type.enum';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private readonly usersRepo: Repository<User>,
    @InjectRepository(Role) private readonly rolesRepo: Repository<Role>,
  ) {}

  async getUsers(): Promise<User[]> {
    return await this.usersRepo.find({
      where: { isDeleted: false },
    });
  }

  async getAllAdmins(): Promise<User[]> {
    const foundUsers = await this.usersRepo.find({
      where: { isDeleted: false, isBanned: false },
    });
    const result = foundUsers.forEach(admin => {
      const userRoles = [];
      admin.roles.forEach(role => userRoles.push(role.role));
      admin.roles = userRoles;
    });
    return foundUsers;
  }

  // Register User
  async registerUser(user: CreateUserDTO): Promise<User> {
    const { username, email } = user;
    const foundUsers: User[] = await this.usersRepo.find({
      where: `username LIKE '${username}' OR email LIKE '${email}'`,
    });

    if (foundUsers) {
      foundUsers.map(foundUser => {
        if (foundUser.email === email) {
          throw new SystemError('User with such email already exists!', 422);
        } else if (foundUser.username === username) {
          throw new SystemError('User with such username already exists!', 422);
        }
      });
    }
    const userRole: Role = await this.rolesRepo.findOne({
      role: RoleType.User,
    });

    const newUser: User = this.usersRepo.create(user);
    newUser.password = await bcrypt.hash(user.password, 10);
    newUser.roles = [userRole];

    return await this.usersRepo.save(newUser);
  }

  async createAdminByAdmin(user: CreateUserDTO): Promise<User> {
    const { username, email } = user;
    const foundUsers: User[] = await this.usersRepo.find({
      where: `username LIKE '${username}' OR email LIKE '${email}'`,
    });

    if (foundUsers) {
      foundUsers.map(foundUser => {
        if (foundUser.email === email) {
          throw new SystemError('User with such email already exists!', 422);
        } else if (foundUser.username === username) {
          throw new SystemError('User with such username already exists!', 422);
        }
      });
    }
    const userRoleAdmin: Role = await this.rolesRepo.findOne({
      role: RoleType.Admin,
    });
    const userRoleUser: Role = await this.rolesRepo.findOne({
      role: RoleType.User,
    });

    const newUser: User = this.usersRepo.create(user);
    newUser.password = await bcrypt.hash(user.password, 10);
    newUser.roles = [userRoleAdmin, userRoleUser];

    return await this.usersRepo.save(newUser);
  }

  async deleteUser(userId: string): Promise<User> {
    const foundUser = await this.usersRepo.findOne({ id: userId });
    return await this.usersRepo.save({
      ...foundUser,
      isDeleted: true,
    });
  }

  async banUser(
    userId: string,
    DaysUserIsBanned: DaysUserIsBannedDTO,
  ): Promise<User> {
    const { numberOfDays } = DaysUserIsBanned;

    const foundUser = await this.usersRepo.findOne({
      id: userId,
      isDeleted: false,
    });
    this.validate(foundUser, 'user');

    foundUser.BanExpirationDate = addDays(new Date(), numberOfDays);
    foundUser.isBanned = true;
    return await this.usersRepo.save(foundUser);
  }

  async unBanUser(userId: string): Promise<User> {
    const foundUser = await this.usersRepo.findOne({
      id: userId,
      isDeleted: false,
    });
    this.validate(foundUser, 'user');

    foundUser.BanExpirationDate = null;
    foundUser.isBanned = false;
    return await this.usersRepo.save(foundUser);
  }

  async getUserById(userId: string): Promise<User> {
    return await this.usersRepo.findOne({ where: { id: userId } });
  }

  async getLoggedUserInfo(user: User): Promise<User> {
    const foundUser: User = await this.usersRepo.findOne({
      where: { id: user.id },
      relations: [
        'debts',
        'investments',
        'investments.debt',
        'investments.user',
        'investments.debt.user',
        'loans',
        'loans.investment',
        'loans.investment.user',
        'loans.payments',
      ],
    });
    this.validate(foundUser, 'User');

    return foundUser;
  }

  async withdrawBalance(amount: number, user: User): Promise<User> {
    const foundUser: User = await this.usersRepo.findOne({
      where: { id: user.id },
      relations: ['debts', 'loans', 'investments'],
    });

    validateEntity(foundUser, 'User');

    foundUser.balance -= amount;

    return await this.usersRepo.save(foundUser);
  }

  async depositBalance(amount: number, user: User): Promise<User> {
    const foundUser: User = await this.usersRepo.findOne({
      where: { id: user.id },
      relations: ['debts', 'loans', 'investments'],
    });

    validateEntity(foundUser, 'User');

    foundUser.balance += amount;

    return await this.usersRepo.save(foundUser);
  }

  private validate(body: any, name: string) {
    if (!body) {
      throw new SystemError(`There is no such ${name}!`, 404);
    }
  }
}
