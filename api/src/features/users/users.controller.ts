import { UserBalanceDataDTO } from './../../models/user-models/user-balance-data.dto';
import { UserDTO } from './../../models/user-models/user.dto';
import { BaseUserDataDTO } from './../../models/user-models/base-user-data.dto';
import { TransformInterceptor } from './../../middlewares/transformer/interceptors/transform.interceptor';
import { validateLoggedUser } from '../../common/utils/validate-logged-user';
import { AuthGuardWithBlacklisting } from './../../middlewares/guards/blacklist.guard';
import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  UseGuards,
  ValidationPipe,
  Delete,
  Put,
  Patch,
  UseInterceptors,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDTO } from '../../models/user-models/create-user.dto';
import { DaysUserIsBannedDTO } from '../../models/user-models/days-user-is-banned.dto';
import { ResponseMessageDTO } from '../../models/responseMessageDTO';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import { GetUser } from '../../middlewares/decorators/user.decorator';
import { User } from '../../database/entities/user.entity';
import { UpdateBalanceStatusDTO } from '../../models/user-models/update-balance-status.dto';
import { BALANCE_STATUS } from '../../common/utils/balance-status';
import { RolesGuard } from '../../middlewares/guards/roles.guard';
import { Roles } from '../../common/decorators/roles.decorator';
import { RoleType } from '../../common/enums/role-type.enum';

@Controller('users')
@ApiUseTags('Users Controller')
export class UsersController {
  constructor(private readonly userService: UsersService) {}

  @Post()
  @UseInterceptors(new TransformInterceptor(BaseUserDataDTO))
  async registerUser(
    @Body(new ValidationPipe({ transform: true, whitelist: true }))
    body: CreateUserDTO,
  ) {
    return await this.userService.registerUser(body);
  }

  @Post('/admin')
  @UseInterceptors(new TransformInterceptor(BaseUserDataDTO))
  async createAdminByAdmin(
    @Body(new ValidationPipe({ transform: true, whitelist: true }))
    body: CreateUserDTO,
  ) {
    return await this.userService.createAdminByAdmin(body);
  }

  @Get()
  @UseGuards(AuthGuardWithBlacklisting, RolesGuard)
  @Roles(RoleType.User)
  @UseInterceptors(new TransformInterceptor(BaseUserDataDTO))
  @ApiBearerAuth()
  async getAll() {
    const allUsers = await this.userService.getUsers();
    return allUsers;
  }

  @Get('/admin')
  @UseGuards(AuthGuardWithBlacklisting, RolesGuard)
  @Roles(RoleType.Admin)
  @UseInterceptors(new TransformInterceptor(BaseUserDataDTO))
  @ApiBearerAuth()
  async getAllAdmins() {
    const allUsers = await this.userService.getAllAdmins();
    return allUsers;
  }

  @Get('/:userId/data')
  @UseGuards(AuthGuardWithBlacklisting, RolesGuard)
  @Roles(RoleType.User)
  @ApiBearerAuth()
  @UseInterceptors(new TransformInterceptor(UserDTO))
  async getLoggedUserInfo(
    @Param('userId') userId: string,
    @GetUser() user: User,
  ) {
    validateLoggedUser(userId, user);
    return await this.userService.getLoggedUserInfo(user);
  }

  @Get('/:userId')
  @UseGuards(AuthGuardWithBlacklisting, RolesGuard)
  @Roles(RoleType.User)
  @UseInterceptors(new TransformInterceptor(UserBalanceDataDTO))
  @ApiBearerAuth()
  async getUserById(@Param('userId') userId: string) {
    return await this.userService.getUserById(userId);
  }

  @Put('/:userId')
  @UseGuards(AuthGuardWithBlacklisting, RolesGuard)
  @Roles(RoleType.Admin)
  @ApiBearerAuth()
  @UseInterceptors(new TransformInterceptor(BaseUserDataDTO))
  async banUser(
    @Param('userId') userId: string,
    @Body(new ValidationPipe({ transform: true, whitelist: true }))
    DaysUserIsBanned: DaysUserIsBannedDTO,
  ) {
    return await this.userService.banUser(userId, DaysUserIsBanned);
  }

  @Delete('/unban/:userId')
  @UseGuards(AuthGuardWithBlacklisting, RolesGuard)
  @Roles(RoleType.Admin)
  @ApiBearerAuth()
  @UseInterceptors(new TransformInterceptor(BaseUserDataDTO))
  async unBanUser(@Param('userId') userId: string) {
    return await this.userService.unBanUser(userId);
  }

  @Delete('/:userId')
  @UseGuards(AuthGuardWithBlacklisting, RolesGuard)
  @Roles(RoleType.User)
  @ApiBearerAuth()
  async deleteUserByAdmin(
    @Param('userId') userId: string,
  ): Promise<ResponseMessageDTO> {
    await this.userService.deleteUser(userId);
    return { msg: 'User Deleted!' };
  }

  @Patch(':userId')
  @UseGuards(AuthGuardWithBlacklisting, RolesGuard)
  @Roles(RoleType.User)
  @ApiBearerAuth()
  @UseInterceptors(new TransformInterceptor(UserBalanceDataDTO))
  async updateBalance(
    @Param('userId') userId: string,
    @Body(new ValidationPipe({ transform: true, whitelist: true }))
    body: UpdateBalanceStatusDTO,
    @GetUser() user: User,
  ): Promise<User> {
    validateLoggedUser(userId, user);
    return await (this.userService as any)[BALANCE_STATUS[body.action]](
      body.amount,
      user,
    );
  }
}
