import { UpdateInvestmentDTO } from './../../models/investments-models/update.investment.dto';
import { validateEntity } from '../../common/utils/validate-entity';
import { User } from './../../database/entities/user.entity';
import { SystemError } from './../../middlewares/exceptions/system.error';
import { Debt } from './../../database/entities/debt.entity';
import { Investment } from './../../database/entities/investment.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateInvestmentDTO } from '../../models/investments-models/create-investment.dto';

@Injectable()
export class InvestmentsService {
  constructor(
    @InjectRepository(Investment)
    private readonly investmentsRepository: Repository<Investment>,
    @InjectRepository(Debt)
    private readonly debtsRepository: Repository<Debt>,
  ) {}

  async createInvestment(
    debtId: string,
    user: User,
    createdInvestment: CreateInvestmentDTO,
  ) {
    const debtToInvestIn = await this.debtsRepository.findOne(debtId, {
      relations: ['user'],
    });
    validateEntity(debtToInvestIn, 'debt');
    const newInvestment: Investment = this.investmentsRepository.create(
      createdInvestment,
    );
    newInvestment.debt = Promise.resolve(debtToInvestIn);
    newInvestment.user = Promise.resolve(user);
    newInvestment.loans = Promise.resolve([]);
    return await this.investmentsRepository.save(newInvestment);
  }

  async getAllInvestmentsPerUser(userId: string): Promise<Investment[]> {
    return await this.investmentsRepository.find({
      where: { user: userId },
      relations: [
        'user',
        'debt',
        'debt.user',
        'loans',
        'loans.investment',
        'loans.payments',
      ],
      order: { id: 'DESC' },
    });
  }

  async updateInvestment(
    investmentId: string,
    partialInvestment: UpdateInvestmentDTO,
  ): Promise<Investment> {
    const investmentToUpdate: Investment = await this.investmentsRepository.findOne(
      investmentId,
    );
    validateEntity(investmentToUpdate, 'investment');
    if (investmentToUpdate.status === 2) {
      throw new SystemError('This investment has already been declined!', 400);
    }
    const updatedInvestment = { ...investmentToUpdate, ...partialInvestment };
    return await this.investmentsRepository.save(updatedInvestment);
  }
}
