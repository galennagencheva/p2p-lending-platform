import { Loan } from './../../database/entities/loan.entity';
import { InvestmentsController } from './investments.controller';
import { Debt } from './../../database/entities/debt.entity';
import { Investment } from './../../database/entities/investment.entity';
import { Module } from '@nestjs/common';
import { InvestmentsService } from './investments.service';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Investment, Debt, Loan])],
  providers: [InvestmentsService],
  controllers: [InvestmentsController],
  exports: [InvestmentsService],
})
export class InvestmentsModule {}
