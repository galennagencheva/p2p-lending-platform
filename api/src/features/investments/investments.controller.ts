import { UpdateInvestmentDTO } from './../../models/investments-models/update.investment.dto';
import { InvestmentDTO } from './../../models/investments-models/investment.dto';
import { AuthGuardWithBlacklisting } from './../../middlewares/guards/blacklist.guard';
import { User } from './../../database/entities/user.entity';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import {
  Controller,
  Post,
  Param,
  Body,
  Get,
  UseGuards,
  UseInterceptors,
  Put,
} from '@nestjs/common';
import { CreateInvestmentDTO } from '../../models/investments-models/create-investment.dto';
import { InvestmentsService } from './investments.service';
import { GetUser } from '../../middlewares/decorators/user.decorator';
import { validateLoggedUser } from '../../common/utils/validate-logged-user';
import { TransformInterceptor } from '../../middlewares/transformer/interceptors/transform.interceptor';

@Controller()
@UseGuards(AuthGuardWithBlacklisting)
@ApiUseTags('Investments controller')
export class InvestmentsController {
  constructor(private readonly investmentsService: InvestmentsService) {}

  @Get('users/:userId/investments')
  @ApiBearerAuth()
  @UseInterceptors(new TransformInterceptor(InvestmentDTO))
  async getAllInvestmentsPerUser(
    @Param('userId') userId: string,
    @GetUser() user: User,
  ) {
    validateLoggedUser(userId, user);
    return await this.investmentsService.getAllInvestmentsPerUser(userId);
  }

  @Post('users/:userId/debts/:debtId/investments')
  @ApiBearerAuth()
  @UseInterceptors(new TransformInterceptor(InvestmentDTO))
  async createInvestment(
    @Param('userId') userId: string,
    @Param('debtId') debtId: string,
    @Body() createInvestment: CreateInvestmentDTO,
    @GetUser() user: User,
  ) {
    validateLoggedUser(userId, user);
    return await this.investmentsService.createInvestment(
      debtId,
      user,
      createInvestment,
    );
  }

  @Put('users/:userId/investments/:investmentId')
  @ApiBearerAuth()
  async updateInvestment(
    @Param('userId') userId: string,
    @Param('investmentId') investmentId: string,
    @GetUser() user: User,
    @Body() partialInvestment: UpdateInvestmentDTO,
  ) {
    validateLoggedUser(userId, user);
    return await this.investmentsService.updateInvestment(
      investmentId,
      partialInvestment,
    );
  }
}
