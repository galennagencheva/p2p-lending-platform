import { validateLoggedUser } from '../../common/utils/validate-logged-user';
import { DebtUserRelationDTO } from './../../models/debts-models/debt-user-relation.dto';
import { BaseDebtDTO } from './../../models/debts-models/base-debt.dto';
import { DebtDTO } from './../../models/debts-models/debt.dto';
import { UpdateDebtDTO } from './../../models/debts-models/update-debt.dto';
import {
  Controller,
  Post,
  Body,
  ValidationPipe,
  UseGuards,
  Get,
  Param,
  Put,
  Delete,
  UseInterceptors,
} from '@nestjs/common';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import { DebtsService } from './debts.service';
import { Debt } from '../../database/entities/debt.entity';
import { User } from '../../database/entities/user.entity';
import { AuthGuardWithBlacklisting } from '../../middlewares/guards/blacklist.guard';
import { CreateDebtDTO } from '../../models/debts-models/create-debt.dto';
import { GetUser } from '../../middlewares/decorators/user.decorator';
import { TransformInterceptor } from '../../middlewares/transformer/interceptors/transform.interceptor';

@Controller()
@UseGuards(AuthGuardWithBlacklisting)
@ApiUseTags('Debts Controller')
export class DebtsController {
  constructor(private readonly debtsService: DebtsService) {}

  @Get('debts')
  @ApiBearerAuth()
  @UseInterceptors(new TransformInterceptor(DebtDTO))
  async getAllDebts() {
    return await this.debtsService.getAllDebts();
  }

  @Post('users/:userId/debts')
  @ApiBearerAuth()
  @UseInterceptors(new TransformInterceptor(DebtDTO))
  async createDebt(
    @Param('userId') userId: string,
    @Body(new ValidationPipe({ whitelist: true, transform: true }))
    newDebt: CreateDebtDTO,
    @GetUser() user: User,
  ): Promise<Debt> {
    return await this.debtsService.createDebt(newDebt, user);
  }

  @Get('users/:userId/debts')
  @ApiBearerAuth()
  @UseInterceptors(new TransformInterceptor(DebtDTO))
  async getAllDebtsPerUser(
    @Param('userId') userId: string,
    @GetUser() user: User,
  ) {
    validateLoggedUser(userId, user);
    return await this.debtsService.getAllDebtsPerUser(userId);
  }

  @Get('users/:userId/debts/:debtId')
  @ApiBearerAuth()
  @UseInterceptors(new TransformInterceptor(DebtDTO))
  async getDebtById(
    @Param('userId') userId: string,
    @Param('debtId') debtId: string,
    @GetUser() user: User,
  ) {
    validateLoggedUser(userId, user);
    return await this.debtsService.getDebtById(debtId);
  }

  @Put('users/:userId/debts/:debtId')
  @ApiBearerAuth()
  @UseInterceptors(new TransformInterceptor(BaseDebtDTO))
  async updateDebt(
    @Param('userId') userId: string,
    @Param('debtId') debtId: string,
    @GetUser() user: User,
    @Body() partialDebt: UpdateDebtDTO,
  ) {
    validateLoggedUser(userId, user);
    return await this.debtsService.updateDebt(debtId, partialDebt);
  }

  @Delete('users/:userId/debts/:debtId')
  @ApiBearerAuth()
  @UseInterceptors(new TransformInterceptor(BaseDebtDTO))
  async deleteDebt(
    @Param('userId') userId: string,
    @Param('debtId') debtId: string,
    @GetUser() user: User,
  ) {
    validateLoggedUser(userId, user);
    return await this.debtsService.deleteDebt(debtId);
  }
}
