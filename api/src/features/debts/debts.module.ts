import { Investment } from './../../database/entities/investment.entity';
import { InvestmentsModule } from './../investments/investments.module';
import { Module } from '@nestjs/common';
import { DebtsService } from './debts.service';
import { DebtsController } from './debts.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Debt } from '../../database/entities/debt.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Debt, Investment]), InvestmentsModule],
  providers: [DebtsService],
  controllers: [DebtsController],
  exports: [DebtsService],
})
export class DebtsModule {}
