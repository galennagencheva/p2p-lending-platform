import { validateEntity } from '../../common/utils/validate-entity';
import { Investment } from './../../database/entities/investment.entity';
import { StatusInvestment } from './../../common/enums/status-investment.enum';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Debt } from '../../database/entities/debt.entity';
import { Repository, In } from 'typeorm';
import { User } from '../../database/entities/user.entity';
import { CreateDebtDTO } from '../../models/debts-models/create-debt.dto';
import { SystemError } from '../../middlewares/exceptions/system.error';
import { UpdateDebtDTO } from '../../models/debts-models/update-debt.dto';
import { StatusDebt } from '../../common/enums/status-debt.enum';

@Injectable()
export class DebtsService {
  constructor(
    @InjectRepository(Debt)
    private readonly debtsRepository: Repository<Debt>,
    @InjectRepository(Investment)
    private readonly investmentsRepository: Repository<Investment>,
  ) {}

  async createDebt(debt: CreateDebtDTO, user: User): Promise<Debt> {
    const createdDebt: Debt = this.debtsRepository.create(debt);
    createdDebt.user = Promise.resolve(user);
    createdDebt.investments = Promise.resolve([]);
    createdDebt.loans = Promise.resolve([]);
    return await this.debtsRepository.save(createdDebt);
  }

  async getDebtById(debtId: string): Promise<Debt> {
    const foundDebt: Debt = await this.debtsRepository.findOne({
      where: { id: debtId, isDeleted: false },
      relations: ['user', 'investments', 'investments.user'],
    });
    validateEntity(foundDebt, 'debt');
    return foundDebt;
  }

  async getAllDebts(): Promise<Debt[]> {
    return await this.debtsRepository.find({
      where: { isDeleted: false },
      relations: ['investments', 'investments.user', 'loans', 'user'],
      order: { id: 'DESC' },
    });
  }

  async getAllDebtsPerUser(userId: string) {
    const allDebtsPerUser = await this.debtsRepository.find({
      where: {
        isDeleted: false,
        user: userId,
        status: In([StatusDebt.Pending, StatusDebt.PartiallyFunded]),
      },
      relations: ['investments', 'investments.user', 'loans'],
      order: { id: 'DESC' },
    });

    allDebtsPerUser.map(async debt => {
      debt.investments = Promise.all(
        (await debt.investments).filter(
          investment => investment.status !== StatusInvestment.Declined,
        ),
      );
    });

    return allDebtsPerUser;
  }

  async updateDebt(debtId: string, partialDebt: UpdateDebtDTO): Promise<Debt> {
    const foundDebt: Debt = await this.debtsRepository.findOne({
      where: { id: debtId, isDeleted: false },
    });
    validateEntity(foundDebt, 'debt');
    const updatedDebt = { ...foundDebt, ...partialDebt };
    return await this.debtsRepository.save(updatedDebt);
  }

  async deleteDebt(debtId: string): Promise<Debt> {
    const debtToDelete: Debt = await this.debtsRepository.findOne({
      where: { id: debtId, isDeleted: false },
    });

    (await debtToDelete.investments)
      .filter(investment => investment.status !== StatusInvestment.Ongoing)
      .map(async investment => {
        await this.investmentsRepository.update(investment.id, {
          status: StatusInvestment.Declined,
        });
      });

    return await this.debtsRepository.save({
      ...debtToDelete,
      isDeleted: true,
    });
  }
}
