import { SystemError } from '../../middlewares/exceptions/system.error';
import { User } from '../../database/entities/user.entity';

export const validateLoggedUser = (id: string, user: User) => {
  if (id !== user.id) {
    throw new SystemError('Bad request!', 400);
  }
};
