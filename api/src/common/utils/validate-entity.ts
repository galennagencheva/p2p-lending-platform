import { SystemError } from '../../middlewares/exceptions/system.error';

export const validateEntity = (body: any, name: string) => {
  if (!body) {
    throw new SystemError(`Such ${name} does not exist!`, 404);
  }
};
