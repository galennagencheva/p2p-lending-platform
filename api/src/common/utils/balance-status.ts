import { UpdateBalanceStatus } from '../enums/balance.enum';

export const BALANCE_STATUS = {
  [UpdateBalanceStatus.Withdraw]: 'withdrawBalance',
  [UpdateBalanceStatus.Deposit]: 'depositBalance',
};
