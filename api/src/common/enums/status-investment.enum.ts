export enum StatusInvestment {
  Pending,
  Ongoing,
  Completed,
  Declined,
}
