import { Global, Module } from '@nestjs/common';
import { ConfigService } from './config/config.service';
import { AuthModule } from './auth/auth.module';

@Global()
@Module({
  imports: [AuthModule, ConfigService],
  exports: [AuthModule, ConfigService],
})
export class CoreModule {}
