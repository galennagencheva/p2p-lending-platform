import { LoginUserDTO } from './../models/user-models/login-user.dto';
import { SystemError } from './../middlewares/exceptions/system.error';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { User } from '../database/entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
  private readonly blacklist: string[] = [];
  constructor(
    @InjectRepository(User)
    private readonly usersRepo: Repository<User>,
    private readonly jwtService: JwtService,
  ) { }

  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.usersRepo.findOne({
      where: { username, isDeleted: false },
    });

    if (!user) {
      throw new SystemError('There is no such user!', 400);
    }

    const comparedPass = await bcrypt.compare(pass, user.password);
    if (user.username === username && !comparedPass) {
      throw new SystemError('Incorrect password!', 400);
    }

    const dateNow = new Date();
    if (user.BanExpirationDate > dateNow) {
      throw new SystemError(
        `You are banned and cannot login until ${user.BanExpirationDate.toLocaleDateString()}`,
        400,
      );
    }

    if (user && comparedPass && user.BanExpirationDate < dateNow) {
      const { password, ...result } = user;
      return result;
    }
  }

  async login(user: LoginUserDTO) {
    const loggedUser: User = await this.usersRepo.findOne({ where: { username: user.username } });
    const userRoles = [];
    loggedUser.roles.forEach(role => userRoles.push(role.role));
    const payload = { username: loggedUser.username, id: loggedUser.id, roles: userRoles };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }

  blacklistToken(token: string): void {
    this.blacklist.push(token);
  }

  isTokenBlacklisted(token: string): boolean {
    return this.blacklist.includes(token);
  }
}
