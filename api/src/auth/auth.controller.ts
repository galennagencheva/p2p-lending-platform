import { LoginUserDTO } from './../models/user-models/login-user.dto';
import { Controller, UseGuards, Post, Delete, Body } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { AuthGuardWithBlacklisting } from '../middlewares/guards/blacklist.guard';
import { Token } from '../middlewares/decorators/token.decorator';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';

@Controller('api')
@ApiUseTags('Auth controller')
export class AuthController {

  constructor(
    private readonly authService: AuthService,
  ) { }

  @Post('login')
  @UseGuards(AuthGuard('local'))
  async login(@Body() user: LoginUserDTO) {
    return this.authService.login(user);
  }

  @Delete('logout')
  @UseGuards(AuthGuardWithBlacklisting)
  @ApiBearerAuth()
  async logOut(@Token() token: string) {
    this.authService.blacklistToken(token);
  }

}
