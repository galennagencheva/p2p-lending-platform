import { createConnection } from 'typeorm';
import { RoleType } from '../../common/enums/role-type.enum';
import * as bcrypt from 'bcrypt';
import { Role } from '../entities/roles.entity';
import { User } from '../entities/user.entity';

/* tslint:disable: max-line-length */

const main = async () => {
  const connection = await createConnection();
  const roleRepo = connection.getRepository(Role);
  const userRepo = connection.getRepository(User);

  const admin = await roleRepo.save({
    role: RoleType.Admin,
  });
  const user = await roleRepo.save({
    role: RoleType.User,
  });

  const firstAdmin = new User();
  firstAdmin.username = 'theBoss';
  firstAdmin.email = 'theBoss@gmail.com';
  firstAdmin.password = await bcrypt.hash('test1234', 10);
  firstAdmin.roles = [admin, user];

  await userRepo.save(firstAdmin);
  await connection.close();

  console.log(`Data seeded successfully`);
};

main().catch(console.log);
