import { Loan } from './loan.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import { Debt } from './debt.entity';
import { Investment } from './investment.entity';
import { Role } from './roles.entity';
import { Problem } from './problems.entity';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('nvarchar', { nullable: false, unique: true })
  username: string;

  @Column('nvarchar', { unique: true })
  email: string;

  @Column('nvarchar', { nullable: false })
  password: string;

  @Column('float', { nullable: false, default: 1000 })
  balance: number;

  @Column('boolean', { default: false })
  isBanned: boolean;

  @Column('datetime', { default: null })
  BanExpirationDate: Date;

  @Column('boolean', { default: false })
  isDeleted: boolean;

  @ManyToMany(type => Role, { eager: true })
  @JoinTable()
  roles: Role[];

  @OneToMany(() => Investment, investment => investment.user)
  investments: Promise<Investment[]>;

  @OneToMany(() => Problem, problem => problem.user)
  problems: Promise<Problem[]>;

  @OneToMany(() => Debt, debt => debt.user)
  debts: Promise<Debt[]>;

  @OneToMany(() => Loan, loan => loan.user)
  loans: Promise<Loan[]>;
}
