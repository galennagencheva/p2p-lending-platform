import { Loan } from './loan.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  Column,
  OneToMany,
} from 'typeorm';
import { User } from './user.entity';
import { StatusInvestment } from '../../common/enums/status-investment.enum';
import { Debt } from './debt.entity';

@Entity('investments')
export class Investment {
  @PrimaryGeneratedColumn('increment')
  id: string;

  @Column('float', { nullable: false })
  amount: number;

  @Column('int', { nullable: false })
  period: number;

  @Column('float', { nullable: false })
  interest: number;

  @Column('float', { nullable: false, default: 0 })
  overdueInterest: number;

  @Column({
    type: 'enum',
    enum: StatusInvestment,
    nullable: false,
    default: StatusInvestment.Pending,
  })
  status: StatusInvestment;

  @ManyToOne(() => User, user => user.investments)
  user: Promise<User>;

  @ManyToOne(() => Debt, debt => debt.investments)
  debt: Promise<Debt>;

  @OneToMany(() => Loan, loans => loans.investment)
  loans: Promise<Loan[]>;
}
