import { Loan } from './loan.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
} from 'typeorm';
import { User } from './user.entity';

@Entity('problems')
export class Problem {
  @PrimaryGeneratedColumn('increment')
  id: string;

  @Column('nvarchar')
  description: string;

  @ManyToOne(() => User, user => user.problems)
  user: Promise<User>;

  @ManyToOne(() => Loan, loans => loans.problems)
  loan: Promise<Loan>;
}
