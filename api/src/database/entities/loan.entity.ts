import { Debt } from './debt.entity';
import { StatusLoan } from './../../common/enums/status-loan.enum';
import {
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  Column,
  CreateDateColumn,
  OneToMany,
} from 'typeorm';
import { User } from './user.entity';
import { Investment } from './investment.entity';
import { Payment } from './payment.entity';
import { Problem } from './problems.entity';

@Entity('loans')
export class Loan {
  @PrimaryGeneratedColumn('increment')
  id: string;

  @Column('boolean', { nullable: false, default: false })
  isDeleted: boolean;

  @CreateDateColumn()
  createdOn: string;

  @Column('float', { nullable: false })
  monthlyInstallment: number;

  @Column('float', { nullable: false })
  overallAmount: number;

  @Column({
    type: 'enum',
    enum: StatusLoan,
    nullable: false,
    default: StatusLoan.Ongoing,
  })
  status: StatusLoan;

  @ManyToOne(() => Investment, investment => investment.loans)
  investment: Promise<Investment>;

  @ManyToOne(() => User, user => user.debts)
  user: Promise<User>;

  @OneToMany(() => Payment, payments => payments.loan)
  payments: Promise<Payment[]>;

  @OneToMany(() => Problem, problem => problem.user)
  problems: Promise<Problem[]>;

  @ManyToOne(() => Debt, debt => debt.loans)
  debt: Promise<Debt>;
}
