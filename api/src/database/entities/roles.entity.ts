import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import { RoleType } from '../../common/enums/role-type.enum';

@Entity('roles')
export class Role {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'enum', enum: RoleType })
  role: RoleType;
}
