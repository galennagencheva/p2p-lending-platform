import {
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  Column,
  CreateDateColumn,
} from 'typeorm';
import { Loan } from './loan.entity';

@Entity('payments')
export class Payment {
  @PrimaryGeneratedColumn('increment')
  id: string;

  @Column('boolean', { nullable: false, default: false })
  isDeleted: boolean;

  @CreateDateColumn()
  createdOn: string;

  @Column('float', { nullable: false })
  amount: number;

  @Column('int', { nullable: false, default: 0 })
  overdue: number;

  @Column('float', { nullable: false, default: 0 })
  penaltyAmount: number;

  @ManyToOne(() => Loan, loan => loan.payments)
  loan: Promise<Loan>;
}
