import { Loan } from './loan.entity';
import { StatusDebt } from './../../common/enums/status-debt.enum';
import {
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  Column,
  OneToMany,
} from 'typeorm';
import { User } from './user.entity';
import { Investment } from './investment.entity';

@Entity('debts')
export class Debt {
  @PrimaryGeneratedColumn('increment')
  id: string;

  @Column('float', { nullable: false })
  amount: number;

  @Column('int', { nullable: false })
  period: number;

  @Column({
    type: 'enum',
    enum: StatusDebt,
    nullable: false,
    default: StatusDebt.Pending,
  })
  status: StatusDebt;

  @Column('boolean', { nullable: false, default: false })
  isPartial: boolean;

  @Column('boolean', { nullable: false, default: false })
  isDeleted: boolean;

  @ManyToOne(() => User, user => user.debts)
  user: Promise<User>;

  @OneToMany(() => Investment, investment => investment.debt)
  investments: Promise<Investment[]>;

  @OneToMany(() => Loan, loan => loan.debt)
  loans: Promise<Debt[]>;
}
