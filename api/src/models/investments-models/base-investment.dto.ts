import { DebtUserRelationDTO } from './../debts-models/debt-user-relation.dto';
import { Debt } from './../../database/entities/debt.entity';
import { StatusInvestment } from './../../common/enums/status-investment.enum';
import { Publish } from '../../middlewares/transformer/decorators/publish';
export class BaseInvestmentDTO {
  @Publish()
  id: string;

  @Publish()
  amount: number;

  @Publish()
  period: number;

  @Publish()
  interest: number;

  @Publish()
  overdueInterest: number;

  @Publish()
  status: StatusInvestment;

  @Publish(DebtUserRelationDTO)
  debt: Debt;
}
