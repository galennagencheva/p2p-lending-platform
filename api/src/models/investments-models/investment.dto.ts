import { LoanWithoutDebtDTO } from './../loans-models/loan-without-debt.dto';
import { Debt } from './../../database/entities/debt.entity';
import { User } from './../../database/entities/user.entity';
import { BaseUserDataDTO } from './../user-models/base-user-data.dto';
import { StatusInvestment } from './../../common/enums/status-investment.enum';
import { Publish } from '../../middlewares/transformer/decorators/publish';
import { DebtUserRelationDTO } from '../debts-models/debt-user-relation.dto';
import { Loan } from '../../database/entities/loan.entity';

export class InvestmentDTO {
  @Publish()
  id: string;

  @Publish()
  amount: number;

  @Publish()
  period: number;

  @Publish()
  interest: number;

  @Publish()
  overdueInterest: number;

  @Publish()
  status: StatusInvestment;

  @Publish(BaseUserDataDTO)
  user: User;

  @Publish(DebtUserRelationDTO)
  debt: Debt;

  @Publish(LoanWithoutDebtDTO)
  loans: Loan[];
}
