import { ApiModelProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional, IsEnum } from 'class-validator';
import { StatusInvestment } from '../../common/enums/status-investment.enum';

export class UpdateInvestmentDTO {
  @ApiModelProperty()
  @IsNumber()
  @IsOptional()
  amount: number;

  @ApiModelProperty()
  @IsNumber()
  @IsOptional()
  period: number;

  @ApiModelProperty()
  @IsNumber()
  @IsOptional()
  interest: number;

  @ApiModelProperty()
  @IsNumber()
  @IsOptional()
  overdueInterest: number;

  @ApiModelProperty()
  @IsEnum(StatusInvestment)
  @IsOptional()
  status: StatusInvestment;
}
