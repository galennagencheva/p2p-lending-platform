import { User } from './../../database/entities/user.entity';
import { BaseUserDataDTO } from './../user-models/base-user-data.dto';
import { StatusInvestment } from './../../common/enums/status-investment.enum';
import { Publish } from '../../middlewares/transformer/decorators/publish';

export class InvestmentWithUserRelationDTO {
  @Publish()
  id: string;

  @Publish()
  amount: number;

  @Publish()
  period: number;

  @Publish()
  interest: number;

  @Publish()
  overdueInterest: number;

  @Publish()
  status: StatusInvestment;

  @Publish(BaseUserDataDTO)
  user: User;
}
