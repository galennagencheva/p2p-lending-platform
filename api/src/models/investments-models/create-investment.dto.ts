import { ApiModelProperty } from '@nestjs/swagger';
import { IsNumber, IsNotEmpty } from 'class-validator';

export class CreateInvestmentDTO {
  @ApiModelProperty()
  @IsNumber()
  @IsNotEmpty()
  amount: number;

  @ApiModelProperty()
  @IsNumber()
  @IsNotEmpty()
  period: number;

  @ApiModelProperty()
  @IsNumber()
  @IsNotEmpty()
  interest: number;

  @ApiModelProperty()
  @IsNumber()
  @IsNotEmpty()
  overdueInterest: number;
}
