import { ApiModelProperty } from '@nestjs/swagger';
import { IsOptional, IsNumber } from 'class-validator';

export class UpdateDebtDTO {
  @ApiModelProperty()
  @IsOptional()
  @IsNumber()
  amount: number;
  @ApiModelProperty()
  @IsOptional()
  @IsNumber()
  period: number;

  @ApiModelProperty()
  @IsOptional()
  @IsNumber()
  isPartial: boolean;
}
