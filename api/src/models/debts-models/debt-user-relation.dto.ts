import { BaseUserDataDTO } from './../user-models/base-user-data.dto';
import { Publish } from '../../middlewares/transformer/decorators/publish';
import { StatusDebt } from '../../common/enums/status-debt.enum';
import { User } from '../../database/entities/user.entity';

export class DebtUserRelationDTO {
  @Publish()
  id: string;

  @Publish()
  amount: number;

  @Publish()
  period: number;

  @Publish()
  status: StatusDebt;

  @Publish()
  isPartial: boolean;

  @Publish()
  isDeleted: boolean;

  @Publish(BaseUserDataDTO)
  user: User;
}
