import { Publish } from '../../middlewares/transformer/decorators/publish';
import { StatusDebt } from '../../common/enums/status-debt.enum';

export class BaseDebtDTO {
  @Publish()
  id: string;

  @Publish()
  amount: number;

  @Publish()
  period: number;

  @Publish()
  status: StatusDebt;

  @Publish()
  isPartial: boolean;

  @Publish()
  isDeleted: boolean;
}
