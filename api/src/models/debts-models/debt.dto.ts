import { BaseLoanDTO } from './../loans-models/base-loan.dto';
import { BaseUserDataDTO } from './../user-models/base-user-data.dto';
import { InvestmentWithUserRelationDTO } from './../investments-models/investment-with-user-relation.dto';
import { Investment } from '../../database/entities/investment.entity';
import { Publish } from '../../middlewares/transformer/decorators/publish';
import { StatusDebt } from '../../common/enums/status-debt.enum';
import { User } from '../../database/entities/user.entity';
import { Loan } from '../../database/entities/loan.entity';

export class DebtDTO {
  @Publish()
  id: string;

  @Publish()
  amount: number;

  @Publish()
  period: number;

  @Publish()
  status: StatusDebt;

  @Publish()
  isPartial: boolean;

  @Publish()
  isDeleted: boolean;

  @Publish(BaseUserDataDTO)
  user: User;

  @Publish(BaseLoanDTO)
  loans: Loan[];

  @Publish(InvestmentWithUserRelationDTO)
  investments: Investment[];
}
