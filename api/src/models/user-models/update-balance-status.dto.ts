import { IsNumber, IsEnum, IsNotEmpty } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { UpdateBalanceStatus } from '../../common/enums/balance.enum';

export class UpdateBalanceStatusDTO {
  @IsEnum(UpdateBalanceStatus)
  @ApiModelProperty({ enum: ['Deposit', 'Withdraw'] })
  action: UpdateBalanceStatus;

  @ApiModelProperty()
  @IsNotEmpty()
  @IsNumber()
  amount: number;
}
