import { LoanWithoutDebtDTO } from './../loans-models/loan-without-debt.dto';
import { BaseDebtDTO } from './../debts-models/base-debt.dto';
import { Loan } from '../../database/entities/loan.entity';
import { Investment } from '../../database/entities/investment.entity';
import { Debt } from '../../database/entities/debt.entity';
import { Publish } from '../../middlewares/transformer/decorators/publish';
import { InvestmentDTO } from '../investments-models/investment.dto';

export class UserDTO {
  @Publish()
  id: string;

  @Publish()
  username: string;

  @Publish()
  balance: string;

  @Publish(BaseDebtDTO)
  debts: Debt[];

  @Publish(InvestmentDTO)
  investments: Investment[];

  @Publish(LoanWithoutDebtDTO)
  loans: Loan[];
}
