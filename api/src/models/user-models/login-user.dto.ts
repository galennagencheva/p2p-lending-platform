import { ApiModelProperty } from '@nestjs/swagger';
export class LoginUserDTO {
  @ApiModelProperty({ example: 'galenna' })
  username: string;
  @ApiModelProperty({ example: 'test1234' })
  password: string;
}
