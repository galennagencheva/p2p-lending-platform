import { MinLength, MaxLength, IsEmail } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class CreateUserDTO {
  @ApiModelProperty()
  @MinLength(3)
  @MaxLength(20)
  public username: string;
  @ApiModelProperty()
  @MinLength(3)
  @MaxLength(20)
  public password: string;
  @ApiModelProperty()
  @IsEmail()
  public email: string;
}
