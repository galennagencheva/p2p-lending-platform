import { IsNumber, IsNotEmpty } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class DaysUserIsBannedDTO {
  @ApiModelProperty()
  @IsNumber()
  @IsNotEmpty()
  numberOfDays: number;
}
