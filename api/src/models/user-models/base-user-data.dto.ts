import { Publish } from '../../middlewares/transformer/decorators/publish';

export class BaseUserDataDTO {
  @Publish()
  id: string;

  @Publish()
  username: string;

  @Publish()
  roles: [];

  @Publish()
  isBanned: boolean;

  @Publish()
  balance: number;
}
