import { Publish } from '../../middlewares/transformer/decorators/publish';

export class UserBalanceDataDTO {
  @Publish()
  id: string;

  @Publish()
  username: string;

  @Publish()
  balance: string;
}
