import { Publish } from '../../middlewares/transformer/decorators/publish';

export class CreateIssueDTO {
  @Publish()
  id: string;

  @Publish()
  issue: string;
}
