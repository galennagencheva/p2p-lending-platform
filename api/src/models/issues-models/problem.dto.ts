import { User } from '../../database/entities/user.entity';
import { Publish } from '../../middlewares/transformer/decorators/publish';
import { Loan } from '../../database/entities/loan.entity';
import { BaseUserDataDTO } from '../user-models/base-user-data.dto';
import { LoanForProblemsDTO } from '../loans-models/loan-for-problems.dto';

export class ProblemDTO {
  @Publish()
  id: string;

  @Publish()
  description: string;

  @Publish(BaseUserDataDTO)
  user: User;

  @Publish(LoanForProblemsDTO)
  loan: Loan;
}
