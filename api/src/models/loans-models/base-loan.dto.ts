import { StatusLoan } from './../../common/enums/status-loan.enum';
import { Publish } from '../../middlewares/transformer/decorators/publish';

export class BaseLoanDTO {
  @Publish()
  id: string;

  @Publish()
  createdOn: Date;

  @Publish()
  monthlyInstallment: number;

  @Publish()
  overallAmount: number;

  @Publish()
  status: StatusLoan;
}
