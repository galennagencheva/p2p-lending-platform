import { Publish } from '../../middlewares/transformer/decorators/publish';
import { StatusLoan } from '../../common/enums/status-loan.enum';
import { InvestmentWithUserRelationDTO } from '../investments-models/investment-with-user-relation.dto';
import { Investment } from '../../database/entities/investment.entity';
import { PaymentDTO } from '../payments-models/payment.dto';
import { Payment } from '../../database/entities/payment.entity';

export class LoanWithoutDebtDTO {
  @Publish()
  id: string;

  @Publish()
  createdOn: Date;

  @Publish()
  monthlyInstallment: number;

  @Publish()
  overallAmount: number;

  @Publish()
  status: StatusLoan;

  @Publish(InvestmentWithUserRelationDTO)
  investment: Investment;

  @Publish(PaymentDTO)
  payments: Payment[];
}
