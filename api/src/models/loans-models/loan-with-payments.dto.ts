import { StatusLoan } from './../../common/enums/status-loan.enum';
import { Publish } from '../../middlewares/transformer/decorators/publish';
import { PaymentDTO } from '../payments-models/payment.dto';
import { Payment } from '../../database/entities/payment.entity';

export class LoanWithPaymentsDTO {
  @Publish()
  id: string;

  @Publish()
  createdOn: Date;

  @Publish()
  monthlyInstallment: number;

  @Publish()
  overallAmount: number;

  @Publish()
  status: StatusLoan;

  @Publish(PaymentDTO)
  payments: Payment[];
}
