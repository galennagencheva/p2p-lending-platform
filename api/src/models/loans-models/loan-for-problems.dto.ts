import { StatusLoan } from './../../common/enums/status-loan.enum';
import { Publish } from '../../middlewares/transformer/decorators/publish';
import { Investment } from '../../database/entities/investment.entity';
import { PaymentDTO } from '../payments-models/payment.dto';
import { Payment } from '../../database/entities/payment.entity';
import { InvestmentWithUserRelationDTO } from '../investments-models/investment-with-user-relation.dto';

export class LoanForProblemsDTO {
  @Publish()
  id: string;

  @Publish()
  createdOn: Date;

  @Publish()
  monthlyInstallment: number;

  @Publish()
  overallAmount: number;

  @Publish()
  status: StatusLoan;

  @Publish(InvestmentWithUserRelationDTO)
  investment: Investment;
}
