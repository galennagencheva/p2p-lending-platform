import { Publish } from '../../middlewares/transformer/decorators/publish';
import { BaseUserDataDTO } from '../user-models/base-user-data.dto';
import { User } from '../../database/entities/user.entity';
import { BaseInvestmentDTO } from '../investments-models/base-investment.dto';
import { Investment } from '../../database/entities/investment.entity';
import { StatusLoan } from '../../common/enums/status-loan.enum';

export class CreatedLoanDTO {
  @Publish()
  id: string;

  @Publish()
  createdOn: Date;

  @Publish()
  status: StatusLoan;

  @Publish(BaseUserDataDTO)
  user: User;

  @Publish(BaseInvestmentDTO)
  investment: Investment;
}
