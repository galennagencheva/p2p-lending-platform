import { Debt } from './../../database/entities/debt.entity';
import { BaseDebtDTO } from './../debts-models/base-debt.dto';
import { InvestmentWithUserRelationDTO } from './../investments-models/investment-with-user-relation.dto';
import { Investment } from './../../database/entities/investment.entity';
import { BaseUserDataDTO } from './../user-models/base-user-data.dto';
import { User } from './../../database/entities/user.entity';
import { Publish } from '../../middlewares/transformer/decorators/publish';
import { Payment } from '../../database/entities/payment.entity';
import { PaymentDTO } from '../payments-models/payment.dto';
import { StatusLoan } from '../../common/enums/status-loan.enum';

export class LoanDTO {
  @Publish()
  id: string;

  @Publish()
  createdOn: Date;

  @Publish()
  monthlyInstallment: number;

  @Publish()
  overallAmount: number;

  @Publish()
  status: StatusLoan;

  @Publish(BaseUserDataDTO)
  user: User;

  @Publish(InvestmentWithUserRelationDTO)
  investment: Investment;

  @Publish(BaseDebtDTO)
  debt: Debt;

  @Publish(PaymentDTO)
  payments: Payment[];
}
