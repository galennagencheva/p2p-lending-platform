import { Publish } from '../../middlewares/transformer/decorators/publish';

export class PaymentDTO {
  @Publish()
  id: string;

  @Publish()
  createdOn: string;

  @Publish()
  amount: number;

  @Publish()
  overdue: number;

  @Publish()
  penaltyAmount: number;
}
