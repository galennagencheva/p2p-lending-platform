import { IsOptional, IsNumber, IsNotEmpty } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class CreatePaymentDTO {
  @ApiModelProperty()
  @IsNumber()
  @IsNotEmpty()
  amount: number;

  @ApiModelProperty()
  @IsNumber()
  @IsOptional()
  overdue: number;

  @ApiModelProperty()
  @IsNumber()
  @IsOptional()
  penaltyAmount: number;
}
