import { User } from '../../database/entities/user.entity';
import { Injectable, CanActivate, ExecutionContext, HttpException, HttpStatus } from '@nestjs/common';
import { Observable } from 'rxjs';
import { Reflector } from '@nestjs/core';
import { RoleType } from '../../common/enums/role-type.enum';
import { Role } from '../../database/entities/roles.entity';

@Injectable()
export class RolesGuard implements CanActivate {

  constructor(private readonly reflector: Reflector) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
      const roles = this.reflector.get<RoleType[]>('roles', context.getHandler());
      if (!roles) {
        return true;
      }
      const request = context.switchToHttp().getRequest();
      const user: User = request.user;
      const hasRole = () => user.roles.some((role: Role) => roles.includes(role.role));
      if (user && user.roles && hasRole()) {
        return true;
      }

      throw new HttpException('You are not authorized to do this operation', HttpStatus.FORBIDDEN);
    }
}
