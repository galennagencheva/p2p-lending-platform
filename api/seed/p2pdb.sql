-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: localhost    Database: p2pdb
-- ------------------------------------------------------
-- Server version	5.7.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `debts`
--

DROP TABLE IF EXISTS `debts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `debts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` float NOT NULL,
  `period` int(11) NOT NULL,
  `status` enum('Pending','Partially Funded','Funded') NOT NULL DEFAULT 'Pending',
  `isPartial` tinyint(4) NOT NULL DEFAULT '0',
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `userId` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_834960a509c776eb841644a9bac` (`userId`),
  CONSTRAINT `FK_834960a509c776eb841644a9bac` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `debts`
--

LOCK TABLES `debts` WRITE;
/*!40000 ALTER TABLE `debts` DISABLE KEYS */;
INSERT INTO `debts` VALUES (1,10000,24,'Pending',1,0,'f8d6daec-525c-4ad6-89c1-e76fd23fb9cb'),(2,5000,12,'Pending',0,0,'f8d6daec-525c-4ad6-89c1-e76fd23fb9cb'),(3,4200,8,'Funded',0,0,'f8d6daec-525c-4ad6-89c1-e76fd23fb9cb'),(4,20000,20,'Pending',1,0,'af1001ed-0342-4618-9b8a-124d69ebd405'),(5,5000,6,'Pending',0,0,'af1001ed-0342-4618-9b8a-124d69ebd405'),(6,8000,14,'Pending',1,0,'af1001ed-0342-4618-9b8a-124d69ebd405'),(7,4000,6,'Funded',0,0,'c896a140-83e2-4244-91d0-fcb913cc8024');
/*!40000 ALTER TABLE `debts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `investments`
--

DROP TABLE IF EXISTS `investments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `investments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` float NOT NULL,
  `period` int(11) NOT NULL,
  `interest` float NOT NULL,
  `overdueInterest` float NOT NULL DEFAULT '0',
  `status` enum('0','1','2','3') NOT NULL DEFAULT '0',
  `userId` varchar(36) DEFAULT NULL,
  `debtId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_1ee4fc01d07959ee6cf7926fe3c` (`userId`),
  KEY `FK_43dad118c601f791573d2ae4d05` (`debtId`),
  CONSTRAINT `FK_1ee4fc01d07959ee6cf7926fe3c` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_43dad118c601f791573d2ae4d05` FOREIGN KEY (`debtId`) REFERENCES `debts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `investments`
--

LOCK TABLES `investments` WRITE;
/*!40000 ALTER TABLE `investments` DISABLE KEYS */;
INSERT INTO `investments` VALUES (1,4200,8,5,6,'1','c896a140-83e2-4244-91d0-fcb913cc8024',3),(2,4000,6,3,4,'1','96174a3c-3823-469e-9a13-b5852826d66e',7);
/*!40000 ALTER TABLE `investments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loans`
--

DROP TABLE IF EXISTS `loans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `loans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `monthlyInstallment` float NOT NULL,
  `overallAmount` float NOT NULL,
  `status` enum('Ongoing','Completed') NOT NULL DEFAULT 'Ongoing',
  `investmentId` int(11) DEFAULT NULL,
  `userId` varchar(36) DEFAULT NULL,
  `debtId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_520cdb0f5d5fc36870ecfa253e7` (`investmentId`),
  KEY `FK_4c2ab4e556520045a2285916d45` (`userId`),
  KEY `FK_90d92d94ab31b887216d7fe335c` (`debtId`),
  CONSTRAINT `FK_4c2ab4e556520045a2285916d45` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_520cdb0f5d5fc36870ecfa253e7` FOREIGN KEY (`investmentId`) REFERENCES `investments` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_90d92d94ab31b887216d7fe335c` FOREIGN KEY (`debtId`) REFERENCES `debts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loans`
--

LOCK TABLES `loans` WRITE;
/*!40000 ALTER TABLE `loans` DISABLE KEYS */;
INSERT INTO `loans` VALUES (1,0,'2019-10-15 23:14:19.524165',542.5,4340,'Ongoing',1,'f8d6daec-525c-4ad6-89c1-e76fd23fb9cb',3),(2,0,'2019-12-15 23:17:19.003212',676.667,4060,'Ongoing',2,'c896a140-83e2-4244-91d0-fcb913cc8024',7);
/*!40000 ALTER TABLE `loans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `amount` float NOT NULL,
  `overdue` int(11) NOT NULL DEFAULT '0',
  `penaltyAmount` float NOT NULL DEFAULT '0',
  `loanId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_138820eff6d03b08b4a6614a5e6` (`loanId`),
  CONSTRAINT `FK_138820eff6d03b08b4a6614a5e6` FOREIGN KEY (`loanId`) REFERENCES `loans` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payments`
--

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `problems`
--

DROP TABLE IF EXISTS `problems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `problems` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `userId` varchar(36) DEFAULT NULL,
  `loanId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_5be7092c979e0414c8851beafb9` (`userId`),
  KEY `FK_e44a9bd7499f98b477a753f3bdd` (`loanId`),
  CONSTRAINT `FK_5be7092c979e0414c8851beafb9` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_e44a9bd7499f98b477a753f3bdd` FOREIGN KEY (`loanId`) REFERENCES `loans` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `problems`
--

LOCK TABLES `problems` WRITE;
/*!40000 ALTER TABLE `problems` DISABLE KEYS */;
/*!40000 ALTER TABLE `problems` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` varchar(36) NOT NULL,
  `role` enum('admin','user') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES ('40db886d-8f95-4a5f-9e1f-b7c8da0446ee','admin'),('65c0a16e-3144-45ac-b0c9-1f1a3c3dfddd','user');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` varchar(36) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `balance` float NOT NULL DEFAULT '1000',
  `isBanned` tinyint(4) NOT NULL DEFAULT '0',
  `BanExpirationDate` datetime DEFAULT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_fe0bb3f6520ee0469504521e71` (`username`),
  UNIQUE KEY `IDX_97672ac88f789774dd47f7c8be` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('1a8b40f5-9a5e-4ab7-9dbd-194c05a5e551','theBoss','theBoss@gmail.com','$2b$10$qS/GRzlxYSb7P0Zt9cZQOeQNXf3/uix6K3IPoOhOUF2XVeJ00kW5W',1000,0,NULL,0),('96174a3c-3823-469e-9a13-b5852826d66e','petar_at','petar@gmail.com','$2b$10$5SxmtT4Fc9iXZ/RPOSDBF.6runk5VS8lLdeN9Uk6ASy92M2882ujG',3000,0,NULL,0),('af1001ed-0342-4618-9b8a-124d69ebd405','georgi','georgi@gmail.com','$2b$10$7orUePI0uWbQjbhiz1wZRO7m4VKEFozVyK8oSOnBE55zDyheKHLLe',1000,0,NULL,0),('c896a140-83e2-4244-91d0-fcb913cc8024','denis_invest','denis_invest@gmail.com','$2b$10$wYSDy3OkceVjTd9jQweGve5a.PwtlyBZVyUama/FxlFOJAn3hZ61y',40800,0,NULL,0),('f8d6daec-525c-4ad6-89c1-e76fd23fb9cb','galenna','galenna@gmail.com','$2b$10$KOhOICKYMQlKqb.Ua13gmuS8ah38WAViyoCRm0bnn5ihydxD02gKy',5200,0,NULL,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_roles_roles`
--

DROP TABLE IF EXISTS `users_roles_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_roles_roles` (
  `usersId` varchar(36) NOT NULL,
  `rolesId` varchar(36) NOT NULL,
  PRIMARY KEY (`usersId`,`rolesId`),
  KEY `IDX_df951a64f09865171d2d7a502b` (`usersId`),
  KEY `IDX_b2f0366aa9349789527e0c36d9` (`rolesId`),
  CONSTRAINT `FK_b2f0366aa9349789527e0c36d97` FOREIGN KEY (`rolesId`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_df951a64f09865171d2d7a502b1` FOREIGN KEY (`usersId`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_roles_roles`
--

LOCK TABLES `users_roles_roles` WRITE;
/*!40000 ALTER TABLE `users_roles_roles` DISABLE KEYS */;
INSERT INTO `users_roles_roles` VALUES ('1a8b40f5-9a5e-4ab7-9dbd-194c05a5e551','40db886d-8f95-4a5f-9e1f-b7c8da0446ee'),('1a8b40f5-9a5e-4ab7-9dbd-194c05a5e551','65c0a16e-3144-45ac-b0c9-1f1a3c3dfddd'),('96174a3c-3823-469e-9a13-b5852826d66e','65c0a16e-3144-45ac-b0c9-1f1a3c3dfddd'),('af1001ed-0342-4618-9b8a-124d69ebd405','65c0a16e-3144-45ac-b0c9-1f1a3c3dfddd'),('c896a140-83e2-4244-91d0-fcb913cc8024','65c0a16e-3144-45ac-b0c9-1f1a3c3dfddd'),('f8d6daec-525c-4ad6-89c1-e76fd23fb9cb','65c0a16e-3144-45ac-b0c9-1f1a3c3dfddd');
/*!40000 ALTER TABLE `users_roles_roles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-15 23:18:06
