# Utile Peer to Peer Lending Platform

## Description
In **Utile** platform users can both benefit from giving or taking money, depending on the intentions. Both investors and borrowers are able to deposit to or withdraw money from their balance, see statistics on borrower and on investor side. The user as a borrower is able to submit a debt request, see all his active loans and proccess monthly payments. The user as an investor can see a list of all pending debt requests and make investments based on them. He has access to all his active investments and information about the given loan.

## Backend setup

1. Enter the *p2p-lending-platform/api* folder and run ``npm install`` in the terminal.
2. In MySQL Workbench, import the p2pdb.sql file, which is in *./api/seed*. You can follow these steps:
<img src="https://i.ibb.co/9cdZT2s/steps-for-importing-database.png">

3. Setup ``.env file`` and ``.ormconfig.json`` files on root level in the *./api* folder. These files make the connection between the database and the server. Follow these examples:

### .env
```env
PORT = 3000
DB_TYPE = mysql
DB_HOST = localhost
DB_PORT = 3306
DB_USERNAME = // the username of your MySQL server
DB_PASSWORD = // the password of your MySQL server
DB_DATABASE_NAME = // the name of the newly created schema
JWT_SECRET = tainadrugput // :D or whatever secret keyword you want
JWT_EXPIRE_TIME = 1800 // token expiration time in seconds, 1800 seconds are 30 minutes
```

### ormconfig.json
```json
{
 "type": "mysql",
 "host": "localhost",
 "port": 3306,
 "username": "example", // the username of your MySQL server as a string
 "password": "example", // the password of your MySQL server as a string
 "database": "example", // the name of the newly created schema as a string
 "synchronize": true,
 "logging": false,
 "entities": [
   "src/database/entities/**/*.ts"
 ],
 "migrations": [
   "src/database/migration/**/*.ts"
 ],
 "cli": {
   "entitiesDir": "src/database/entities",
   "migrationsDir": "src/database/migration"
 }
}
```
4. Run ```npm run start:dev``` or ```npm run start``` in the terminal and you will have the backend with populated database ready for use.

If you want to take a closer look into the api, you can access our **Swagger documentation** by going to http://localhost:3000/api/

## Frontend setup

1. Enter the *p2p-lending-platform/client* folder and run ``npm install`` in the terminal.
2. Next, run ``ng serve``, which will fire the Angular application. If you want the project to automatically open in your browser, run ``ng serve --o``.

You can either create your own profile, log in as an admin with these credentials:

<kbd>username: theBoss</kbd>

<kbd>password: test1234</kbd>

or log in as the experienced investor and borrower:

<kbd>username: denis_invest</kbd>

<kbd>password: test1234</kbd>

3. Enjoy!

## Unit testing

1. You can run our unit tests by entering the *client* folder and running ``npm run test`` in the terminal.

Tests themselves are located in the same folders as the tested components/services.